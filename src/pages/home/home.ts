import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { DatabaseProvider } from './../../providers/database/database';
import {HymnsPage} from '../hymns/hymns';
import { FormControl } from '@angular/forms';
import {CustomHymnProvider} from './../../providers/custom-hymn/custom-hymn'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  hymns = []; 
  ready=false;
  custom_hymn: Array<CustomHymnProvider>=[];

   constructor(public navCtrl: NavController, private databaseprovider: DatabaseProvider, private platform: Platform) {
    this.createCustomHymns();
    this.databaseprovider.getDatabaseState().subscribe(rdy => {
      if (rdy) {  
        this.loadHymnData();
        this.ready=true;
        
      }
    })

  }

  
 
  loadHymnData() {
    this.databaseprovider.getAllHymns().then(data => {
      this.hymns = data;
    })
  }

  viewHymn(i:number){
      this.navCtrl.push(HymnsPage,{
        val:'key1',
        id:i
      })
  }


  
//   ionViewDidLoad() {

//     this.setFilteredItems();

//     this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
//         this.searching = false;
//         this.setFilteredItems();

//     });

//  }

//   onSearchInput(){
//     this.searching = true;
//  }

//  setFilteredItems() {

//     this.items = this.databaseprovider.filterItems(this.searchTerm);

//  }

}
