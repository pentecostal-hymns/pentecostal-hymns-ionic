import { Component } from '@angular/core';
import { NavController, Platform, NavParams } from 'ionic-angular';
import { DatabaseProvider } from './../../providers/database/database';

@Component({
  selector: 'page-hymns',
  templateUrl: 'hymns.html'
})

export class HymnsPage {
    hymns = [];
    hymnRes = [];

    constructor(public navCtrl: NavController, public navParams:NavParams,private databaseprovider: DatabaseProvider, private platform: Platform) {
        this.databaseprovider.getDatabaseState().subscribe(rdy => {
          
            if (rdy) {  
              let id = navParams.get('id');
              this.getHymn(id+1);
              this.loadHymnData();
            }
          })
      }
      getHymn(num:number){
        this.databaseprovider.getHymn(num).then(data => {
            this.hymnRes = data;
          })
      }

      loadHymnData() {
        this.databaseprovider.getAllHymns().then(data => {
          this.hymns = data;
        })
      }

}