import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Identifiers } from '@angular/compiler';

@Injectable()
export class CustomHymnProvider {
  id:number;
  title:string;
  author:string;
  hymn:string;
  
  constructor(id:number,title:string, author:string,hymn) {
    this.id=id;
    this.title=title;
    this.author=author;
    this.hymn=hymn;
  }

  createCustomHymns(){

    let cusHymn=new CustomHymnProvider ("1. In the book of God so precious, We are told of pentecost, How the blessed Lord’s disciples, Tarried for the Holy Ghost.
    Pentecostal fire fell on them, Burning up their sin and dross, Filling them with pow’r for service, Making them a mighty host.
    
    \n\nCho-.\nPentecostal fire is falling, Praise the Lord, it fell on me,
    Pentecostal fire is falling, Brother, it will fall on thee.
    
    \n\n2. Pentecost can be repeated, For the Lord is just the same, Yesterday, today, forever, Glory to His precious name!
    Saints of God can be victorious, Over sin and death and hell;
    Have a full and free salvation, And the blessed story tell.
    
    \n\n3. When the Church of Jesus tarries, Pentecostal fire will fall;
    Sin and wrong will be defeated, Sinners on the Lord will call.
    She will march to glorious vict\’ry, Over ev\’ry land and sea, Lifting high the bloodstained banner, Holiness her motto be.
    ","Pentecostal Fire is Falling. ","Rev. George Bennard ");  this.custom_hymn.push(cusHymn);
    
    
            let cusHymn=new CustomHymnProvider ("1. How sweet the name of Jesus sounds, Blessed be the name of the Lord;
                It soothes my sorrows, heals my wounds, Blessed be the name of the Lord.
    
    \n\nCho-.\nBlessed be the name, Blessed be the name, Blessed be the name of the Lord;
    Blessed be the name, Blessed be the name, Blessed be the name of the Lord.
    
    \n\n2.       It makes the wounded spirit whole, Blessed be the name of the Lord;
                \'Tis manna to the hungry soul, Blessed be the name of the Lord.
    
    \n\n3. It soothes the trouble sinner\'s breast, Blessed be the name of the Lord;
            It gives the weary sweetest rest, Blessed be the name of the Lord;
    
    \n\n4. Then will I tell to sinners \'round, Blessed be the name of the Lord;
    What a dear Saviour I have found, Blessed be the name of the Lord.
    
    \n\n5. There\'s music in the Saviour\'s name, Blessed be the name of the Lord;
    Let every heart His love proclaim, Blessed be the name of the Lord.
     ","Blessed Be the Name. ","R. E. Winsett. ");  this.custom_hymn.push(cusHymn);
    
    
            let cusHymn=new CustomHymnProvider ("1.    Deeper, deeper in the love of Jesus, Daily let me go;
    Higher, higher in the school of wisdom, More of grace to know.
    
    \n\nCho-.\nOh, deeper yet, I pray, And higher every day,
    And wiser, blessed Lord, In Thy precious, holy Word.
    
    \n\n2. Deeper, deeper, blessed Holy Spirit, Take me deeper still,
    Till my life is wholly lost in Jesus, And His perfect will.
    
    \n\n3. Deeper, deeper! though it cost hard trials, Deeper let me go!
    Rooted in the holy love of Jesus, Let me fruitful grow.
    
    \n\n4. Deeper, higher, every day in Jesus, Till all conflict past,
    Finds me conqu\’ror, and in His own image, Perfected at last.
    
    \n\n5. Deeper, deeper in the faith of Jesus, Holy faith and true;
    In His pow’r and soul exulting wisdom, Let me peace pursue.
    ","Deeper, Deeper. ","C. P. Jones. ");  this.custom_hymn.push(cusHymn);      
    
    
            let cusHymn=new CustomHymnProvider ("1.    I wandered in the shades of night, Till Jesus came to me,
    And with the sunlight of His love, Bid all my darkness flee.
    
    \n\nCho-.\nSunlight, sunlight in my soul today, Sunlight, sunlight all along the way;
    Since the Savior found me, took away my sin, I have had the sunlight of His love within.
    
    \n\n2. Though clouds may gather in the sky, And billows ’round me roll,
    However dark the world may be, I’ve sunlight in my soul.
    
    \n\n3. While walking in the light of God, I sweet communion find;
    I press with holy vigor on, And leave the world behind.
    
    \n\n4. I cross the wide, extended fields, I journey o’er the plain,
    And in the sunlight of His love, I reap the golden grain.
    
    \n\n5. Soon I shall see Him as He is, The Light that came to me;
    Behold the brightness of His face, Throughout eternity.
    ", "Sunlight Sunlight. ","W. S. Weeden. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. We are children of the King, As we journey hear us sing!
            Giving Jesus everything, As our all to Him we bring.
    
    \n\nCho-.\nSinging as we go today, Marching up the narrow way,
    Loudest praise let us sing, Unto Jesus Lord and King;
    Let us sing redeeming grace, Till at last we see His
    face, Shouting thru eternity, Redeemed! Redeemed!
    
    \n\n2.       He doth heav\’nly grace bestow, As we journey here below,
     And the blood doth cleanse we know, That is why we love Him so.
    
    \n\n3.       Soon we’ll see Him face to face, Shout the story, saved by grace;
     Joys shall fill that holy place, When we’ve run, and won the race.
    
    \n\n4.        Soon our journey will be o\’ver, Soon we’ll gather on that shore,
               Gather to go out no more, Vict’ry ours for evermore.
    ", "Redeemed! Redeemed! ","L. C. Hall. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1.    It may be at morn, when the day is awaking, When sunlight
    through darkness and shadow is breaking, That Jesus will come in the
    fullness of glory To receive from the world “His own.”
    
            \n\nCho-.\nLord Jesus, how long, how long Ere we shout the glad song—
    Christ returneth! Hallelujah! Hallelujah! Amen. Hallelujah! Amen.
    
    \n\n2. It may be at midday, it may be at twilight, It may be, per
    chance, that the blackness of midnight, Will burst into light in the
    blaze of His glory, When Jesus receives “His own.”
    
    \n\n3. While hosts cry Hosanna, from heaven descending, With glorified
    saints and the angels attending, With grace on His brow,
    like a halo of glory, Will Jesus receive “His own.”
    
    \n\n4. Oh, joy! oh, delight! should we go without dying, No sickness, no sadness,
    no dread and no crying; Caught up through the clouds with our
    Lord into glory, When Jesus receives “His own.” 
    ", "Christ Returned. ","H. L. Turner. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Christ our Redeemer died on the cross, Died for the sinner,
    paid all his due; Sprinkle your soul with the blood of the Lamb,
    And I will pass, will pass over you.
    
    \n\nCho-.\nWhen I see the blood, when I see the blood,
       When I see the blood, I will pass, I will pass over you.
    
    \n\n2. Chief-est of sinners, Jesus will save; As He has promised,
         that He will do; Wash in the fountain opened for sin,
     And I will pass, will pass over you.
    
    \n\n3. Judgment is coming, all will be there, Each one receiving justly his due;
    Hide in the saving sin-cleansing blood,
    And I will pass, will pass over you.
    
    \n\n4. O great compassion! O boundless love! O loving kindness,
         Faithful and true! Find peace and shelter under the blood,
    And I will pass, will pass over you.
    ", "When I See the Blood. ","John & E. A. H. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. On a hill far away stood an old rugged cross, The emblem of suff’ring and shame;
            And I love that old cross where the Dearest and Best, For a world of lost sinners was slain.
    \n\nCho-.\nSo I’ll cherish the old rugged cross, Till my trophies at last I lay down;
    I will cling to the old rugged cross, And exchange it someday for a crown.
    
    \n\n2. Oh, that old rugged cross, so despised by the world,
    Has a wondrous attraction for me; For the dear Lamb of God left His glory above,
    To bear it to dark Calvary.
    
    \n\n3. In that old rugged cross, stained with blood so divine, A wondrous beauty I see,
            For ’twas on that old cross Jesus suffered and died,
            To pardon and sanctify me.
    
    \n\n4. To the old rugged cross I will ever be true; Its shame and reproach gladly bear;
           Then He’ll call me someday to my home far away,
            Where His glory forever I’ll share.
    ", "The Old Rugged Cross. ","Rev. Geo. Bennard. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1.     O my brother, do you know the Saviour, Who is wondrous, kind, and true?
    He’s the Rock of your salvation! There’s honey in the Rock for you.
    
    \n\nCho-.\nOh, there’s honey in the Rock, my brother; There’s honey in the Rock for you;
            Leave your sins for the blood to cover; There’s honey in the Rock for you.
    
    \n\n2.  Have you tasted that the Lord is gracious? Do you walk in the way that’s new?
    Have you drunk from the living fountain? There’s honey in the Rock for you.
    
    \n\n3.  Do you pray unto God the Father, “What wilt Thou have me to do?”
    Never fear, He will surely answer, There’s honey in the Rock for you.
    
    \n\n4.  Then go out through the streets and byways, Preach the Word to the many or few;
    Say to every fallen brother, There’s honey in the Rock for you.
    ", "Honey in the Rock. ","F. A. Graves. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1.   Nothing between my soul and my Savior, Naught of this world’s delusive dream;
    I have renounced all sinful pleasure;
    Jesus is mine, there’s nothing between.
    
    \n\nCho-.\nNothing between my soul and my Savior, So that His blessed face may be seen;
    Nothing preventing the least of His favor; Keep the way clear! Let nothing between.
    
    \n\n2. Nothing between, like worldly pleasure; Habits of life, though harmless they seem,
    Must not my heart from Him ever sever; He is my all, there’s nothing between.
    
    \n\n3. Nothing between, like pride or station; Self or friends shall not intervene;
    Though it may cost me much tribulation, I am resolved, there’s nothing between.
    
    \n\n4. Nothing between, e’en many hard trials, Though the whole world against me convene;
    Watching with prayer and much selfdenial, I’ll triumph at last, there’s nothing between.
    ", "Nothing Between. ","F. A. Clark. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Broader than the ocean wide,
            stronger than its highest tide,
            Deeper than its measuring rod,
            Are the promises of God
    
    \n\nCho-:\nThey are sure if you only believe,
            they are sure if you only believe
            They are sure if you only believe,
            God’s promises are sure.
    
    \n\n2. Firmer than mountain high,
            higher than the distant sky,
            Thou this earth should pass away,
            yet God’s promises will stay.
    
    \n\n3.When this world is wrapped in flame,
            and the judge his own shall name,
            When the judgement day is passed,
            yet the promises will last.
    
    \n\n4. While eternal years roll on,
            through the ages yet to come,
            Still God’s promises are true
            and we’ll find them ever new.
    ", "God's Promises Are Sure. ","C. B. Widmeyer. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Will your anchor hold in the storms of life,
    when the clouds unfold their wings of strife?
    When the strong tides lift, and the cables strain,
    will your anchor drift, or firm remain?
    
            \n\n[Refrain:]\nWe have an anchor that keeps the soul
    steadfast and sure while the billows roll;
    fastened to the Rock which cannot move,
    grounded firm and deep in the Saviour’s love!
    
            \n\n2.It is safely moored, ’twill the storm withstand,
    For ’tis well secured by the Savior’s hand;
    And the cables, passed from His heart to mine,
    Can defy that blast, thro’ strength divine.
    
            \n\n3. It will firmly hold in the Straits of Fear—
    When the breakers have told that the reef is near;
    Though the tempest rave and the wild winds blow,
    Not an angry wave shall our bark o/’erflow.
    
            \n\n4. It will surely hold in the Floods of Death—
    When the waters cold chill our latest breath,
    On the rising tide it can never fail,
    While our hopes abide within the Veil.
    
            \n\n5. When our eyes behold through the gath’ring night
    The city of gold, our harbor bright,
    We shall anchor fast by the heav’nly shore,
    With the storms all past forevermore.
    ", "We Have an Anchor. ","P. J. Owens & W. J. Kirkpatrick. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I remember when my burdens rolled away;
    I had carried them for years, night and day.
    When I sought the blessed Lord,
    And I took Him at His word,
    Then at once all my burdens rolled away.
    
            \n\nCho-:\nRolled away, rolled away,
    I am happy since my burdens rolled away.
    Rolled away, rolled away,
    I am happy since my burdens rolled away.
    
            \n\n2. I remember when my burdens rolled away;
    That I feared would never leave, night or day.
    Jesus showed to me the loss,
    So I left them at the cross;
    I was glad when my burdens rolled away.
    
            \n\n3. I remember when my burdens rolled away,
    That had hindered me for years, night and day.
    As I sought the throne of grace,
    Just a glimpse of Jesus’ face,
    And I knew that my burdens could not stay.
    
            \n\n4. I am singing since my burdens rolled away;
    There’s a song within my heart night and day.
    I am living for my King,
    And with joy I shout and sing:
    “Hallelujah, all my burdens rolled away!”
    ", "My Burdens Rolled Away. ","Minnie A. Steele. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. My heart was distressed ’neath Jehovah’s dread frown,
    And low in the pit where my sins dragged me down;
    I cried to the Lord from the deep miry clay,
    Who tenderly brought me out to golden day.
    
            \n\nRefrain:\nHe brought me out of the miry clay,
    He set my feet on the Rock to stay;
    He puts a song in my soul today,
    A song of praise, hallelujah!
    
            \n\n2. He placed me upon the strong Rock by His side,
    My steps were established and here I’ll abide;
    No danger of falling while here I remain,
    But stand by His grace until the crown I gain.
    
            \n\n3. He gave me a song, ’twas a new song of praise;
    By day and by night its sweet notes I will raise;
    My heart’s overflowing, I’m happy and free;
    I’ll praise my Redeemer, Who has rescued me.
    
            \n\n4.I’ll sing of His wonderful mercy to me,
    I’ll praise Him till all men His goodness shall see;
    I’ll sing of salvation at home and abroad,
    Till many shall hear the truth and trust in God.
    
            \n\n5. I’ll tell of the pit, with its gloom and despair,
    I’ll praise the dear Father, who answered my prayer;
    I’ll sing my new song, the glad story of love,
    Then join in the chorus with the saints above.
    ", "He Brought me Out. ","Henry J. Zelley. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. O think of a home over there,
    By the side of the river of light,
    Where the saints, all immortal and fair,
    Are robed in their garments of white.
    
            \n\nCho:-\nOver there, over there,
    O think of a home over there,
    Over there, over there, over there,
    O think of a home over there.
    
            \n\n2. O think of the friends over there,
    Who before us the journey have trod,
    Of the songs that they breathe on the air,
    In their home in the palace of God.
    
            \n\n3. My Saviour is now over there,
    There my kindred and friends are at rest,
    Then away from my sorrow and care
    Let me fly to the land of the blest.
    
            \n\n4. I\'ll soon be at home over there,
    For the end of my journey I see,
    Many dear to my heart, over there,
    Are watching and waiting for me.
    ", "The Home Over There. ","D. W. C. Huntington. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Far away in the depths of my spirit tonight
    Rolls a melody sweeter than psalm;
    In celestial-like strains it unceasingly falls
    O’er my soul like an infinite calm.
    
            \n\nRefrain:\nPeace, peace, wonderful peace,
    Coming down from the Father above!
    Sweep over my spirit forever, I pray
    In fathomless billows of love!
    
            \n\n2. What a treasure I have in this wonderful peace,
    Buried deep in the heart of my soul,
    So secure that no power can mine it away,
    While the years of eternity roll!
    
            \n\n3. I am resting tonight in this wonderful peace,
    Resting sweetly in Jesus’ control;
    For I’m kept from all danger by night and by day,
    And His glory is flooding my soul!
    
            \n\n4. And I think when I rise to that city of peace,
    Where the Author of peace I shall see,
    That one strain of the song which the ransomed will sing
    In that heavenly kingdom will be:
    
            \n\n5. Ah soul, are you here without comfort and rest,
    Marching down the rough pathway of time?
    Make Jesus your friend ere the shadows grow dark;
    Oh, accept this sweet peace so sublime!
    ", "Wonderful Peace. ","W. D. Corneil.  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Hiding in the Rock, where we are blest;
    Hiding in the Rock of Ages;
    Hiding in the Rock where we find rest,
    Hiding in the Rock of Ages.
    
            \n\nRefrain:\nHiding in the Rock,
    Safe from ev’ry shock;
    Hiding in the Rock,
    I am Hiding in the Rock of Ages.
    
            \n\n2. Hiding in the Rock that has been riv’n,
    Hiding in the Rock of Ages;
    Hiding in the Rock, sweet peace is giv’n,
    Hiding in the Rock of Ages. [Refrain]
    
            \n\n3. Hiding in the Rock, a safe retreat,
    Hiding in the Rock of Ages;
    Hiding in the Rock, in Him complete,
    Hiding in the Rock of Ages. [Refrain]
    
            \n\n4. Hiding in the Rock, the storms without,
    Hiding in the Rock of Ages;
    Hiding in the Rock, we sing and shout,
    Hiding in the Rock of Ages. [Refrain]
    
            \n\n5. Hiding in the Rock, no harm can come,
    Hiding in the Rock of Ages;
    Hiding in the Rock till we get home,
    Hiding in the Rock of Ages. [Refrain]
    ", "Hiding in the Rock. ","L. C. Hall. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Standing on the promises of Christ my King,
    Through eternal ages let His praises ring,
    Glory in the highest, I will shout and sing,
    Standing on the promises of God.
    
            \n\nRefrain:\nStanding, standing,
    Standing on the promises of God my Savior;
    Standing, standing,
    I’m standing on the promises of God.
    
    
            \n\n2. Standing on the promises that cannot fail,
    When the howling storms of doubt and fear assail,
    By the living Word of God I shall prevail,
    Standing on the promises of God.
    
            \n\n3. Standing on the promises of Christ the Lord,
    Bound to Him eternally by love’s strong cord,
    Overcoming daily with the Spirit’s sword,
    Standing on the promises of God.
    
            \n\n4. Standing on the promises I cannot fall,
    List’ning every moment to the Spirit’s call,
    Resting in my Savior as my all in all,
    Standing on the promises of God.
    ", "Standing On the Promises. ","R. K. Carter. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Saved to the uttermost, I am the Lord’s;
        Jesus my Savior salvation affords;
        Gives me His Spirit a witness within,
        Whisp’ring of pardon, and saving from sin.
    
        \n\nCho:-\nSaved, saved, saved to the uttermost:
        Saved, saved by power divine;
        Saved, saved, I’m saved to the uttermost;
        Jesus the Savior is mine!
    
        \n\n2. Saved to the uttermost; Jesus is near;
        Keeping me safely, He casteth out fear;
        Trusting His promises, how I am blest;
        Leaning upon Him, how sweet is my rest.
    
        \n\n3. Saved to the uttermost: this I can say,
        “Once all was darkness, but now it is day.”
        Beautiful visions of glory I see;
        Jesus in brightness revealed unto me.
    
        \n\n4. Saved to the uttermost: cheerfully sing
        Loud hallelujahs to Jesus my King!
        Ransomed and pardoned, redeemed by His blood,
        Cleansed from unrighteousness, glory to God!
    ", "Saved to the Uttermost. ","W. J. Kirkpatrick. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. There is sunshine in my soul today,
    More glorious and bright
    Than glows in any earthly sky,
    For Jesus is my light.
            \n\nCho-:\nOh, there’s sunshine, blessed sunshine
    When the peaceful happy moments roll.
    When Jesus shows his smiling face,
    There is sunshine in the soul.
    
            \n\n2. There is music in my soul today,
    A carol to my King,
    And Jesus listening can hear
    The songs I cannot sing.
    
    
            \n\n3. There is springtime in my soul today,
    For when the Lord is near,
    The dove of peace sings in my heart,
    The flow’rs of grace appear.
    
    \n\n4. There is gladness in my soul today,
    And hope and praise and love,
    For blessings which he gives me now,
    For joys “laid up” above.
    ", "Sunshine in the Soul. ","E. E. Hewitt. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1.Waiting on the Lord, for the promise given;
    Waiting on the Lord, to send from Heaven;
    Waiting on the Lord, by our faith receiving;
    Waiting in the upper room.
    
            \n\nCho-:\nThe power! the power!
    Gives vict’ry over sin, and purity within;
    The power! the power!
    The pow’r they had at Pentecost.
    
            \n\n2.\tWaiting on the Lord, giving all to Jesus;
    Waiting on the Lord, till from sin He frees us;
    Waiting on the Lord for the heav’nly breezes;
    Waiting in the upper room.
    
            \n\n3.\tWaiting on the Lord, longing to mount higher;
    Waiting on the Lord, having great desire;
    Waiting on the Lord, for the heav’nly fire;
    Waiting in the upper room.
    ", "Waiting on the Lord. ","Charles F. Weigle. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Deeper In Thy love, O Jesus
    Doth my spirit cry to go.
    Until all my life is hidden
    Deep within the cleansing flow.
    
            \n\nCho-:\nDeeper in that holy life
    ‘Til I’m lost with Christ in God.
    Hidden with my blessed Lord
    While I walk this earthly sod.
    
            \n\n2. Humbly must I walk with Jesus
    In the paths that He doth lead.
    Never falter, never murmur
    Always to His voice give heed.
    
            \n\n3. Daily must I seek to please Him
    Whether it bring joy or pain.
    I shall know then just how blessed
    Is the worth of Heav’nly gain.
    
            \n\n4. Deeper, ‘til at last in Glory
    In His likeness I shall come.
    Where the saints of all the ages
    Shall receive their welcome home.
    ", "Deeper. ","L. C. Hall. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Give of your best to the Master;
    Give of the strength of your youth;
    Throw your soul’s fresh, glowing ardor
    Into the battle for truth.
    Jesus has set the example,
    Dauntless was He, young and brave;
            Give Him your loyal devotion;
    Give Him the best that you have.
    
            \n\nCho-:\nGive of your best to the Master;
    Give of the strength of your youth;
    Clad in salvation’s full armor,
    Join in the battle for truth.
    
            \n\n2. Give of your best to the Master;
    Give Him first place in your heart;
    Give Him first place in your service;
    Consecrate every part.
            Give, and to you will be given;
    God His beloved Son gave;
            Gratefully seeking to serve Him,
    Give Him the best that you have.
    
    \n\n3. Give of your best to the Master;
    Naught else is worthy His love;
    He gave Himself for your ransom,
    Gave up His glory above.
    Laid down His life without murmur,
    You from sin’s ruin to save;
            Give Him your heart’s adoration;
    Give Him the best that you have.
    ", "Give of Your Best to the Master. ","Howard B. Grose ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Tell me the story of Jesus,
    Write on my heart every word;
    Tell me the story most precious,
    Sweetest that ever was heard.
    Tell how the angels in chorus,
    Sang as they welcomed His birth,
    
            “Glory to God in the highest!
    Peace and good tidings to earth.”
           \n\nCho-:\nTell me the story of Jesus,
    Write on my heart every word;
    Tell me the story most precious,
    Sweetest that ever was heard.
    
    \n\n2. Fasting alone in the desert,
    Tell of the days that are past,
    How for our sins He was tempted,
    Yet was triumphant at last.
    Tell of the years of His labor,
    Tell of the sorrow He bore;
    He was despised and afflicted,
    Homeless, rejected and poor.
    
    \n\n3. Tell of the cross where they nailed Him,
    Writhing in anguish and pain;
    Tell of the grave where they laid Him,
    Tell how He liveth again.
    Love in that story so tender,
    Clearer than ever I see;
    Stay, let me weep while you whisper,
    “Love paid the ransom for me.”
    ", "Tell me the Story of Jesus. ","Frances J. Crosby ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("He leadeth me, O blessed thought!
    O words with heav’nly comfort fraught!
    Whate’er I do, where’er I be
    Still ’tis God’s hand that leadeth me.
    
            \n\nCho-:\nHe leadeth me, He leadeth me,
    By His own hand He leadeth me;
    His faithful foll’wer I would be,
    For by His hand He leadeth me.
    
            \n\n2. Sometimes ’mid scenes of deepest gloom,
    Sometimes where Eden’s bowers bloom,
    By waters still, o’er troubled sea,
    Still ’tis His hand that leadeth me.
    
            \n\n3. Lord, I would clasp Thy hand in mine,
    Nor ever murmur nor repine;
    Content, whatever lot I see,
    Since ’tis my God that leadeth me.
    
            \n\n4. And when my task on earth is done,
    When by Thy grace the vict’ry’s won,
    E’en death’s cold wave I will not flee,
    Since God through Jordan leadeth me.
    ", "He Leadeth Me. ","Joseph H. Gilmore ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. How to reach the masses, men of every birth,
    For an answer, Jesus gave the key:
    “And I, if I be lifted up from the earth,
    Will draw all men unto Me.”
    
            \n\nCho-:\nLift Him up, lift Him up;
    Still He speaks from eternity:
    “And I, if I be lifted up from the earth,
    Will draw all men unto Me.”
    
            \n\n2. Oh! the world is hungry for the Living Bread,
    Lift the Savior up for them to see;
    Trust Him, and do not doubt the words that He said,
    “I’ll draw all men unto Me.”
    
            \n\n3. Don’t exalt the preacher, don’t exalt the pew,
    Preach the Gospel simple, full, and free;
    Prove Him and you will find that promise is true,
    “I’ll draw all men unto Me.”
    
            \n\n4. Lift Him up by living as a Christian ought,
    Let the world in you the Savior see;
    Then men will gladly follow Him Who once taught,
    “I’ll draw all men unto Me.”
    ", "Lift Him Up. ","Johnson Oatman ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I will sing of my Redeemer,
    And His wondrous love to me;
    On the cruel cross He suffered,
    From the curse to set me free.
    
            \n\nCho-:\nSing, oh, sing of my Redeemer,
    With His blood He purchased me;
    On the cross He sealed my pardon,
    Paid the debt, and made me free.
    
            \n\n2. I will tell the wondrous story,
    How my lost estate to save,
    In His boundless love and mercy,
    He the ransom freely gave.
    
            \n\n3. I will praise my dear Redeemer,
    His triumphant pow’r I’ll tell,
    How the victory He giveth
    Over sin, and death, and hell.
    
            \n\n4. I will sing of my Redeemer,
    And His heav’nly love to me;
    He from death to life hath brought me,
    Son of God with Him to be.
    ", "My Redeemer. ","Philip P. Bliss ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Take the Name of Jesus with you,
    Child of sorrow and of woe,
    It will joy and comfort give you;
    Take it then, where’er you go.
    
            \n\nRefrain:\nPrecious Name, oh, how sweet!
    Hope of earth and joy of Heav’n;
    Precious Name, oh, how sweet!
    Hope of earth and joy of Heav’n.
    
            \n\n2. Take the Name of Jesus ever,
    As a shield from every snare;
    If temptations round you gather,
    Breathe that holy Name in prayer.
    
    \n\n3. Oh, the precious Name of Jesus!
    How it thrills our souls with joy,
    When His loving arms receive us,
    And His songs our tongues employ!
    
    \n\n4. At the Name of Jesus bowing,
    Falling prostrate at His feet,
    King of kings in heav’n we’ll crown Him,
    When our journey is complete.
    ", "Precious Name. ","Lydia O. Baxter  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. There’s a call comes ringing o’er the restless wave,
    “Send the light! Send the light!”
    There are souls to rescue, there are souls to save,
    Send the light! Send the light!
    
            \n\nRefrain:\nSend the light, the blessed Gospel light;
    Let it shine from shore to shore!
    Send the light, and let its radiant beams
    Light the world forevermore!
    
    \n\n2. We have heard the Macedonian call today,
    “Send the light! Send the light!”
    And a golden off’ring at the cross we lay,
    Send the light! Send the light!
    
    \n\n3. Let us pray that grace may everywhere abound,
    “Send the light! Send the light!”
    And a Christlike spirit everywhere be found,
    Send the light! Send the light!
    
            \n\n4. Let us not grow weary in the work of love,
    “Send the light! Send the light!”
    Let us gather jewels for a crown above,
    Send the light! Send the light!
    ", "Send the Light. ","Charles H. Gabriel ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. In the great triumphant morning,
    When we hear the Bridegroom cry,
    And the dead in Christ shall rise,
    We’ll be changed to life immortal,
    In the twinkling of an eye,
    And meet Jesus in the skies.
    \n\nRefrain\nWe shall all rise to meet Him,
    We shall all rise to greet Him,
    In the morning when the dead in Christ shall rise
    We shall all rise to meet Him,
    We shall all rise to greet Him,
    And shall have the marriage supper in the skies.
    
            \n\n2. In the great triumphant morning,
    What a happy time ’twill be,
    When the dead in Christ shall rise,
    When the Lord descends in glory,
    Sets His waiting children free,
    And we meet Him in the skies.
    
            \n\n3. In the great triumphant morning,
    When the harvest is complete,
    And the dead in Christ shall rise,
    We’ll be crowned with life immortal,
    Christ and all the loved ones meet,
    In the rapture in the skies.
    
            \n\n4. In the great triumphant morning,
    All the kingdom we’ll possess,
    Then the dead in Christ shall rise,
    Reign as kings and priests eternal,
    Under Christ forever blest,
    After meeting in the skies.
    ", "In the Great Triumphant Morning. ","R. E. Winsett ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Have you been to Jesus for the cleansing pow’r?
    Are you washed in the blood of the Lamb?
    Are you fully trusting in His grace this hour?
    Are you washed in the blood of the Lamb?
    
            \n\nCho-:\nAre you washed in the blood,
    In the soul-cleansing blood of the Lamb?
    Are your garments spotless? Are they white as snow?
    Are you washed in the blood of the Lamb?
    
            \n\n2. Are you walking daily by the Savior’s side?
    Are you washed in the blood of the Lamb?
    Do you rest each moment in the Crucified?
    Are you washed in the blood of the Lamb?
    
            \n\n3. When the Bridegroom cometh will your robes be white?
    Are you washed in the blood of the Lamb?
    Will your soul be ready for the mansions bright,
    And be washed in the blood of the Lamb?
    
            \n\n4. Lay aside the garments that are stained with sin,
    And be washed in the blood of the Lamb;
    There’s a fountain flowing for the soul unclean,
    Oh, be washed in the blood of the Lamb!
    ", "Are you Washed in the Blood. ","Elisha A. Hoffman ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. This sacred truth means more to me Than aught earth could ever be;
    That now, and through eternity, God Lives within my soul.
    
    \n\nCho-.\nWithin my soul, Within my soul is. His spirit guides, for He abides within my soul.
    
    \n\n2. God manifest in form of man, Once wro\’t redemption\’s wondrous plan; Completing what He then began, God lives Within my soul.
    
    \n\n3. Tho\’ I am weak, yet He is strong; He is my life, my joy, my song; and tho\’ the night be dark and long, God lives within my soul.
    
    \n\n4. And Heav\’n seems not some far-off place, Since even here, thru Saving grace, In fancy I behold His face who lives within my soul.
    
    \n\n5. When He shall bid me “come up hig\’r,” I\’ll sing with all the ransomed choir; But then than now He\’ll be no nigh\’r-God Lives within my soul!
    ", "God Lives Within My Soul. ","T. Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. When we walk with the Lord in the light of His Word,
    What a glory He sheds on our way!
    While we do His good will, He abides with us still,
    And with all who will trust and obey.
    
            \n\nRefrain:\nTrust and obey, for there’s no other way
    To be happy in Jesus, but to trust and obey.
    
           \n\n2. Not a shadow can rise, not a cloud in the skies,
    But His smile quickly drives it away;
    Not a doubt or a fear, not a sigh or a tear,
    Can abide while we trust and obey.
    
            \n\n3. Not a burden we bear, not a sorrow we share,
    But our toil He doth richly repay;
    Not a grief or a loss, not a frown or a cross,
    But is blessed if we trust and obey.
    
            \n\n4. But we never can prove the delights of His love
    Until all on the altar we lay;
    For the favor He shows, for the joy He bestows,
    Are for them who will trust and obey.
    
            \n\n5. Then in fellowship sweet we will sit at His feet,
    Or we’ll walk by His side in the way;
    What He says we will do, where He sends we will go;
    Never fear, only trust and obey.
    ", "Trust and Obey. ","John H. Sammis ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. There shall be showers of blessing:
    This is the promise of love;
    There shall be seasons refreshing,
    Sent from the Savior above.
        \n\nRefrain:\nShowers of blessing,
    Showers of blessing we need:
    Mercy-drops round us are falling,
    But for the showers we plead.
    
            \n\n2. There shall be showers of blessing,
    Precious reviving again;
    Over the hills and the valleys,
    Sound of abundance of rain.
    
            \n\n3. There shall be showers of blessing;
    Send them upon us, O Lord;
    Grant to us now a refreshing,
    Come, and now honor Thy Word.
    
            \n\n4. There shall be showers of blessing:
    Oh, that today they might fall,
    Now as to God we’re confessing,
    Now as on Jesus we call!
    ", "There Shall be Showers of Blessing. ","Daniel W. Whittle ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. When the trumpet of the Lord shall sound, and time shall be no more,
    And the morning breaks, eternal, bright and fair;
    When the saved of earth shall gather over on the other shore,
    And the roll is called up yonder, I’ll be there.
    
            \n\nRefrain:\nWhen the roll is called up yonder,
    When the roll is called up yonder,
    When the roll is called up yonder,
    When the roll is called up yonder, I’ll be there.
    
            \n\n2. On that bright and cloudless morning when the dead in Christ shall rise,
    And the glory of His resurrection share;
    When His chosen ones shall gather to their home in paradise,
    And the roll is called up yonder, I’ll be there.
    
            \n\n3. Let us labor for the Master from the dawn till setting sun,
    Let us talk of all His wondrous love and care;
    Then when all of life is over, and our work on earth is done,
    And the roll is called up yonder, I’ll be there.
    ", "When the Roll is Called Up Yonder. ","James M. Black ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. If I walk in the pathway of duty,
    If I work till the close of the day,
    I shall see the great King in His beauty,
    When I’ve gone the last mile of the way.
    
            \n\nRefrain:\nWhen I’ve gone the last mile of the way,
    I will rest at the close of the day;
    And I know there are joys that await me,
    When I’ve gone the last mile of the way.
    
            \n\n2. If for Christ I proclaim the glad story,
    If I seek for His sheep gone astray,
    I am sure He will show me His glory,
    When I’ve gone the last mile of the way.
    
            \n\n3. Here the dearest of ties we must sever,
    Tears of sorrow are seen every day;
    But no sickness, no sighing forever,
    When I’ve gone the last mile of the way.
    
    \n\n4. And if here I have earnestly striven,
    And have tried all His will to obey,
    ’Twill enhance all the rapture of heaven,
    When I’ve gone the last mile of the way.
    ", "The Last Mile of the Way. ","Johnson Oatman ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Once a sinner far from Jesus,
    I was perishing with cold,
    But the blessèd Savior heard me when I cried;
    Then He threw His robe around me,
    And He led me to His fold,
    And I’m living on the hallelujah side.
    
    \n\nRefrain\nOh, glory be to Jesus, let the hallelujahs roll;
    Help me ring the Savior’s praises far and wide,
    For I’ve opened up tow’rd heaven
    All the windows of my soul,
    And I’m living on the hallelujah side.
    
            \n\n2. Tho’ the world may sweep around me
    With her dazzle and her dreams,
    Yet I envy not her vanities and pride,
    For my soul looks up to heaven,
    Where the golden sunlight gleams,
    And I’m living on the hallelujah side. [Refrain]
    
            \n\n3. Not for all earth’s golden millions
    Would I leave this precious place,
    Tho’ the tempter to persuade me oft has tried,
    For I’m safe in God’s pavilion,
    Happy in His love and grace,
    And I’m living on the hallelujah side. [Refrain]
    
            \n\n4. Here the sun is always shining,
    Here the sky is always bright;
    ’Tis no place for gloomy Christians to abide,
    For my soul is filled with music
    And my heart with great delight,
    And I’m living on the hallelujah side. [Refrain]
    
            \n\n5. And upon the streets of glory,
    When we reach the other shore,
    And have safely crossed the Jordan’s rolling tide,
    You will find me shouting Glory
    Just outside my mansion door
    Where I’m living on the hallelujah side. [Refrain]
    ", "The Hallelujah Side. ","Johnson Oatman ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. my bridegroom is coming,
    I hear his sweet voice,
    “Make ready, I soon shall the there,”
     it thrills thru my spirit,
    And makes me rejoice, to
    Know we shall meet in the air.
    
    \n\nCho-:\nHe’s coming so soon,
    He’s coming so soon,
    O’ watch for it may be today!
    He’s coming so soon,
    He’s coming so soon;
    To take his beloved away.
    
    \n\n2. I hear speeding nearer
    His blest chariot wheels;
    So softly the turn in the skies,
    This world cannot hear them,
    He only reveals his truth
    To the spirit-touched eyes.
    
    \n\n3. He told me to always keep
    Watch and to pray,
    That he account me of those
    Made free to escape
    from the awful dismay
    That coming to all of his foes
    
    \n\n4. The bells sweetly chiming
    All over the land,
    My lord marriage supper proclaim;
    With oil in her and lamp in her hand,
    He’s fitting His bride for His name.
    ", "My Bridegroom is Coming. ","L. C. Hall  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Washed in the blood, by the spirit sealed, Christ in His word is to me revealed; Glory to God! in my soul doth shine God, my salvation, and His life is mine!
    
     \n\nCho-.\nWashed in the blood, Washed in the blood! Washed in the blood, in the soul cleansing Blood! Washed in the blood, Washed in the blood! Sealed in the scripture, and washed in the blood.
    
    \n\n2. Once I was blind, but behold, I see; God from above now hath shine into me; Cleansed from all sin, in His word I behold Wealth which can never, be compared to gold.
    
    \n\n3. Oh that the world might the Sav-ior see, That blessed Savior who saved poor me! O how the lost ones would come shouting home, Never, never, never, never more to roam!
    
    \n\n4. Washed in the blood! sinner, Come today; Jesus so free-ly the debt will pay; Come to His arms, to His arms of grace, Come now in meekness seek the Savior\’s face.
    ", "Washed in the Blood. ","C. P. Jones ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Would you live for Jesus, and be always pure and good?
    Would you walk with Him within the narrow road?
    Would you have Him bear your burden, carry all your load?
    Let Him have His way with thee.
    
            \n\nRefrain:\nHis pow’r can make you what you ought to be;
    His blood can cleanse your heart and make you free;
    His love can fill your soul, and you will see
    ’Twas best for Him to have His way with thee.
    
            \n\n2. Would you have Him make you free, and follow at His call?
    Would you know the peace that comes by giving all?
    Would you have Him save you, so that you need never fall?
    Let Him have His way with thee.
    
            \n\n3. Would you in His kingdom find a place of constant rest?
    Would you prove Him true in providential test?
    Would you in His service labor always at your best?
    Let Him have His way with thee.
    ", "His Way With Thee. ","Cyrus S. Nusbaum ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider (" 1. There was a time on earth when in the book of Heav’n
    An old account was standing for sins yet unforgiv’n;
    My name was at the top, and many things below—
    I went unto the Keeper, and settled long ago.
    
            \n\nRefrain:\nLong ago, long ago,
    Yes, the old account was settled long ago;
    And the record’s clear today, for He washed my sins away,
    When the old account was settled long ago.
    
            \n\n2. The old account was large, and growing every day,
    For I was always sinning, and never tried to pay;
    But when I looked ahead, and saw such pain and woe,
    I said that I would settle—I settled long ago.
    
            \n\n3. When at the Judgment bar I stand before my King,
            And He the book will open, He cannot find a thing; Then
            will my heart be glad, While tears of joy will flow,
            Because I had it settled, And settled long ago.
    
    
            \n\n4. O sinner, seek the Lord, Repent of all your sin,
    For thus He has command,If you would enter in;
    And then if you should live a hundred years below,
    Even here you’ll not regret it—you settled long ago.
    ", "An Old Account Settled. ","Frank M. Graham ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. They were in an upper chamber,
    They were all with one accord,
    When the Holy Ghost descended
    As was promised by the Lord.
    
            \n\nRefrain:\nO Lord, send the pow’r just now,
    O Lord, send the pow’r just now;
    O Lord, send the pow’r just now
    And baptize every one.
    
            \n\n2. Yes, the pow’r from Heav’n descended
    With the sound of rushing wind;
    Tongues of fire came down upon them,
    As the Lord said He would send.
    
            \n\n3. Yes, this old-time pow’r was given
    To our fathers who were true;
    This is promised to believers,
    And we all may have it too.
    ", "Old Time Power. ","Charles D. Tillman ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Have you found the great Physician,
      Jesus Christ of Galilee?
    He who bore our pain and sorrow,
      On the shameful, cruel tree?
    Still He heals the sick and suff’ring,
      As before He went away;
    For His word most plainly tells us,
      “He is just the same today.”
    
            \n\nCho-:\nHe is just the same today;
    As before He went away.
    Look to Him, believe and pray;
    Trust His word and then obey.
      “Praise God, He is just the same today.”
    
            \n\n2. Consecrate your life to Jesus,
      Spirit, soul, and body too;
    For “the Lord is for the body,”
      Every pow’r He gave to you.
    Let there be no reservation,
      Give the Lord full right of way;
    He will come and heal His temple,
      For He is the same today.
    
            \n\n3. Do you doubt God’s will to heal you?
      Take His word and ask for light;
    If you seek in deep contrition,
      He will guide your heart aright.
    Do not fear to claim His promise,
      He will not your trust betray;
    When on earth He gladly healed them,
      And He is the same today.
    
            \n\n4. Oh! I’m glad to tell you, suff’rer,
      Christ has more than healing too;
    Life abundant overflowing,
      He will gladly give to you.
    Step out boldly, claim His fulness,
      Let your sadness flee away;
    When on earth He made them joyful,
      And He is the same today.
    ", "He is Just the Same Today. ","L. C. Hall ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Hear the battle cry that\’s sounding thro\’ the land, Calling for the soldiers of our King; They are going forward led by God\’s own hand, While this mighty name they sing.
    
    \n\nCho-.\nJesus! Jesus! Marching on in Jesus\’ mighty name! Victory! Victory! Victory in Jesus name!
    
    \n\n2. There is Victory when we go in Jesus\’ name, Heralding the tidings of His love; Telling to the lost ones, that His power\’s the same, for He\’s reigning now above.
    
    \n\n3. If the battle Rages and the foe be strong, Victory is coming in His name; For we fight the battle of right against wrong, And His conq\’ring power proclaim.
    
    \n\n4. Won\’t you join this army of the King of kings? Royal, Noble soldiers of our God; They will come rejoicing, while all heaven rings, Up the heights the saints have trod.
    ", "Victory in Jesus' Name. ","L. C. Hall ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Life is like a mountain railway,
    With an engineer that’s brave;
    We must make the run successful,
    From the cradle to the grave;
    Watch the curves, the fills, the tunnels;
    Never falter, never fail;
    Keep your hands upon the throttle,
    And your eyes upon the rail.
    
            \n\nRefrain:\nBlessed Savior, Thou wilt guide us,
    Till we reach that blissful shore,
    Where the angels wait to join us
    In Thy praise forevermore.
    
    
            \n\n2. You will roll up grades of trial;
    You will cross the bridge of strife;
    See that Christ is your conductor
    On this lightning train of life;
    Always mindful of obstruction,
    Do your duty, never fail;
    Keep your hands upon the throttle,
    And your eyes upon the rail.
    
    \n\n3. You will often find obstructions,
    Look for storms and wind and rain;
    On a fill, or curve, or trestle
    They will almost ditch your train;
    Put your trust alone in Jesus,
    Never falter, never fail;
    Keep your hands upon the throttle,
    And your eyes upon the rail.
    
    \n\n4. As you roll across the trestle,
    Spanning Jordan’s swelling tide,
    You behold the Union Depot
    Into which your train will glide;
    There you’ll meet the Sup’rintendent,
    God the Father, God the Son,
    With the hearty, joyous plaudit,
    “Weary Pilgrim, welcome home.”
    ", "Life's Railway to Heaven. ","E. R. Snow, M. E. Abbey ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. All that thrills my soul is Jesus,
            Everyday and every hour;
            Jesus and His free salvation,
            Jesus and His mighty power.
    
            \n\nRefrain:\nAll that thrills my soul is Jesus,
    He is more than life to me;
    And the fairest of ten thousand
    In my blessed Lord I see.
    
            \n\n2. His is love beyond all knowledge,
            His is grace beyond degree,
            Mercy higher than the heaven,
            Deeper than the deepest sea.
    
            \n\n3. Every need His hand sup-pli-eth,
            Every good in Him I see;
            And the strength He gives His weak ones
            Is sufficient unto me.
    
            \n\n4. What a wonderful redemption!
    Never can a mortal know,
    How my sin, tho red like crimson,
    Can be whiter than the snow.
    
            \n\n5. In you everlasting city
            With the ransomed I will sing,
            And forever and forever,
            Praise and glorify the King.
    ", "All That Trills My Soul. ","Thore Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. A friend of Jesus! Oh, what bliss
    That one so weak as I
    Should ever have a Friend like this
    To lead me to the sky!
    
            \n\nRefrain:\nFriendship with Jesus!
    Fellowship divine!
    Oh, what blessed, sweet communion!
    Jesus is a Friend of mine.
    
            \n\n2. A Friend when other friendships cease,
    A Friend when others fail,
    A Friend who gives me joy and peace,
    A Friend when foes assail!
    
            \n\n3. A friend to lead me in the dark,
            A friend who knows the way;
            A friend to steer my weak,frail bark,
            A friend my debts to pay.
    
    \n\n4. A Friend when sickness lays me low,
    A Friend when death draws near,
    A Friend as through the vale I go,
    A Friend to help and cheer!
    
    \n\n5. A Friend when life\'s rough voyage is o\'er,
            A friend when death is past;
            A friend to greet on heaven\'s shore,
            A friend when home at last.
    ", "Friendship With Jesus. ","Joseph C. Ludgate ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. “Whosoever heareth,” shout, shout the sound!
    Send the blessed tidings all the world around;
    Spread the joyful news wherever man is found:
    “Whosoever will may come.”
    
            \n\nRefrain:\n“Whosoever will, whosoever will,”
    Send the proclamation over vale and hill;
    ’Tis a loving Father calls the wand’rer home:
    “Whosoever will may come.”
    
    \n\n2. Whosoever cometh need not delay,
    Now the door is open, enter while you may;
    Jesus is the true, the only Living Way:
    “Whosoever will may come.”
    
    \n\n3. “Whosoever will,” the promise secure,
    “Whosoever will,” forever must endure;
    “Whosoever will,” ’tis life forevermore:
    “Whosoever will may come.”
    ", "Whosoever Will. ","Philip P. Bliss ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. \’Tis a sweet and glorious tho’t that comes to me,
    I’ll live on, yes, I’ll live on;
    Jesus saved my soul from death, and now I’m free,
    I’ll live on, yes, I’ll live on.
    
            \n\nRefrain\nI’ll live on, yes, I’ll live on.
    Thru eternity I’ll live on;
    I’ll live on, yes, I’ll live on,
    Thru eternity I’ll live on.
    
            \n\n2. When my body’s slumb’ring in the cold, cold clay,
    I’ll live on, yes, I’ll live on;
    There to sleep in Jesus till the judgment day,
    I’ll live on, yes, I’ll live on.
    
            \n\n3. When the world’s on fire and darkness veils the sun,
    I’ll live on, yes, I’ll live on;
    Men will cry and to the rocks and mountains run,
    I’ll live on, yes, I’ll live on.
    
            \n\n4. In the glory-land, with Jesus on the throne,
    I’ll live on, yes, I’ll live on;
    Thru eternal ages singing, home, sweet home,
    I’ll live on, yes, I’ll live on.
    ", "I'll Live On. ","Thomas J. Laney ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Death hath no terrors for the blood bought one,
    O glory hallelujah to the Lamb!
    The boasted vict’ry of the grave is gone,
    O glory hallelujah to the Lamb!
    
            \n\nRefrain:\nJesus rose from the dead,
    Rose triumphant as He said,
    Snatched the vict’ry from the grave,
    Rose again our souls to save—
    O glory hallelujah to the Lamb!
    
            \n\n2. Our souls die daily to the world and sin,
    O glory hallelujah to the Lamb!
    By the Spirit’s power as He dwells within,
    O glory hallelujah to the Lamb! [Refrain]
    
            \n\n3. We seek a city far beyond this vale,
    O glory hallelujah to the Lamb!
    Where joys celestial never, never fail,
    O glory hallelujah to the Lamb! [Refrain]
    
            \n\n4. We’ll then press forward to the heav’nly land,
    O glory hallelujah to the Lamb!
    Nor mind the troubles met on ev’ry hand,
    O glory hallelujah to the Lamb! [Refrain]
    
            \n\n5. We’ll rise some day just as our Savior rose,
    O glory hallelujah to the Lamb!
    Till then shall death be but a calm repose,
    O glory hallelujah to the Lamb! [Refrain]
    ", "Death Hath No Terrors. ","Charles Price Jones ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. When sorrows cloud hangs low, and hides the golden light, Reach Thou Thy hand below, and guide me thru the night; With Thy great wings of Love, Oh God over shadow me; till safe above, with wings of Love overshadow me.
    
    \n\nCho-.\nOvershadow me, overshadow me! With Thy great wings of Peace and Love; \’Till I am safe in heaven with Thee, With wings divine, overshadow me.
    
    \n\n2. When storms around me rage and foes assail my soul, Pilot my humble bark o\’er rock and hidden shoal; With Thy great wings of Peace, Oh God, overshadow me; \’Till storms are past, with wings of Peace o\’er-shadow me.
    
    \n\n3. When tempted, Lord, to tread in paths denying Thee, Spirit of strength, abide continually in me. With Thy great wings of Faith, Oh God, overshadow me; \’Till faith is sight, with wings of Faith overshadow me.
    
    \n\n4. When friendless and alone, tho\’ rough may seem the way, Be Thou my Sav-ior near till close of life\’s short day. With Thy great wings of Hope, O God, over Shadow me; Oh Lord my God, with wings of Hope overshadow me.
    ", "overshadow Me. ","L. C. Hall ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I belong to the King, I’m a child of His love,
    I shall dwell in His palace so fair;
    For He tells of its bliss in yon heaven above,
    And His children in splendor shall share.
    
            \n\nCho-:\nI belong to the King, I’m a child of His love,
    And he never forsaketh His own;
    He will call me some day to His palace above,
    I shall dwell by His glorified throne.
    
            \n\n2. I belong to the King, and He loves me I know,
    For His mercy and kindness, so free,
    Are unceasingly mine wheresoever I go,
    And my refuge unfailing is He. (Chorus)
    
            \n\n3. I belong to the King, and His promise is sure,
    That we all shall be gathered at last
    In His kingdom above, by life’s waters so pure,
    When this life with its trials is past. (Chorus)
    ", "I Belong to the King. ","Ida L. Reed ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Blessed be the fountain of blood,
    To a world of sinners revealed;
    Blessed be the dear Son of God—
    Only by His stripes we are healed.
    Though I’ve wandered far from His fold,
    Bringing to my heart pain and woe,
    Wash me in the blood of the Lamb,
    And I shall be whiter than snow.
    
            \n\nRefrain:\nWhiter than the snow,
    Whiter than the snow;
    Wash me in the blood of the Lamb,
    And I shall be whiter than snow.
    
            \n\n2. Thorny was the crown that He wore,
    And the cross His body o’ercame;
    Grievous were the sorrows He bore,
    But He suffered thus not in vain.
    May I to that fountain be led,
    Made to cleanse my sins here below;
    Wash me in the blood that He shed,
    And I shall be whiter than snow.
    
            \n\n3. Father, I have wandered from Thee,
    Often has my heart gone astray;
    Crimson do my sins seem to me—
    Water cannot wash them away.
    Jesus, to that fountain of Thine,
    Leaning on Thy promise, I go;
    Cleanse me by Thy washing divine,
    And I shall be whiter than snow.
    ", "Blessed Be the Fountain. ","Eden R. Latta ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. When I survey the wondrous cross
    On which the Prince of glory died,
    My richest gain I count but loss,
    And pour contempt on all my pride.
    
    \n\n2. See, from His head, His hands, His feet,
            Sorrow and love flow mingled down;Did
            ever such love and sorrow meet
            Or thorns compose so rich a crown!
    
            \n\n3. Since I, who was undone and lost,
            Have pardon thru my Savior\'s blood, For-
            bid it, Lord! that I should boast,
            Save in the cross of Christ my God.
    
            \n\n4. His dying crimson, like a robe,
    Spreads o’er His body on the tree;
    Then I am dead to all the globe,
    And all the globe is dead to me.
    
            \n\n5. Were the whole realm of nature mine,
    That were a present far too small;
    Love so amazing, so divine,
    Demands my soul, my life, my all.
    
            \n\n6. To Christ, Who won for sinners grace
    By bitter grief and anguish sore,
    Be praise from all the ransomed race
    Forever and forevermore.
    ", "When I Survey the Wondrous Cross. ","Isaac Watts ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. What a wonderful, wonderful Savior,
    Who would die on the cross for me!
    Freely shedding His precious lifeblood,
    That the sinner might be made free.
    
            \n\nRefrain\nHe was nailed to the cross for me,
    He was nailed to the cross for me;
    On the cross crucified for me He died;
    He was nailed to the cross for me.
    
    \n\n2. Thus He left His heavenly glory,
    To accomplish His Father’s plan;
    He was born of the virgin Mary,
    Took upon Him the form of man.
    
    \n\n3. He was wounded for our transgressions,
    And He carried our sorrows, too;
    He’s the Healer of ev’ry sickness,
    This He came to the world to do.
    
    \n\n4. So He gave His life for others
    In Redeeming this world from sin,
    And He’s gone to prepare a mansion,
    That at last we may enter in.
    ", "He was Nailed to the Cross for me. ","F. A. Graves ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. O I love to walk with Jesus
    Like the disciples did of old,
    When He gathered them about him
    And the blessèd tidings told;
    How He came to bring deliverance
    To the captives in distress,
    Take away our ev’ry burden,
    Giving perfect peace and rest.
    
            \n\nRefrain\nI will follow where He leadeth;
    I will pasture where He feedeth.
    I will follow all the way, Lord.
    I will follow Jesus ev’ry day.
    
            \n\n2. O I love to walk with Jesus
    Like the man of long ago
    Who had tarried by the wayside
    Near the gates of Jericho,
    Jesus heard his cry for mercy,
    Gave him back his sight that day,
    And immediately he followed
    Jesus all along the way. [Refrain]
    
            \n\n3. O I love to walk with Jesus
    All the way to Calv’ry’s brow,
    Gaze upon that scene of suffering
    While my tears of sorrow flow.
    There He tells me how He loves me—
    Takes my ev’ry sin away;
    So I follow Him so gladly,
    Lead me anywhere He may. [Refrain]
    
            \n\n4. O sometime I’ll walk with Jesus
    In that land of endless day,
    When our journey here is over
    And we’ve reached our home to stay.
    Then I’ll walk with him forever,
    Sing His praises o’er and o’er,
    And with all the saints in glory
    Love, and worship, and adore. [Refrain]
    ", "I Love to Walk with Jesus. ","Charles F. Weigle ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Oh, they tell me of a home far beyond the skies,
    Oh, they tell me of a home far away;
    Oh, they tell me of a home where no storm clouds rise,
    Oh, they tell me of an unclouded day;
            Oh, the land of cloudless day,
    Oh, the land of an unclouded sky,
    Oh, they tell me of a home where no storm clouds rise,
    Oh, they tell me of an unclouded day.
    
    \n\n2. Oh, they tell me of a home where my friends have gone,
    Oh, they tell me of that land far away,
            Where the tree of life in eternal bloom
    Sheds its fragrance through the unclouded day;
            Oh, the land of cloudless day,
    Oh, the land of an unclouded sky,
    Oh, they tell me of my friends by the tree of life,
            In the land of the unclouded day.
    
    \n\n3. Oh, they tell me of a King in His beauty there,
    And they tell me that mine eyes shall behold;
    Where He sits on the throne that is whiter than snow,
    In the city that is made of gold;
            Oh, that land mine eyes shall see,
    Oh, that land of an unclouded sky,
            O they tell me of the King on his snow-white throne,
            In the land of the unclouded day.
    
            \n\n4. Oh, they tell me that He smiles on His children there,
    And His smile drives their sorrows all away;
    And they tell me that no tears ever come again,
    In that lovely land of unclouded day;
            Oh, that land of lovely smiles,
    Oh, the smiles of His love-beaming eye;
    Oh, the King in his beauty invites me there,
    To the land of the unclouded day.
    ", "The Unclouded Day. ","Josiah K. Alwood ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. O happy day, that fixed my choice
    On Thee, my Savior and my God!
    Well may this glowing heart rejoice,
    And tell its raptures all abroad.
    
            \n\nRefrain:\nHappy day, happy day,
    When Jesus washed my sins away!
    He taught me how to watch and pray,
    And live rejoicing every day:
    Happy day, happy day,
    When Jesus washed my sins away!
    
            \n\n2. O happy bond, that seals my vows
    To Him Who merits all my love!
    Let cheerful anthems fill His house,
    While to that sacred shrine I move.
    
            \n\n3.\'Tis done: the great transaction’s done!
    I am the Lord’s and He is mine;
    He drew me and I followed on;
    Charmed to confess the voice divine.
    ", "O Happy Day ","Philip Doddridge ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I came to Jesus, weary, worn, and sad,
    He took my sins away,
    He took my sins away.
    And now His love has made my heart so glad,
    He took my sins away.
    
            \n\nCho-:\nHe took my sins away,
    He took my sins away,
    And keeps me singing every day!
    I’m so glad He took my sins away.
    He took my sins away.
    
            \n\n2. The load of sin was more than I could bear.
    He took them all away,
    He took them all away.
    And now on Him I roll my every care,
    He took my sins away.
    
    \n\n3. No condemnation have I in my heart,
    He took my sins away,
    He took my sins away.
    His perfect peace He did to me impart,
    He took my sins away.
    
    \n\n4. If you will come to Jesus Christ today,
    He’ll take your sins away,
    He’ll take your sins away,
    And keep you happy in His love each day,
    He’ll take your sins away. (Chorus)
    ", "He Took My Sins Away. ","Margaret J. Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Christ will me His aid afford,
    Never to fall, never to fall,
    While I find my precious Lord,
    Sweeter than all, sweeter than all.
    
            \n\nRefrain\nJesus is now, and ever will be,
    Sweeter than all the world to me,
    Since I heard His loving call,
    Sweeter than all, sweeter than all.
    
            \n\n2. I will follow all the way,
    Hearing Him call, hearing Him call,
    Finding Him, from day to day,
    Sweeter than all, sweeter than all. [Refrain]
    
            \n\n3. Tho’ a vessel I may be,
    Broken and small, broken and small,
    Yet His blessings fall on me,
    Sweeter than all, sweeter than all. [Refrain]
    
            \n\n4. When I reach the crystal sea,
    Voices will call, voices will call,
    But my Savior’s voice will be
    Sweeter than all, sweeter than all. [Refrain]
    ", "Sweeter Than All. ","Johnson Oatman ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. There were ninety and nine that safely lay
    In the shelter of the fold;
    But one was out on the hills away,
    Far off from the gates of gold.
    Away on the mountains wild and bare;
    Away from the tender Shepherd’s care.
    
    \n\n2. “Lord, Thou hast here Thy ninety and nine;
    Are they not enough for Thee?”
    But the Shepherd made answer: “This of Mine
    Has wandered away from Me.
    And although the road be rough and steep,
    I go to the desert to find My sheep.”
    
            \n\n3. But none of the ransomed ever knew
    How deep were the waters crossed;
    Nor how dark was the night the Lord passed through
    Ere He found His sheep that was lost.
    Out in the desert He heard its cry;
    ’Twas sick and helpless and ready to die.
    
            \n\n4. “Lord, whence are those blood-drops all the way,
    That mark out the mountain’s track?”
    “They were shed for one who had gone astray
    Ere the Shepherd could bring him back.”
    “Lord, whence are Thy hands so rent and torn?”
    “They’re pierced tonight by many a thorn.”
    
            \n\n5. And all through the mountains, thunder-riv’n,
    And up from the rocky steep,
    There arose a glad cry to the gate of heav’n,
    “Rejoice! I have found My sheep!”
    And the angels echoed around the throne,
    “Rejoice, for the Lord brings back His own!”
    ", "The Ninety and Nine. ","Elizabeth C. Clephane ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. God’s abiding peace is in my soul today,
    Yes, I feel it now, yes, I feel it now;
    He has taken all my doubts and fears away,
    Though I cannot tell you how.
    
            \n\nRefrain:\nIt is mine, mine, blessed be His Name!
    He has given peace, perfect peace to me;
    It is mine, mine, blessed be His Name!
    Mine for all eternity!
    
            \n\n2. He has wrought in me a sweet and perfect rest,
    In my raptured heart I can feel it now;
    He each passing moment keeps me saved and blest,
    Floods with light my heart and brow.
    
        \n\n3. He has given me a never failing joy,
    Oh, I have it now! oh, I have it now!
    To His praise I will my ransomed pow’rs employ,
    And renew my grateful vow.
        \n\n4. Oh, the love of God is comforting my soul,
    For His love is mine, yes, His love is mine!
    Waves of joy and gladness o’er my spirit roll,
    Thrilling me with life divine.
    ", "It Is Mine. ","Elisha A. Hoffman ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Wonderful name of Jesus the Lord! Sweet to my soul its fragrance out poured; Wonderful tho\’t revealed in God\’s word: Jesus is still the same.
    
    \n\nCho-.\nJesus! Jesus! Wonderful name of Jesus! Jesus! Jesus! Wonderful name, Oh precious name!
    \n\n2. No other name to mortals is given By which we reach the portals of heaven, Name by whose power sin feathers are riv\’n; Jesus is still the same.
    \n\n3. No other name two saints is revealed, by which the sick and suffering are healed; No other name such blessing can yield, Jesus is still the same.
    
    \n\n4. Oh, I would join the rapturous throng, Praising His name in jubilant song; I would for aye it\’s glories prolong. Jesus is still the same.
    
    ", "Wonderful Name of Jesus. ","Thoro Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. You may know this day all your guilt forgiven, Every burden gone, every Fet-ter riv\’n, That you\’ve started well on the way to heav\’n- You may find salvation if you will.
    
    \n\nCho-.\nYou may find Salvation if you will. You may find salvation if you will; “Sinner, look and live” is the message still; You may find this great salvation if you only will.
    
    \n\n2. You may taste the Sweets of redeeming love, With your name enrolled in life\’s book above, And a piece divine every moment prove- You may find Salvation if you will.
    
    \n\n3. If you but renounce all your selfish pride For the sake of One who for Sinners died, In the sight of God you are Justified-You may find Salvation if you will.
    
    \n\n4. By the Spirit\’s power of the heavenly birth You may live beyond all the ills of earth, Seeking treasures rich of eternal worth-You may find Salvation if you will.
    
    \n\n5. Now this very hour bid farewell to sin, Let the light of God and His glory in, And a Holy life hasten to begin-You may find Salvation if you will.
    ", "You May Find Salvation. ","Thoro Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Jesus has a table spread
    Where the saints of God are fed,
    He invites His chosen people, “Come and dine”;
    With His manna He doth feed
    And supplies our every need:
    Oh, ’tis sweet to sup with Jesus all the time!
    
            \n\nRefrain:\n“Come and dine,” the Master calleth, “Come and dine”;
    You may feast at Jesus’ table all the time;
    He Who fed the multitude, turned the water into wine,
    To the hungry calleth now, “Come and dine.”
    
            \n\n2. The disciples came to land,
    Thus obeying Christ’s command,
            For the Master called unto them, “Come and dine”;
    There they found their heart’s desire,
    Bread and fish upon the fire;
    Thus He satisfies the hungry every time.
    
            \n\n3. Soon the Lamb will take His bride
    To be ever at His side,
    All the host of heaven will assembled be;
    Oh, ’twill be a glorious sight,
    All the saints in spotless white;
    And with Jesus they will feast eternally.
    ", "Come and Dine. ","Charles B. Widmeyer ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Oh, the joy of sins forgiv’n,
    Oh, the bliss the blood-washed know,
    Oh, the peace akin to Heav’n,
    Where the healing waters flow.
    
            \n\nRefrain\nWhere the healing waters flow,
    Where the joys celestial glow,
    Oh, there’s peace and rest and love,
    Where the healing waters flow!
    
            \n\n2. Now with Jesus crucified,
    At His feet I’m resting low;
    Let me evermore abide
    Where the healing waters flow. [Refrain]
    
            \n\n3. O, this precious, perfect love!
    How it keeps the heart aglow,
    Streaming from the fount above,
    Where the healing waters flow. [Refrain]
    
            \n\n4. Oh, to lean on Jesus’ breast,
    While the tempests come and go!
    Here is blessèd peace and rest,
    Where the healing waters flow. [Refrain]
    
            \n\n5. Cleansed from every sin and stain,
    Whiter than the driven snow,
    Now I sing my sweet refrain,
    Where the healing waters flow. [Refrain]
    ", "The Healing Waters. ","H. H. Heimar ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Kneel at the cross,
    Christ will meet you there,
    Come while He waits for you;
    List to His voice,
    Leave with Him your care
    And begin life anew.
    
            \n\nRefrain\nKneel at the cross, (at the cross)
    Leave (ev\'ry care) ev\'ry care;
    Kneel at the cross (at the cross)
    Jesus will meet you there. (meet you there)
    
    \n\n2. Kneel at the cross,
    There is room for all
    Who would His glory share;
    Bliss there awaits,
    Harm can ne\'er befall
    Those who are anchored there.
    
    \n\n3. Kneel at the cross
    Give your idols up,
    Look unto realms above;
    Turn not away
    To Life\'s sparkling cup,
    Trust only in His love.
    ", "Kneel At the Cross. ","Charles E. Moody ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. To me the Holy Ghost is giv’n,
    An earnest of the joys of Heav’n;
    Since He has taken full control,
    I’ve Pentecost in my soul.
    
            \n\nRefrain\nI’ve Pentecost in my soul,
    I’ve Pentecost in my soul;
    The Spirit has come, has come to abide;
    I’ve Pentecost in my soul.
    
            \n\n2. The Spirit dwells my heart within
    And breaks the pow’r of pardoned sin;
    Tho’ o’er my head dark clouds may roll,
    I’ve Pentecost in my soul.
    
    \n\n3. O holy rest! O peace sublime!
    He reigns within me all the time;
    He purifies and makes me whole—
    I’ve Pentecost in my soul.
    
            \n\n4. Then seek this Gift without delay,
    Receive the Holy Ghost today;
    Yield all to His benign control,
    Let Pentecost fill your soul!
    O yes! [Refrain]
    ", "Pentecost in My Soul. ","Thoro Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Time\'s clock is striking the hour
    Jesus will soon descend
    Clothed in the garments of power
    The reign of sin to end
    Then will this anthem be ringing
    Like to a mighty flood
    Then \'round the throne we\'ll be singing
    Glory and praise to God
            \n\nCho-:\nGlory glory to God
    Thus will the ransomed sing
    Glory glory to God
    The everlasting King
    Praise Him alleluia
    To that eternal sphere
    We are waiting
    For our translating
    The time is near
            \n\n2. In those bright mansions supernal
    Death cannot enter there
    Ages on ages eternal
    His likeness we shall bear
    There will the once brokenhearted
    Rest in the spirit know
    Sorrow forever departed
    Gladness shall overflow
    
    \n\n3. Sighing forever is ended
    Foes shall oppress no more
    Voices in worship are blended
    To Him whom all adore
    Bathed in the light soft and tender
    Sealed for eternity
    Praise to the Lamb we will render
    Worthy of praise is He
    
    \n\n4. Beautiful wonderful story
    Jesus Himself the light
    There in the kingdom of glory
    Never shall fall the night
    Now I am singing of heaven
    While here I wage the strife
    Then will the victors be given
    Crowns of eternal life
    ", "Eternal Rest. ","Thoro Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. All for Jesus, all for Jesus!
    All my being’s ransomed pow’rs:
    All my thoughts and words and doings,
    All my days and all my hours.
    
           \n\nCho-:\nAll for Jesus! All for Jesus!
    All my days and all my hours;
    All for Jesus! All for Jesus!
    All my days and all my hours.
    Let my hands perform His bidding,
    Let my feet run in His ways;
    Let my eyes see Jesus only,
    Let my lips speak forth His praise.
    
            \n\n2. All for Jesus! All for Jesus!
    Let my lips speak forth His praise;
    All for Jesus! All for Jesus!
    Let my lips speak forth His praise.
    Since my eyes were fixed on Jesus,
    I’ve lost sight of all beside;
    So enchained my spirit’s vision,
    Looking at the Crucified.
    
            \n\n3. All for Jesus! All for Jesus!
    Looking at the Crucified;
    All for Jesus! All for Jesus!
    Looking at the Crucified.
    Oh, what wonder! How amazing!
    Jesus, glorious King of kings,
    Deigns to call me His beloved,
    Lets me rest beneath His wings.
    
            \n\n4. All for Jesus! All for Jesus!
    Resting now beneath His wings;
    All for Jesus! All for Jesus!
    Resting now beneath His wings.
    ", "All for Jesus. ","Mary D. James ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Rejoicing in Hope, believing His word, we look for the coming of Jesus our Lord; But \’till he return through heaven\’s blue dome, He bids His disciples, “Hold fast till I come.”
    
    \n\nCho-.\n”Hold fast till I come,” hear the word of the master; The o-mens increase, and the signs thicken faster; “Until I restore your lost Eden home, Give heed to the message, Hold fast till I come.”
    
    \n\n2. When o\’er all the world the storm shall Prevail, The love and devotion of many shall fail, Be steadfast and true, nor slumber like some, And yours is the Kingdom- “Hold fast till I come.”
    
    \n\n3. Then let all our lamps burn steady and bright, Lest, faithless, He find us, asleep in the night; Endure to the end and watch unto prayer- For Jesus\’ appearing His own must prepare.
    
    \n\n4. He\’ll banish all sin, all pain and distress, The lion of Judah the nations will bless; With crowns on our heads, with harp and with palm, We\’ll sing Hallelujah to God and the Lamb.
    ", "Hold Fast 'Till I Come. ","Thoro Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I’ve found a friend in Jesus, He’s everything to me,
    He’s the fairest of ten thousand to my soul;
    The Lily of the Valley, in Him alone I see
    All I need to cleanse and make me fully whole.
    In sorrow He’s my comfort, in trouble He’s my stay;
    He tells me every care on Him to roll.
    
            \n\nRefrain:\nHe’s the Lily of the Valley, the Bright and Morning Star,
    He’s the fairest of ten thousand to my soul.
    
            \n\n2. He all my grief has taken, and all my sorrows borne;
    In temptation He’s my strong and mighty tow’r;
    I’ve all for Him forsaken, and all my idols torn
    From my heart and now He keeps me by His pow’r.
    Though all the world forsake me, and Satan tempt me sore,
    Through Jesus I shall safely reach the goal.
    
            \n\n3. He’ll never, never leave me, nor yet forsake me here,
    While I live by faith and do His blessed will;
    A wall of fire about me, I’ve nothing now to fear,
    From His manna He my hungry soul shall fill.
    Then sweeping up to glory to see His blessed face,
    Where rivers of delight shall ever roll.
    ", "The Lily of the Valley. ","Thoro Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. All the way my Savior leads me,
    What have I to ask beside?
    Can I doubt His tender mercy,
    Who through life has been my Guide?
    Heav’nly peace, divinest comfort,
    Here by faith in Him to dwell!
    For I know, whate’er befall me,
    Jesus doeth all things well;
    For I know, whate’er befall me,
    Jesus doeth all things well.
    
            \n\n2. All the way my Savior leads me,
    Cheers each winding path I tread,
    Gives me grace for every trial,
    Feeds me with the living Bread.
    Though my weary steps may falter
    And my soul athirst may be,
    Gushing from the Rock before me,
    Lo! A spring of joy I see;
    Gushing from the Rock before me,
    Lo! A spring of joy I see.
    
            \n\n3. All the way my Savior leads me,
    Oh, the fullness of His love!
    Perfect rest to me is promised
    In my Father’s house above.
    When my spirit, clothed immortal,
    Wings its flight to realms of day
    This my song through endless ages:
    Jesus led me all the way;
    This my song through endless ages:
    Jesus led me all the way.
    ", "All the Way my Saviour Leads Me. ","R. Lowry ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Alas! and did my Savior bleed
    And did my Sov’reign die?
    Would He devote that sacred head
    For such a worm as I?
    
            \n\nRefrain:\nAt the cross, at the cross where I first saw the light,
    And the burden of my heart rolled away,
    It was there by faith I received my sight,
    And now I am happy all the day!
    
            \n\n2. Was it for crimes that I had done
    He groaned upon the tree?
    Amazing pity! grace unknown!
    And love beyond degree!
    
            \n\n3. But drops of grief can ne’er repay
    The debt of love I owe:
    Here, Lord, I give myself away,
    ’Tis all that I can do.
    
            \n\n4. Well might the sun in darkness hide
    And shut his glories in,
    When Christ, the mighty Maker died,
    For man the creature’s sin.
    
            \n\n5. Thus might I hide my blushing face
    While His dear cross appears,
    Dissolve my heart in thankfulness,
    And melt my eyes to tears.
    ", "At the Cross. ","R. E. Hudson ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. When peace, like a river, attendeth my way,
    When sorrows like sea billows roll;
    Whatever my lot, Thou hast taught me to say,
    It is well, it is well with my soul.
    
            \n\nRefrain:\nIt is well with my soul,
    It is well, it is well with my soul.
    
            \n\n2. Though Satan should buffet, though trials should come,
    Let this blest assurance control,
    That Christ hath regarded my helpless estate,
    And hath shed His own blood for my soul.
    
            \n\n3. My sin—oh, the bliss of this glorious thought!—
    My sin, not in part but the whole,
    Is nailed to the cross, and I bear it no more,
    Praise the Lord, praise the Lord, O my soul!
    
            \n\n4. And Lord, haste the day when the faith shall be sight,
    The clouds be rolled back as a scroll;
    The trump shall resound, and the Lord shall descend,
    Even so, it is well with my soul.
    
            \n\n5. For me, be it Christ, be it Christ hence to live:
    If Jordan above me shall roll,
    No pang shall be mine, for in death as in life
    Thou wilt whisper Thy peace to my soul.
            \n\n6. But, Lord, ’tis for Thee, for Thy coming we wait,
    The sky, not the grave, is our goal;
    Oh, trump of the angel! Oh, voice of the Lord!
    Blessed hope, blessed rest of my soul!
    ", "It is Well with My Soul. ","P. P. Bliss ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. There are faces dear that I hold in mem’ry,
    Though I lost them long ago;
    But the face of One “altogether lovely”
    Is the fairest face I know.
    
            \n\nRefrain:\nHis face will outshine them all,
    His face will outshine them all;
    Glory to the Lamb, hallelujah!
    His face will outshine them all.
    
            \n\n2. There were voices sweet over Bethl’hem singing
    When the Savior Christ was born;
    And the golden harps of the angels ringing
    Ushered in that holy morn. [Refrain]
    
            \n\n3. There are friendly hands unto me extended
    When I seem to miss my way;
    But the pierced hand of the Man of Calv’ry
    Leadeth on to realms of day. [Refrain]
    
            \n\n4. There are wondrous scenes lying all around me,
    Golden gleams o’er land and sea;
    But when Jesus comes in the clouds of heaven,
    O what glory that will be. [Refrain]
    ", "His Face Will Outshine Them All. ","Thore Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. On the cross of Calvary,
    Jesus died for you and me;
    There He shed His precious blood,
    That from sin we might be free.
    O the cleansing stream does flow,
    And it washes white as snow!
    It was for me that Jesus died
    On the cross of Calvary.
    
            \n\nRefrain:\nOn Calvary! On Calvary!
    It was for me that Jesus died
    On the cross of Calvary.
    
    
            \n\n2. O what wondrous, wondrous love
    Brought me down at Jesus’ feet!
    O such wondrous, dying love
    Asks a sacrifice complete!
    Here I give myself to Thee,
    Soul and body, Thine to be;
    It was for me Thy blood was shed
    On the cross of Calvary.
    
            \n\n3. Take me, Jesus, I am Thine,
    Wholly Thine, forevermore;
    Blessèd Jesus, Thou art mine,
    Dwell within forevermore;
    Cleanse, O cleanse my heart from sin,
    Make and keep me pure within!
    It was for this Thy blood was shed
    On the cross of Calvary.
    
            \n\n4. Clouds and darkness veiled the sky
    When the Lord was crucified;
    It is finished! was His cry
    When He bowed His head and died.
    With this cry from Calv’ry’s tree
    All the world may now go free;
    It was for this that Jesus died
    On the cross of Calvary.
    ", "On the Cross of Calvary. ","W. J. K ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I have been to Jesus who has cleansed my soul,
    I’ve been washed in the blood of the Lamb,
    By the blood of Jesus I have been made whole,
    I’ve been washed in the blood of the Lamb.
    
            \n\nRefrain:\nI’ve been washed, I’ve been washed,
    I’ve been washed in the blood of the Lamb,
    And my robe is spotless; it is white as snow,
    I’ve been washed in the blood of the Lamb.
    
            \n\n2. I am daily trusting Jesus at my side,
    I’ve been washed in the blood of the Lamb,
    I am sweetly resting in the Crucified,
    I’ve been washed in the blood of the Lamb. [Refrain]
    
            \n\n3. I am working in the vineyard of the Lord,
    I’ve been washed in the blood of the Lamb,
    I am trusting in the promise of His word,
    I’ve been washed in the blood of the Lamb. [Refrain]
    
            \n\n4. I am list’ning now to hear the Bridegroom’s voice,
    I’ve been washed in the blood of the Lamb,
    How His coming will each faithful heart rejoice,
    I’ve been washed in the blood of the Lamb. [Refrain]
    
            \n\n5. I am watching for the coming of the Lord,
    I’ve been washed in the blood of the Lamb,
    He will come according to His faithful word,
    I’ve been washed in the blood of the Lamb. [Refrain]
    ", "I've Been Washed in the Blood. ","D. H. Dortch ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Talk of His power, Maker of all-Stars in their courses obeying His call; Tell of His Triumph Over her foe: Wondrous deliverance His people may know.
    
    \n\nCho-.\nTalk of His power, tell of His love, Sing of the Sav-ior who came from above; Talk of His power, Tell of His love, Sing of the Sav-ior who came from above.
    
    \n\n2. Tell of His mercy, Speak of His love, Robed in the splendor of Heaven above; Laud ye His goodness, show forth His praise; Load hallelujahs with gratitude raise.
    
    \n\n3. Sing of salvation sent from on high- He that believe-eth Shall never more die; For our Redemption, Jesus the Lord Died, and is risen: Oh Glory to God!
    
    \n\n4. Power to heal us, Power to save, Power to deliver From death and the grave, Power to keep us Pure every hour; Sing of His goodness And talk of His power.
    ", "Talk of His Power. ","L. C. Hall ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("    1. John saw the multitude, and cried, Behold the Lamb of God! \’A sacrifice will He provide\’-Behold the Lamb of God!
    \n\nCho-.\nBehold the Lamb of God! Behold the Lamb of God! For He also can save your soul -Behold the Lamb of God!
    \n\n2. Extended on the shameful tree, Behold the Lamb of God! He pays the price, and man is free! Behold the Lamb of God.
    \n\n3. He bore the woes of all the world; Behold the Lamb of God! The darts of sin on Him were hurled: Behold the Lamb of God!
    \n\n4. “Himself took our infirmities”-Behold the Lamb of God! “In His own body on the tree”- Behold the Lamb of God!
    \n\n5. Once Slain on rugged Calvary, Behold the Lamb of God! Man\’s only mediator He: Behold the Lamb of God.
    \n\n6. “\’Tis finished!” Hear the Savior say; Behold the Lamb of God! He Bears our load of guilt away; Behold the Lamb of God.
    \n\n7. In Heaven above He intercedes; Behold the Lamb of God! The merits of His blood He pleads: Behold the Lamb of God!
    \n\n8. He\’s coming back to earth again: Behold the Lamb of God! And none will dare revile Him then: Behold the Lamb of God!
    \n\n9. Now let Him undertake your case: Behold the Lamb of God! Oh trust His “more abundant” grace: Behold the Lamb of God!
    ", "Behold the Lamb of God. ","L. C. Hall ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. The wedding feast draws near,
            The marriage of the Son; The bride in rich array,
            Then clothed in spotless white, no wrinkles in her robe,
            Will soon her garments don; And filled with perfect love,
            She comes from all the globe.
    
    \n\nCho-.\nWill you be there? Will you be there? To meet Him in the air! From North and South, from West and East, They gather for the feast.
     \n\n2. They come with glory crowned, Washed in the Savior\’s blood; Bright jewels of the Lord, And in the spirit sealed, Since they the gift received; And cleansed in calv\’ry\’s flood, For they have walked with Him, Nor was the Spirit grieved.
    
    \n\n3. The multitudes look on, Behold the wondrous sight; Christ and His bride are one And as the ages roll, forever, ever on, For aye, in spotless white; Each happy, raptured soul Will still be feasting on.
    \n\n4. And all eternity Will never see them part, For they will still be one Be ready for that feast, For soon the call will sound; In love and work and heart; The Bride herself will go, and with her Lord be crowned.
    ", "The Wedding Feast. ","L. C. Hall ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I’ve a home prepared where the saints abide,
    Just over in the glory land;
    And I long to be by my Savior’s side,
    Just over in the glory land.
    
            \n\nRefrain:\nJust over in the glory land,
    I’ll join the happy angel band,
    Just over in the glory land;
    Just over in the glory land,
    There with the mighty host I’ll stand,
    Just over in the glory land.
    
            \n\n2. I am on my way to those mansions fair,
    Just over in the glory land;
    There to sing God’s praise and His glory share,
    Just over in the glory land.
    
            \n\n3. What a joyful thought that my Lord I’ll see,
    Just over in the glory land;
    And with kindred saved, there forever be,
    Just over in the glory land.
    
            \n\n4. With the blood-washed throng I will shout and sing,
    Just over in the glory land;
    Glad hosannas to Christ, the Lord and King,
    Just over in the glory land.
    ", "Just Over in the Glory-Land. ","R. E. Winsett, Dayton, Tenn ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. We are often tossed and driv’n
    On the restless sea of time,
    Somber skies and howling tempests
    Oft succeed a bright sunshine,
    In that land of perfect day,
    When the mists have rolled away,
    We will understand it better by and by.
    
            \n\nRefrain:\nBy and by when the morning comes,
    All the saints of God are gathered home,
    We’ll tell the story how we’ve overcome:
    For we’ll understand it better by and by.
    
            \n\n2. We are often destitute
    Of the things that life demands,
    Want of food and want of shelter,
    Thirsty hills and barren lands,
    We are trusting in the Lord,
    And according to His word,
    We will understand it better by and by.
    
    \n\n3. Trials dark on ev’ry hand,
    And we cannot understand,
    All the ways that God would lead us
    To that blessed Promised Land;
    But He guides us with His eye
    And we’ll follow till we die.
    For we’ll understand it better by and by.
    
    \n\n4. Temptations, hidden snares
    Often take us unawares,
            And our hearts are made to bleed
    For many a thoughtless word or deed,
    And we wonder why the test
    When we try to do our best.
    But we’ll understand it better by and by.
    ", "We'll Understand It Better. ","F. A. Clark ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Come from the loathsome way of sin,
    Hide you in the blood of Jesus;
    Come, for the Lord will take you in,
    Hide you in the blood of Jesus.
    
            \n\nRefrain:\nO hide you in the blood,
    For the storms are raging high,
    O hide you in the blood,
    Till the dangers pass you by.
    
            \n\n2. Come to the shelter’s safe retreat,
    Hide you in the blood of Jesus;
    Come, for the storms around you beat,
    Hide you in the blood of Jesus. [Refrain]
    
            \n\n3. Come, for your sins the Lord has bled,
    Hide you in the blood of Jesus;
    Come, tho’ they be like crimson red,
    Hide you in the blood of Jesus. [Refrain]
    
            \n\n4. Come to the Lord, He’s calling thee,
    Hide you in the blood of Jesus;
    Come, let His Spirit set you free,
    Hide you in the blood of Jesus. [Refrain]
    
            \n\n5. Come, there is safety in the blood,
    Hide you in the blood of Jesus;
    Now plunge beneath the crimson flood,
    Hide you in the blood of Jesus. [Refrain]
    
            \n\n6. Come now, believing in the Lord,
    Hide you in the blood of Jesus;
    Trusting His grace and precious Word,
    Hide you in the blood of Jesus. [Refrain]
    ", "Hide You in the Blood. ","St. Louis Assembly ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Blessed assurance, Jesus is mine!
        Oh, what a foretaste of glory divine!
        Heir of salvation, purchase of God,
        Born of His Spirit, washed in His blood.
    
        \n\nRefrain:\nThis is my story, this is my song,
        Praising my Savior all the day long;
        This is my story, this is my song,
        Praising my Savior all the day long.
    
        \n\n2. Perfect submission, perfect delight,
        Visions of rapture now burst on my sight;
        Angels, descending, bring from above
        Echoes of mercy, whispers of love.
    
        \n\n3. Perfect submission, all is at rest,
        I in my Savior am happy and blest,
        Watching and waiting, looking above,
        Filled with His goodness, lost in His love.
    ", "Blessed Assurance. ","Fanny J. Crosby ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. You have longed for sweet peace,
    And for faith to increase,
    And have earnestly, fervently prayed;
    But you cannot have rest,
    Or be perfectly blest,
    Until all on the altar is laid.
    
            \n\nRefrain:\nIs your all on the altar of sacrifice laid?
    Your heart does the Spirit control?
    You can only be blest,
    And have peace and sweet rest,
    As you yield Him your body and soul.
    
            \n\n2. Would you walk with the Lord,
    In the light of His word,
    And have peace and contentment alway?
    You must do His sweet will,
    To be free from all ill,
    On the altar your all you must lay.
    
            \n\n3. Oh, we never can know
    What the Lord will bestow
    Of the blessings for which we have prayed,
    Till our body and soul
    He doth fully control,
    And our all on the altar is laid.
    
            \n\n4. Who can tell all the love
    He will send from above,
    And how happy our hearts will be made;
    Of the fellowship sweet
    We shall share at His feet,
    When our all on the altar is laid.
    ", "Is Your All on the Altar. ","Rev. Elisha A. Hoffman ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Am I a soldier of the cross,
    A follower of the Lamb,
    And shall I fear to own His cause,
    Or blush to speak His Name?
    
    
            \n\nRefrain:\nAnd when the battle’s over
    We shall wear a crown!
    Yes, we shall wear a crown!
    Yes, we shall wear a crown!
    And when the battle’s over
    We shall wear a crown
    In the new Jerusalem
    Wear a crown (wear a crown)
    Wear a crown (wear a crown)
    Wear a bright and shining crown;
    And when the battle’s over
    We shall wear a crown
    In the new Jerusalem.
    
            \n\n2. Must I be carried to the skies
    On flowery beds of ease,
    While others fought to win the prize,
    And sailed through bloody seas?
    
            \n\n3. Are there no foes for me to face?
    Must I not stem the flood?
    Is this vile world a friend to grace,
    To help me on to God?
    
            \n\n4. Sure I must fight if I would reign,
    Increase my courage, Lord;
    I\'ll bear the toil, endure the pain,
    Supported by Thy Word.
    ", "And When the Battle's Over. ","Isaac Watts ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider (" 1. O the wondrous pow’r  of the
    Saviour’s love unto sinners
    Is now revealed; ev’ry  ling\'ring pain
    Jesus can remove
    Praise Lord by
    His stripes we are healed.
    
    \n\nCho-:\nWe are healed.
    By His stripes we are healed,
    We are healed;
    On His guiltless head all
    Our sins were laid,
    By his Stripes we are healed.
    
    \n\n2. There in Pilate’s hall,
    See the guiltless one:
    How hearts of His foes
    Were steeled’ gainst the
    Gift of God, His beloved son
    Praise the Lord
    By His stripes we are healed.
    
    \n\n3. His atoning blood
    Still Avails today:
    For the kingdom be saved
    Sealed; in the opened
    Fount wash thine sins away
    Praise the Lord
    By his stripes we are healed.
    
    \n\n4. Turn, o turn from sin,
    Let the saviour in,
    Bow the heart, in contrition
    Yield to the spirit’s pow’r
    This accepted hour,
    Praise the Lord
    By His stripes we are healed.
    ", "By His Stripes We Are Healed. ","Thoro Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. My soul in sad exile was out on life’s sea,
    So burdened with sin and distressed,
    Till I heard a sweet voice, saying, “Make Me your choice”;
    And I entered the “Haven of Rest”!
    
            \n\nRefrain:\nI’ve anchored my soul in the “Haven of Rest,”
    I’ll sail the wide seas no more;
    The tempest may sweep over wild, stormy, deep,
    In Jesus I’m safe evermore.
    
            \n\n2. I yielded myself to His tender embrace,
    In faith taking hold of the Word,
    My fetters fell off, and I anchored my soul;
    The “Haven of Rest” is my Lord.
    
            \n\n3. The song of my soul, since the Lord made me whole,
    Has been the old story so blest,
    Of Jesus, who’ll save whosoever will have
    A home in the “Haven of Rest.”
    
            \n\n4. How precious the thought that we all may recline,
    Like John, the beloved so blest,
    On Jesus’ strong arm, where no tempest can harm,
    Secure in the “Haven of Rest.”
    
            \n\n5. Oh, come to the Savior, He patiently waits
    To save by His power divine;
    Come, anchor your soul in the “Haven of Rest,”
    And say, “My Beloved is mine.”
    ", "The Haven of Rest. ","Geo. D. Moore ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Onward, Christian soldiers, marching as to war,
    With the cross of Jesus going on before.
    Christ, the royal Master, leads against the foe;
    Forward into battle see His banners go!
    
            \n\nRefrain:\nOnward, Christian soldiers, marching as to war,
    With the cross of Jesus going on before.
    
            \n\n2. Like a mighty army moves the church of God;
    Brothers, we are treading where the saints have trod.
    We are not divided, all one body we,
    One in hope and doctrine, one in charity.
    
            \n\n3. Crowns and thrones may perish, kingdoms rise and wane,
    But the church of Jesus constant will remain.
    Gates of hell can never ’gainst that church prevail;
    We have Christ’s own promise, and that cannot fail.
    
            \n\n4. Onward then, ye people, join our happy throng,
    Blend with ours your voices in the triumph song.
    Glory, laud, and honor unto Christ the King,
    This through countless ages men and angels sing.
    ", "Onward, Christian Soldiers. ","Sabine Baring-Gould ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. I am happy today, and the sun shines bright,
    The clouds have been rolled away;
    For the Savior said, whosoever will
    May come with Him to stay.
    
            \n\nRefrain:\n“Whosoever” surely meaneth me,
    Surely meaneth me, oh, surely meaneth me;
    “Whosoever” surely meaneth me,
    “Whosoever” meaneth me.
    
            \n\n2. All my hopes have been raised, oh, His Name be praised,
    His glory has filled my soul;
    I’ve been lifted up, and from sin set free,
    His blood has made me whole.
    
            \n\n3. Oh, what wonderful love, oh, what grace divine,
    That Jesus should die for me;
    I was lost in sin, for the world I pined,
    But now I am set free.
    ", "Whosoever Meaneth Me. ","J. Edwin McConnell ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Central’s never “busy,” always on the line;
    You may hear from heaven almost any time;
    ’Tis a royal service, free for one and all;
    When you get in trouble, give this royal line a call.
    
            \n\nRefrain:\nTelephone to glory, oh, what joy divine!
    I can feel the current moving on the line,
    Built by God the Father for His loved and own,
    We may talk to Jesus through this royal telephone.
    
            \n\n2. There will be no charges, telephone is free,
    It was built for service, just for you and me;
    There will be no waiting on this royal line,
    Telephone to glory always answers just in time.
    
            \n\n3. Fail to get the answer, Satan’s crossed your wire,
    By some strong delusion, or some base desire;
    Take away obstructions, God is on the throne,
    And you’ll get your answer through this royal telephone.
    
            \n\n4. If your line is “grounded,” and connection true
    Has been lost with Jesus, tell you what to do:
    Prayer and faith and promise mend the broken wire,
    Till your soul is burning with the Pentecostal fire.
    
            \n\n5. Carnal combinations cannot get control
    Of this line to glory, anchored in the soul;
    Storm and trial cannot disconnect the line,
    Held in constant keeping by the Father’s hand divine.
    ", "The Royal Telephone. ","F. M. Lehman ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Oh, spread the tidings ’round, wherever man is found,
    Wherever human hearts and human woes abound;
    Let every Christian tongue proclaim the joyful sound:
    The Comforter has come!
    
            \n\nRefrain:\nThe Comforter has come, the Comforter has come!
    The Holy Ghost from Heav’n, the Father’s promise giv’n;
    Oh, spread the tidings ’round, wherever man is found—
    The Comforter has come!
    
            \n\n2. The long, long night is past, the morning breaks at last,
    And hushed the dreadful wail and fury of the blast,
    As o’er the golden hills the day advances fast!
    The Comforter has come!
    
            \n\n3. Lo, the great King of kings, with healing in His wings,
    To every captive soul a full deliv’rance brings;
    And through the vacant cells the song of triumph rings;
    The Comforter has come!
    
            \n\n4. O boundless love divine! How shall this tongue of mine
    To wond’ring mortals tell the matchless grace divine—
    That I, a child of hell, should in His image shine!
    The Comforter has come!
    
            \n\n5. Sing till the echoes fly above the vaulted sky,
    And all the saints above to all below reply,
    In strains of endless love, the song that ne’er will die:
    The Comforter has come!
    ", "The Comforter Has Come. ","F. Bottome ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Have thy affections been nailed to the cross?
    Is thy heart right with God?
    Dost thou count all things for Jesus but loss?
    Is thy heart right with God?
    
            \n\nRefrain:\nIs thy heart right with God,
    Washed in the crimson flood,
    Cleansed and made holy, humble and lowly,
    Right in the sight of God?
    
            \n\n2. Hast thou dominion o’er self and o’er sin?
    Is thy heart right with God?
    Over all evil without and within?
    Is thy heart right with God?
    
            \n\n3. Is there no more condemnation for sin?
    Is thy heart right with God?
    Does Jesus rule in the temple within?
    Is thy heart right with God?
    
            \n\n4. Are all thy pow’rs under Jesus’ control?
    Is thy heart right with God?
    Does He each moment abide in thy soul?
    Is thy heart right with God?
            \n\n5. Art thou now walking in heaven\'s pure light?
            Is thy heart right with God?
    Is thy soul wearing a garment of white?
            Is thy heart right with God?
    ", "Is Thy Heart Right With God. ","Elisha A. Hoffman ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. When I first found Jesus, something o\'er me stole,
    Like lightning it went through me, and glory filled my soul;
    Salvation made me happy, and took my fears away,
    And when I meet old Satan, to him I always say:
    
            \n\nRefrain:\nI am determined to hold out to the end,
    Jesus is with me, on Him I can depend;
    And I know I have salvation, for I feel it in my soul;
    I am determined to hold out to the end.
    
            \n\n2. Satan, he was angry, said he\'d soon be back:
    Just let the path get narrow, and he will lose the track;
    But I\'m so full of glory, my Lord I always find,
    And I just say to Satan, Old man, get thee behind.
    
            \n\n3. This old-time religion makes me sometimes shout,
            I don\'t have time to gossip, nor any time to pout,
            They say that I\'m too noisy, but when these blessings flow,
            I shout, O hallelujah, I want the world to know.
    
            \n\n4. When I hear the trumpet sounding in the sky,
    And see the mountains trembling, to Heaven I will fly;
    For Jesus will be calling, there\'ll be no time to mend,
    With joy I\'ll go up singing, I\'ve held out to the end.",
     "I am Determined To Hold Out. ","C. S. Hamilton ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Oh, the unsearchable riches of Christ,
    Wealth that can never be told!
    Riches exhaustless of mercy and grace,
    Precious, more precious than gold!
    
            \n\nRefrain:\nPrecious, more precious,
    Wealth that can never be told!
    Oh, the unsearchable riches of Christ!
    Precious, more precious than gold.
    
            \n\n2. Oh, the unsearchable riches of Christ!
    Who shall their greatness declare?
    Jewels whose luster our lives may adorn,
    Pearls that the poorest may wear!
    
            \n\n3. Oh, the unsearchable riches of Christ!
    Freely, how freely they flow,
    Making the souls of the faithful and true
    Happy wherever they go!
    
            \n\n4. Oh, the unsearchable riches of Christ!
    Who would not gladly endure
    Trials, afflictions, and crosses on earth,
    Riches like these to secure!
    ", "Unsearchable Riches. ","F. J. Crosby, R. Sweney ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I have heard of a land on the faraway strand,
    ’Tis a beautiful home of the soul;
    Built by Jesus on high, where we never shall die,
    ’Tis a land where we never grow old.
    
            \n\nRefrain:\nNever grow old, never grow old,
    In a land where we’ll never grow old;
    Never grow old, never grow old,
    In a land where we’ll never grow old.
    
            \n\n2. In that beautiful home where we’ll never more roam,
    We shall be in the sweet by and by;
    Happy praise to the King through eternity sing,
    ’Tis a land where we never shall die.
    
            \n\n3. When our work here is done and the life-crown is won,
    And our troubles and trials are o’er;
    All our sorrow will end, and our voices will blend,
    With the loved ones who’ve gone on before.
    ", "Where We'll Never Grow Old. ","J. C. Moore ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. There comes to my heart one sweet strain,
    A glad and a joyous refrain;
    I sing it again and again--
    Sweet peace, the gift of God’s love.
    
            \n\nCho-:\nPeace, peace, sweet peace!
    Wonderful gift from above!
    O wonderful, wonderful peace!
    Sweet peace, the gift of God’s love!
    
            \n\n2. Thro\' Christ on the cross peace was made,
    My debt by His death was all paid;
    No other foundation is laid
    For peace, the gift of God’s love. (Chorus)
    
            \n\n3. When Jesus as Lord I had crowned,
    My heart with this peace did abound;
    In Him the rich blessing I found--
    Sweet peace, the gift of God’s love. (Chorus)
    
            \n\n4. In Jesus for peace I abide,
    And as I keep close to His side,
    There’s nothing but peace doth betide--
    Sweet peace, the gift of God’s love. (Chorus)
    ", "Sweet Peace, the Gift of God's Love. ","P. P. Bilhorn ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. ’Tis the grandest theme through the ages rung;
    ’Tis the grandest theme for a mortal tongue;
    ’Tis the grandest theme that the world e’er sung,
    “Our God is able to deliver thee.”
    
            \n\nRefrain:\nHe is able to deliver thee,
    He is able to deliver thee;
    Though by sin oppressed, go to Him for rest;
    “Our God is able to deliver thee.”
    
            \n\n2. ’Tis the grandest theme in the earth or main;
    ’Tis the grandest theme for a mortal strain;
    ’Tis the grandest theme, tell the world again,
    “Our God is able to deliver thee.”
    
            \n\n3. ’Tis the grandest theme, let the tidings roll,
    To the guilty heart, to the sinful soul;
    Look to God in faith, He will make thee whole,
    “Our God is able to deliver thee.”
    ", "He Is Able To Deliver Thee. ","W. A. Ogden ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I will sing the wondrous story
    Of the Christ Who died for me;
    How He left His home in glory
    For the cross of Calvary.
    
            \n\nRefrain:\nYes, I’ll sing the wondrous story
    Of the Christ Who died for me,
    Sing it with the saints in glory,
    Gathered by the crystal sea.
    
            \n\n2. I was lost, but Jesus found me,
    Found the sheep that went astray,
    Threw His loving arms around me,
    Drew me back into His way.
    
            \n\n3. I was bruised, but Jesus healed me,
    Faint was I from many a fall,
    Sight was gone, and fears possessed me,
    But He freed me from them all.
    
            \n\n4. Days of darkness still come o’er me,
    Sorrow’s path I often tread,
    But His presence still is with me;
    By His guiding hand I’m led.
    
            \n\n5. He will keep me till the river
    Rolls its waters at my feet;
    Then He’ll bear me safely over,
    Where the loved ones I shall meet.
    ", "I Will Sing The Wondrous Story. ","Peter Bilhorn ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. My sheep know My voice and the path that I take,
    They follow wherever I go;
    My sheep know My voice and come at my call,
    But a stranger’s voice do they not know.
    
            \n\nRefrain:\nMy sheep know My voice, and day by day
    They abide in the fold and go not astray;
    They love Me because I have made them My choice,
    And they follow My call for My sheep know My voice.
    
            \n\n2. My sheep know My voice and the pastures of green,
    Where I lead them so often to feed;
    My sheep know My voice and the cool, sparkling stream
    Where beside its still waters I lead.
    
            \n\n3. My sheep know My voice and the valley of death
    Through which I shall lead them someday;
    But no danger nor harm can touch one of them,
    For I will be with them alway.
    ", "My Sheep Know My Voice. ","Herbert Buffum ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Faith of our fathers, living still,
    In spite of dungeon, fire, and sword;
    Oh, how our hearts beat high with joy
    Whene’er we hear that glorious Word!
    
            \n\nRefrain:\nFaith of our fathers, holy faith!
    We will be true to thee till death.
    
            \n\n2. Our fathers, chained in prisons dark,
    Were still in heart and conscience free;
    How sweet would be their children’s fate,
    If they, like them, could die for thee!
    
            \n\n3. Faith of our fathers, we will strive
    To win all nations unto thee;
    And through the truth that comes from God,
    We all shall then be truly free.
    
            \n\n4. Faith of our fathers, we will love
    Both friend and foe in all our strife;
    And preach thee, too, as love knows how
    By kindly words and virtuous life.
    ", "Faith of Our Fathers! ","Frederick W. Faber ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I’ve seen the lightning flashing,
    And heard the thunder roll,
    I’ve felt sin’s breakers dashing,
    Trying to conquer my soul;
    I’ve heard the voice of my Savior,
    Telling me still to fight on,
    He promised never to leave me,
    Never to leave me alone.
    
            \n\nRefrain:\nNo, never alone,
    No, never alone;
    He promised never to leave me,
    Never to leave me alone.
    
            \n\n2. The world’s fierce winds are blowing,
    Temptation’s sharp and keen,
    I have a peace in knowing
    My Savior stands between—
    He stands to shield me from danger,
    When earthly friends are gone,
    He promised never to leave me,
    Never to leave me alone.
    
            \n\n3. When in affliction’s valley
    I’m treading the road of care,
    My Savior helps me to carry
    My cross when heavy to bear,
    Though all around me is darkness,
    Earthly joys all flown;
    My Savior whispers His promise,
    “I never will leave thee alone.”
    
            \n\n4. He died for me on the mountain,
    For me they pierced His side,
    For me He opened the fountain,
    The crimson, cleansing tide;
    For me He’s waiting in glory,
    Seated upon His throne,
    He promised never to leave me,
    Never to leave me alone.
    ", "Never Alone. ","O. Hoffman ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I am watching for the coming of the glad millennial day,
    When our blessed Lord shall come and catch His waiting bride away.
    Oh! my heart is filled with rapture as I labor, watch, and pray,
    For our Lord is coming back to earth again.
    
            \n\nRefrain:\nOh, our Lord is coming back to earth again.
    Yes, our Lord is coming back to earth again.
    Satan will be bound a thousand years; we\'ll have no tempter then,
    After Jesus shall come back to earth again.
    
            \n\n2. Jesus\' coming back will be the answer to earth\'s sorrowing cry,
    For the knowledge of the Lord shall fill the earth and sea and sky.
    God shall take away all sickness and the sufferer\'s tears will dry,
    When our Savior will come back to earth again.
    
            \n\n3. Yes, the ransomed of the Lord shall come to Zion then with joy,
    And in all His holy mountain nothing hurts or shall destroy.
    Perfect peace shall reign in every heart, and love without alloy,
    After Jesus shall come back to earth again.
            \n\n4. Then the sin and sorrow, pain and death of this dark world shall cease,
    In a glorious reign with Jesus of a thousand years of peace.
    All the earth is groaning, crying for that day of sweet release,
    For our Jesus shall come back to earth again.
    ", "Our Lord's Return To Earth Again. ","J. M. Kirk ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. At the sounding of the trumpet,
    When the saints are gathered home,
    We will greet each other by the crystal sea,
    With the friends and all the loved ones
    There awaiting us to come,
    What a gath’ring of the faithful that will be!
            \n\nRefrain:\nWhat a gath’ring, gath’ring,
    At the sounding of the glorious jubilee!
    What a gath’ring, gath’ring,
    What a gath’ring of the faithful that will be!
    
    
            \n\n2. When the angel of the Lord proclaims
    That time shall be no more,
    We shall gather, and the saved and ransomed see;
    Then to meet again together,
    On the bright celestial shore,
    What a gath’ring of the faithful that will be!
    
    
            \n\n3. At the great and final judgment,
    When the hidden comes to light,
    When the Lord in all His glory we shall see;
    At the bidding of our Savior,
    Come, ye blessèd, to My right,
    What a gath’ring of the faithful that will be!
    ", "What a Gathering. ","John H. Kurzenknabe ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Hark! ’tis the Shepherd’s voice I hear,
    Out in the desert dark and drear,
    Calling the lambs who’ve gone astray,
    Far from the Shepherd’s fold away.
    
            \n\nRefrain:\nBring them in, bring them in,
    Bring them in from the fields of sin;
    Bring them in, bring them in,
    Bring the little ones to Jesus.
    
            \n\n2. Who’ll go and help this Shepherd kind,
    Help Him the little lambs to find?
    Who’ll bring the lost ones to the fold,
    Where they’ll be sheltered from the cold?
    
            \n\n3. Out in the desert hear their cry,
    Out on the mountain, wild and high;
    Hark! ’tis the Master, speaks to thee,
    “Go, find My lambs where’er they be.”
    ", "Bring Them In. ","Alexcenah Thomas ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. All along on the road to the souls true abode,
    There’s an eye watching you;
    Ev’ry step that you take this great eye is awake,
    There’s an eye watching you.
    
            \n\nRefrain:\nWatching you, watching you,
    Ev’ry day mind the course you pursue,
    Watching you, watching you,
    There’s an all seeing Eye watching you.
    
            \n\n2. As you make life’s great fight, keep the pathway of right,
    There’s an eye watching you;
    God will warn not to go in the path of the foe,
    There’s an eye watching you. [Refrain]
    
            \n\n3. Fix your mind on the goal, that sweet home of the soul,
    There’s an eye watching you;
    Never turn from the way to the kingdom of day,
    There’s an eye watching you. [Refrain]
    ", "Watching You. ","John M. Henson ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. On Mount Olive’s sacred brow
    Jesus spent the night in pray’r,
    He\'s the pattern for us all, all alone,
    If we’ll only steal away
    In some portion of the day,
    We will find it always pays to be alone.
    
            \n\nRefrain:\nThere are times I’d like to be
    All alone with Christ my Lord,
    I can tell Him of my troubles all alone.
    
            \n\n2. There are times I’d like to be
    With the sanctified and blest,
    There are times I like to be all alone,
    God can always grace impart,
    To my weary, saddened heart,
    There are times I\'d like to be just all alone. [Refrain]
    
            \n\n3. There are days to fast and pray
    For the pilgrim in his way,
    There are days to be with Christ all alone,
    We can tell Him all our grief,
    He will give us quick relief,
    There are times I\'d like to be just all alone. [Refrain]
    
            \n\n4. When a heart is broken up
    With the tearful, lonesome cup,
    Then’s the time to go to Christ all alone,
    In our blessed Lord divine,
    There is peace and joy sublime,
    When we take our sorrows all to Him alone. [Refrain]
    ", "All Alone. "," G. T. Byrd ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I come to the garden alone,
    While the dew is still on the roses,
    And the voice I hear falling on my ear
    The Son of God discloses.
    
            \n\nRefrain:\nAnd He walks with me, and He talks with me,
    And He tells me I am His own;
    And the joy we share as we tarry there,
    None other has ever known.
    
            \n\n2. He speaks, and the sound of His voice
    Is so sweet the birds hush their singing,
    And the melody that He gave to me
    Within my heart is ringing.
    
            \n\n3. I’d stay in the garden with Him,
    Though the night around me be falling,
    But He bids me go; through the voice of woe
    His voice to me is calling.
    ", "In the Garden. ","Charles A. Miles ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Over the river, faces I see,
    Fair as the morning, looking for me;
    Free from their sorrow, grief and despair,
    Waiting and watching patiently there.
    
           \n\n Refrain\nLooking this way, yes, looking this way,
    Loved ones are waiting, looking this way;
    Fair as the morning, bright as the day,
    Dear ones in glory, looking this way.
    
    \n\n2. Father and mother, safe in that vale,
    Watch for the boatman, wait for the sail,
    Bearing their loved ones over the tide,
    Into the harbor, near to their side.
    
    \n\n3. Brother and sister, gone to that clime,
    Wait for the others, coming sometime,
    Safe with the angels, whiter than snow,
    Watching for dear ones, waiting below.
    
    \n\n4. Sweet little darling, light of the home,
    Looking for someone, beckoning, Come;
    Bright as a sunbeam, pure as the dew,
    Anxiously looking, mother, for you.
    ", "Looking This Way. ","J. W. Van DeVenter ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. They Told Me Love’s Sweetest Old Story…They Told Me Of A Wonderful Name
    It Thrilled All My Soul With Its Glory…And Burned In My Heart Like A Flame
    They Told Me Of One Who So Loved Me…That In Heaven He Could Not Remain
    He Came Down To Seek And To Save Me…Oh Tell Me His Name Again
    
            \n\nCho:-\nOh Tell Me His Name Again…And Sing Me The Sweet Refrain
    Of Him Who In Love Came Down From Above To Die On A Cross In Shame
    That Story My Heart Has Stirred…It’s The Sweetest That I’ve Ever Heard
    It Banishes Fear…It Brings Hope And Cheer…Oh Tell Me His Name Again
    
            \n\n2. They Say He Was Born In A Manger…That There Was Not Roomin The Inn
    That In His Own World Was A Stranger…Yet Loved Us In Spite Of Our Sin
    They Say That His Path Led To Calvary…And That One Day He Died There In Shame
    He Gave His Great Life As A Ransom…Oh Tell Me His Name Again
    
            \n\n3. They Call Him The Sweet Rose Of Sharon…They Call Him The Lily So Fair
    They Call Him The Rock Of All Ages…They Call Him The Bright Morning Star
    He Is Prophet And Priest And Redeemer…The King Of All Kings He Now Reigns
    He’s Coming In Power And Glory…Oh Tell Me His Name Again
    ", "Tell Me His Name Again. ","George Bennard ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Blest be the tie that binds
    Our hearts in Christian love;
    The fellowship of kindred minds
    Is like to that above.
    
            \n\n2. Before our Father’s throne,
    We pour our ardent prayers;
    Our fears, our hopes, our aims are one,
    Our comforts, and our cares.
    
            \n\n3. We share our mutual woes,
    Our mutual burdens bear;
    And often for each other flows
    The sympathizing tear.
    
            \n\n4. When we asunder part,
    It gives us inward pain;
    But we shall still be joined in heart,
    And hope to meet again.
    ", "Blest be the Tie that Binds. ","John Fawcett ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I serve a risen Savior
      He’s in the world today.
    I know that He is living,
      Whatever men may say.
    I see His hand of mercy;
      I hear His voice of cheer;
    And just the time I need Him
      He’s always near.
    
            \n\nCho-:\nHe lives, He lives, Christ Jesus lives today!
    He walks with me and talks with me along life’s narrow way.
    He lives, He lives, salvation to impart!
    You ask me how I know He lives?
        He lives within my heart.
    
            \n\n2. In all the world around me
      I see His loving care,
    And though my heart grows weary,
      I never will despair;
    I know that He is leading,
      Through all the stormy blast;
    The day of His appearing
      Will come at last.
    
            \n\n3. Rejoice, rejoice, O Christian,
      Lift up your voice and sing
    Eternal hallelujahs
      To Jesus Christ the King!
    The Hope of all who seek Him,
      The Help of all who find,
    None other is so loving,
      So good and kind.
    ", "He Lives. ","A. H. Ackley ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Low in the grave He lay,
    Jesus, my Savior,
    Waiting the coming day,
    Jesus, my Lord!
    
            \n\nRefrain:\nUp from the grave He arose,
    With a mighty triumph o’er His foes,
    He arose a Victor from the dark domain,
    And He lives forever, with His saints to reign.
    He arose! He arose!
    Hallelujah! Christ arose!
    
            \n\n2. Vainly they watch His bed,
    Jesus, my Savior;
    Vainly they seal the dead,
    Jesus, my Lord!
    
            \n\n3. Death cannot keep his Prey,
    Jesus, my Savior;
    He tore the bars away,
    Jesus, my Lord!
    ", "Christ Arose. ","Robert Lowry ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. What a wonderful change in my life has been wrought
    Since Jesus came into my heart;
    I have light in my soul for which long I have sought,
    Since Jesus came into my heart.
    
            \n\nRefrain:\nSince Jesus came into my heart,
    Since Jesus came into my heart;
    Floods of joy o’er my soul like the sea billows roll,
    Since Jesus came into my heart.
    
            \n\n2. I have ceased from my wand’ring and going astray,
    Since Jesus came into my heart;
    And my sins which were many are all washed away,
    Since Jesus came into my heart.
    
            \n\n3. I’m possessed of a hope that is steadfast and sure,
    Since Jesus came into my heart;
    And no dark clouds of doubt now my pathway obscure,
    Since Jesus came into my heart.
    
            \n\n4. There’s a light in the valley of death now for me,
    Since Jesus came into my heart;
    And the gates of the City beyond I can see,
    Since Jesus came into my heart.
    
            \n\n5. I shall go there to dwell in that City I know,
    Since Jesus came into my heart;
    And I’m happy, so happy as onward I go,
    Since Jesus came into my heart.
    ", "Since Jesus Came into My Heart. ","Rufus H. McDaniel ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. When my lifework is ended, and I cross the swelling tide,
    When the bright and glorious morning I shall see;
    I shall know my Redeemer when I reach the other side,
    And His smile will be the first to welcome me.
    
            \n\nRefrain:\nI shall know Him, I shall know Him,
    And redeemed by His side I shall stand,
    I shall know Him, I shall know Him,
    By the print of the nails in His hand.
    
            \n\n2. Oh, the soul-thrilling rapture when I view His blessed face,
    And the luster of His kindly beaming eye;
    How my full heart will praise Him for the mercy, love and grace,
    That prepare for me a mansion in the sky.
    
            \n\n3. Oh, the dear ones in glory, how they beckon me to come,
    And our parting at the river I recall;
    To the sweet vales of Eden they will sing my welcome home;
    But I long to meet my Savior first of all.
    
            \n\n4. Through the gates to the city in a robe of spotless white,
    He will lead me where no tears will ever fall;
    In the glad song of ages I shall mingle with delight;
    But I long to meet my Savior first of all.
    ", "My Saviour First of All. ","Frances J. Crosby ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Each day I’ll do a golden deed,
    By helping those who are in need;
    My life on earth is but a span,
    And so I’ll do the best I can.
    
            \n\nRefrain:\nLife’s evening sun is sinking low,
    A few more days, and I must go
    To meet the deeds that I have done,
    Where there will be no setting sun.
    
            \n\n2. To be a child of God each day,
    My light must shine along the way;
    I’ll sing His praise while ages roll,
    And strive to help some troubled soul.
    
            \n\n3. The only life that will endure,
    Is one that’s kind and good and pure;
    And so for God I’ll take my stand,
    Each day I’ll lend a helping hand.
    
            \n\n4. I’ll help someone in time of need,
    And journey on with rapid speed;
    I’ll help the sick and poor and weak,
    And words of kindness to them speak.
    
            \n\n5. While going down life’s weary road,
    I’ll try to lift some trav’ler’s load;
    I’ll try to turn the night to day,
    Make flowers bloom along the way.
    ", "A Beautiful Life. ","William M. Golden ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Lord Jesus, I long to be perfectly whole;
    I want Thee forever to live in my soul;
    Break down every idol, cast out every foe—
    Now wash me, and I shall be whiter than snow.
    
            \n\nRefrain:\nWhiter than snow, yes, whiter than snow,
    Now wash me, and I shall be whiter than snow.
    
            \n\n2. Lord Jesus, look down from Thy throne in the skies,
    And help me to make a complete sacrifice;
    I give up myself, and whatever I know—
    Now wash me, and I shall be whiter than snow.
    
            \n\n3. Lord Jesus, for this I most humbly entreat,
    I wait, blessed Lord, at Thy crucified feet,
    By faith for my cleansing, I see thy blood flow—
    Now wash me, and I shall be whiter than snow.
    
             \n\n4. Lord Jesus, let nothing unholy remain,
    Apply Thine own blood and extract every stain;
    To get this blest cleansing, I all things forego—
    Now wash me, and I shall be whiter than snow.
    
            \n\n5. Lord Jesus, Thou seest I patiently wait;
    Come now and within me a new heart create;
    To those who have sought Thee Thou never said’st “No”—
    Now wash me, and I shall be whiter than snow.
    
            \n\n6. The blessing by faith, I receive from above;
    Oh, glory! my soul is made perfect in love;
    My prayer has prevailed, and this moment I know,
    The blood is applied, I am whiter than snow.
    ", "Whiter Than Snow. ","James L. Nicholson ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. There’s a blessed time that’s coming, coming soon,
    It may be evening, morning, or at noon,
    The wedding of the bride, united with the Groom,
    We shall see the King when He comes.
    
            \n\nRefrain:\nWe shall see the King, we shall see the King,
    We shall see the King when He comes;
    He is coming in pow’r, we’ll hail the blessed hour,
    We shall see the King when he comes.
    
            \n\n2. Are you ready should the Savior call today?
    Would Jesus say, “Well done” or “Go away”?
    “My home is for the pure, the vile can never stay”;
    We shall see the King when He comes.
    
            \n\n3. Oh, my brother, are you ready for the call?
    To crown your Savior king and Lord of all?
    The kingdoms of this world shall soon before Him fall,
    We shall see the King when He comes.
    ", "We Shall See The King. ","John B. Vaughan ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. What a fellowship, what a joy divine,
    Leaning on the everlasting arms;
    What a blessedness, what a peace is mine,
    Leaning on the everlasting arms.
    
            \n\nRefrain:\nLeaning, leaning,
    Safe and secure from all alarms;
    Leaning, leaning,
    Leaning on the everlasting arms.
    
            \n\n2. Oh, how sweet to walk in this pilgrim way,
    Leaning on the everlasting arms;
    Oh, how bright the path grows from day to day,
    Leaning on the everlasting arms.
    
            \n\n3. What have I to dread, what have I to fear,
    Leaning on the everlasting arms?
    I have blessed peace with my Lord so near,
    Leaning on the everlasting arms.
    ", "Leaning on the Everlasting Arms. ","Elisha A. Hoffman ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. I’ve reached the land of corn and wine,
    And all its riches freely mine;
    Here shines undimmed one blissful day,
    For all my night has passed away.
    
            \n\nRefrain:\nO Beulah Land, sweet Beulah Land,
    As on thy highest mount I stand,
    I look away across the sea,
    Where mansions are prepared for me,
    And view the shining glory shore,
    My heav’n, my home forevermore!
    
            \n\n2. My Savior comes and walks with me,
    And sweet communion here have we;
    He gently leads me by His hand,
    For this is Heaven’s borderland.
    
            \n\n3. A sweet perfume upon the breeze,
    Is borne from ever vernal trees,
    And flow’rs that never fading grow
    Where streams of life forever flow.
    
            \n\n4. The zephyrs seem to float to me,
    Sweet sounds of Heaven’s melody,
    As angels with the white-robed throng
    Join in the sweet redemption song.
    ", "Beulah Land. ","Edgar P. Stites ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Hallelujah, what a thought!
    Jesus full salvation brought,
    Victory, victory;
    Let the pow’rs of sin assail,
    Heaven’s grace can never fail,
    Victory, victory.
    
            \n\nRefrain:\nVictory, yes, victory.
    Hallelujah! I am free,
    Jesus gives me victory;
    Glory, glory! hallelujah!
    He is all in all to me.
    
            \n\n2. I am trusting in the Lord,
    I am standing on His Word,
    Victory, victory;
    I have peace and joy within,
    Since my life is free from sin,
    Victory, victory.
    
            \n\n3. Shout your freedom everywhere,
    His eternal peace declare,
    Victory, victory,
    Let us sing it here below,
    In the face of every foe,
    Victory, victory.
    
            \n\n4. We will sing it on that shore,
    When this fleeting life is o’er,
    Victory, victory;
    Sing it here, ye ransomed throng,
    Start the everlasting song:
    Victory, victory.
    ", "Victory. ","Barney E. Warren ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Are you trusting Jesus,
    All along the way?
    Does He grow more precious
    To your heart each day?
    Are you His disciple?
    Test His Word and see,
    He will give the Spirit more abundantly.
    
            \n\nRefrain\nMore abundantly,
    More abundantly,
    That they might have life
    And more abundantly;
    More abundantly,
    More abundantly,
    That they might have life
    And more abundantly.
    
            \n\n2. For His matchless favor,
    Magnify the name
    Of our gracious Savior
    Who from glory came;
    Let the saints adore Him
    For this wondrous Word,
    Sealing our redemption thro’ the crimson flood. [Refrain]
    
            \n\n3. Come to Him believing,
    Hearken to His call;
    All from Him receiving,
    Yield to Him your all;
    Jesus will accept you
    When to Him you flee;
    He will grant His blessing more abundantly. [Refrain]
    ", "More Abundantly. ","Thoro Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. The mercy of God is an ocean divine,
    A boundless and fathomless flood;
    Launch out in the deep, cut away the shore line,
    And be lost in the fullness of God.
    
            \n\nRefrain:\nLaunch out, into the deep,
    Oh, let the shore line go;
    Launch out, launch out in the ocean divine,
    Out where the full tides flow.
    
            \n\n2. But many, alas! only stand on the shore,
    And gaze on the ocean so wide;
    They never have ventured its depths to explore,
    Or to launch on the fathomless tide.
    
            \n\n3. And others just venture away from the land,
    And linger so near to the shore
    That the surf and the slime that beat over the strand
    Dash over them in floods evermore.
    
            \n\n4. Oh, let us launch out on this ocean so broad,
    Where floods of salvation o’erflow;
    Oh, let us be lost in the mercy of God,
    Till the depths of His fullness we know.
    ", "Launch Out. ","Albert B. Simpson ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Beautiful path my feet must tread,
    Walking with him, walking with him,
    Beautiful peace without a dread,
    Walking Christ my Lord.
    
    \n\nCho-:\nWalking with him (walking with Him)
    Walking with him (walking with Him)
    walking with Jesus,
    Walking with Christ my Lord. (glorious Lord)
    
    \n\n2. Beautiful truth my heart shall know,
    Walking with Him,
    Walking with Him,
    Beautiful cleaning, white as snow,
    Walking with Christ my Lord.
    
    \n\n3. Beautiful life to me is giv’n,
    Walking with him,
    Walking with Him.
    Beautiful hope secure in heav’n
    Walking with Christ my Lord.
    
    \n\n4. Beautiful robe I soon shall wear.
    Walking with Him,
    Walking with Him,
    Beautiful home and mansions fair
    Walking with Christ my Lord.
    ", "Walking With Him. ","L. C. Hall ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. What did they do at Pentecost,
       When the Spirit first was giv’n
       When fell the blessed Holy     Ghost,
       Like a rushing wind from   heav’n?
    
       \n\nCho-:\nThey spoke with tongues and so will you
    When He sends the Holy Ghost:
    His pow’r will fall upon you too,
    As it fell at Pentecost
    
    \n\n2. What did they do at Caesarea
       When God’s servant spoke the word,
       When they were met, with joy
       to hear
       Tidings of the risen Lord?
    
       \n\n3. What did they do at Ephesus?
       Thru the great apostle Paul,
       God gave the sign as at the first
       And He gives the sign to all.
    
       \n\n4. What did they do at the wide world o’er,
       When the Holy Ghost was giv’n,
       When they received the promise sure
       Of the Spirit sent from heav’n?
    
       \n\n5. Even today in copious show’r
       Falls the promised latter rain;
       They who receive the old time pow’r
       Speak “in other tongues” again.
    ", "What Did They Do? ","L. C. Hall ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I love to tell the story of unseen things above,
    Of Jesus and His glory, of Jesus and His love;
    I love to tell the story, because I know ’tis true,
    It satisfies my longings as nothing else would do.
    
            \n\nRefrain:\nI love to tell the story,
            ’Twill be my theme in glory,
            To tell the old, old story
            Of Jesus and His love.
    
            \n\n2. I love to tell the story, more wonderful it seems
    Than all the golden fancies of all our golden dreams;
    I love to tell the story, it did so much for me,
    And that is just the reason I tell it now to thee.
    
            \n\n3. I love to tell the story, ’tis pleasant to repeat,
    What seems each time I tell it more wonderfully sweet;
    I love to tell the story, for some have never heard
    The message of salvation from God’s own holy Word.
    
            \n\n4. I love to tell the story, for those who know it best
    Seem hungering and thirsting to hear it like the rest;
    And when in scenes of glory I sing the new, new song,
    ’Twill be the old, old story that I have loved so long.
    ", "I Love to Tell the Story. ","Arabella K. Hankey ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Come, we that love the Lord,
    And let our joys be known;
    Join in a song with sweet accord,
    And thus surround the throne.
    
            \n\nRefrain:\nWe’re marching to Zion,
        Beautiful, beautiful Zion;
        We’re marching upward to Zion,
        The beautiful city of God.
    
            \n\n2. The sorrows of the mind
    Be banished from the place;
    Religion never was designed
    To make our pleasures less.
    
            \n\n3. Let those refuse to sing,
    Who never knew our God;
    But children of the heav’nly King
    May speak their joys abroad.
    
            \n\n4. The men of grace have found
    Glory begun below;
    Celestial fruits on earthly ground
    From faith and hope may grow.
    
            \n\n5. The hill of Zion yields
    A thousand sacred sweets
    Before we reach the heav’nly fields,
    Or walk the golden streets.
    
            \n\n6. Then let our songs abound,
    And every tear be dry;
    We’re marching through Immanuel’s ground
    To fairer worlds on high.
    ", "We're Marching to Zion. ","Isaac Watts ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Though dark the night, and clouds look black
    And stormy overhead,
    And trials of almost ev’ry kind
    Across my path are spread;
    How soon I conquer all,
    AS to the Lord I call:
    A little talk with Jesus makes it right, all right!
    
            \n\nRefrain:\nA little talk with Jesus makes it right, all right!
    A little talk with Jesus makes it right, all right!
    In trials of ev’ry kind,
    Praise God, I always find
    A little talk with Jesus makes it right, all right!
    
            \n\n2. When those who once were dearest friends
    Begin to persecute,
    And more who once profess’d to love
    Have distant grown, and mute,—
    I tell Him all my grief,
    He quickly sends relief:
    A little talk with Jesus makes it right, all right! [Refrain]
    
            \n\n3. And thus, by frequent little talks,
    I gain the victory,
    And march along with cheerful song,
    Enjoying liberty;
    With Jesus as my Friend,
    I’ll prove unto the end,
    A little talk with Jesus makes it right, all right! [Refrain]
    ", "A Little Talk. ","Anon ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I have found His grace is all complete,
    He supplieth every need;
    While I sit and learn at Jesus’ feet,
    I am free, yes, free indeed.
    
            \n\nRefrain:\nIt is joy unspeakable and full of glory,
    Full of glory, full of glory;
    It is joy unspeakable and full of glory,
    Oh, the half has never yet been told.
    
            \n\n2. I have found the pleasure I once craved,
    It is joy and peace within;
    What a wondrous blessing, I am saved
    From the awful gulf of sin.
    
            \n\n3. I have found that hope so bright and clear,
    Living in the realm of grace;
    Oh, the Savior’s presence is so near,
    I can see His smiling face.
    
            \n\n4. I have found the joy no tongue can tell,
    How its waves of glory roll;
    It is like a great o’erflowing well,
    Springing up within my soul.
    ", "Joy Unspeakable. ","Barney E. Warren ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Come, every soul by sin oppressed,
    there\'s mercy with the Lord,
    and He will surely give you rest
    by trusting in His word.
        \n\nRefrain:\nOnly trust Him, only trust Him,
    only trust Him now;
    He will save you, He will save you.
    He will save you now.
        \n\n2. For Jesus shed His precious blood
    rich blessings to bestow;
    plunge now into the crimson flood
    that washes white as snow. [Refrain]
        \n\n3. Yes, Jesus is the truth, the way,
    that leads you into rest;
    believe in Him without delay,
    and you are fully blest. [Refrain]
        \n\n4. Come, then, and join this holy band,
    and on to glory go,
    to dwell in that celestial land
    where joys immortal flow. [Refrain]
    ", "Only Trust Him. ","John H. Stockton ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Who\'ll be the next to follow Jesus?
    Who\'ll be the next His cross to bear?
    Some one is ready, some one is waiting;
    Who\'ll be the next a crown to wear?
    
            \n\nRefrain:\nWho\'ll be the next?
    Who\'ll be the next?
    Who\'ll be the next to follow Jesus?
    Who\'ll be the next to follow Jesus now?
    Follow Jesus now?
        \n\n2. Who\'ll be the next to follow Jesus
    Follow His weary bleeding feet?
    Who\'ll be the next to lay ev\'ry burden
    Down at the Father\'s mercy-seat? [Chorus]
        \n\n3. Who\'ll be the next to follow Jesus?
    Who\'ll be the next to praise His name?
    Who\'ll swell the chorus of free redemption
    Sing, hallelujah! praise the Lamb? [Chorus]
    ", "Who'll Be the Next? ","Annie S. Hawks ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. On Cal’vry’s hill of sorrow
    Where sin’s demands were paid,
    And rays of hope for tomorrow
    Across our path were laid.
        \n\nRefrain\nI see a crimson stream of blood.
    It flows from Calvary,
    Its waves which reach the throne of God,
    Are sweeping over me.
    
            \n\n2. Today no condemnation
    Abides to turn away
    My soul from His salvation,
    He’s in my heart to stay. [Refrain]
    
            \n\n3. When gloom and sadness whisper,
    You’ve sinned, no use to pray,
    I look away to Jesus,
    And He tells me to say: [Refrain]
    
            \n\n4. And when we reach the portal
    Where life forever reigns,
    The ransomed hosts grand final
    Will be this glad refrain. [Refrain]
    ", "I See A Crimson Stream. ","G. T. Haywood ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Shall we gather at the river,
    Where bright angel feet have trod,
    With its crystal tide forever
    Flowing by the throne of God?
    
            \n\nRefrain:\nYes, we’ll gather at the river,
    The beautiful, the beautiful river;
    Gather with the saints at the river
    That flows by the throne of God.
    
            \n\n2. On the margin of the river,
    Washing up its silver spray,
    We will talk and worship ever,
    All the happy golden day.
    
            \n\n3. Ere we reach the shining river,
    Lay we every burden down;
    Grace our spirits will deliver,
    And provide a robe and crown.
    
            \n\n4. At the smiling of the river,
    Mirror of the Savior’s face,
    Saints, whom death will never sever,
    Lift their songs of saving grace.
    
            \n\n5. Soon we’ll reach the silver river,
    Soon our pilgrimage will cease;
    Soon our happy hearts will quiver
    With the melody of peace.
    ", "Shall We Gather at the River? ","Robert Lowry ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Down in the valley with my Savior I will go,
    Where the flow’rs are blooming and the sweet waters flow;
    Everywhere He leads me I will follow, follow on,
    Walking in His footsteps till the crown be won.
    
            \n\nRefrain:\nFollow! follow! I will follow Jesus!
    Anywhere, everywhere, I will follow on!
    Follow! follow! I will follow Jesus!
    Everywhere He leads me I will follow on!
    
            \n\n2. Down in the valley with my Savior I will go,
    Where the storms are sweeping and the dark waters flow;
    With His hand to lead me I will never, never fear,
    Danger cannot fright me if my Lord is near.
    
            \n\n3. Down in the valley, or upon the mountain steep,
    Close beside my Savior will my soul ever keep;
    He will lead me safely in the path that He has trod,
    Up to where they gather on the hills of God.
    ", "Follow On! ","William O. Cushing. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Jesus Christ the Lord opened up the way to glory
    When He died to save us from our ruined state,
    And He asks that we shall go tell the world the story,
    How His blood will save them from their awful fate.
    
            \n\nRefrain:\nJesus opened up the way to heaven’s gate
    When He died on the cross,
    To redeem all the lost;
    He prepared the road
    That leads to His abode,
    ’Tis a road marked by blood
    But it leads us home to God.
    
            \n\n2. And the way is marked by the footprints of the Savior,
    With His blood he made it, made it plain and straight;
    If you walk that way, it will lead you into heaven,
    Lead you safely into glory’s golden gate.
    
            \n\n3. Sinner, will you come and join in this heav’nly journey,
    Walk the bloody pathway that the Savior trod;
    Then when life is over and all the sheaves are garnered,
    You will meet the Savior and be not afraid.
    ", "In Him Dwelleth. ","L. C. Hall ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Jesus Christ the Lord opened up the way to glory
    When He died to save us from our ruined state,
    And He asks that we shall go tell the world the story,
    How His blood will save them from their awful fate.
    
            \n\nRefrain:\nJesus opened up the way to heaven’s gate
    When He died on the cross,
    To redeem all the lost;
    He prepared the road
    That leads to His abode,
    ’Tis a road marked by blood
    But it leads us home to God.
    
            \n\n2. And the way is marked by the footprints of the Savior,
    With His blood he made it, made it plain and straight;
    If you walk that way, it will lead you into heaven,
    Lead you safely into glory’s golden gate.
    
            \n\n3. Sinner, will you come and join in this heav’nly journey,
    Walk the bloody pathway that the Savior trod;
    Then when life is over and all the sheaves are garnered,
    You will meet the Savior and be not afraid.
    ", "Jesus Opened Up the Way. ","Eugene M. Bartlett ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I’ve believed the true report,
    Hallelujah to the Lamb!
    I have passed the outer court,
    O glory be to God!
    I am all on Jesus’ side,
    On the altar sanctified,
    To the world and sin I’ve died,
    Hallelujah to the Lamb!
    
            \n\nRefrain\nHallelujah! Hallelujah!
    I have passed the riven veil,
    Where the glories never fail,
    Hallelujah! Hallelujah!
    I am living in the presence of the King.
    
            \n\n2. I’m a king and priest to God,
    Hallelujah to the Lamb!
    By the cleansing of the blood,
    O glory be to God!
    By the Spirit’s power and light,
    I am living day and night,
    In the holiest place so bright,
    Hallelujah to the Lamb!
    
            \n\n3. I have passed the outer veil,
    Hallelujah to the Lamb!
    Which did once God’s light conceal,
    O glory be to God!
    But the blood has brought me in
    To God’s holiness so clean,
    Where there’s death to self and sin,
    Hallelujah to the Lamb!
    
            \n\n4. I’m within the holiest pale,
    Hallelujah to the Lamb!
    I have passed the inner veil,
    O glory be to God!
    I am sanctified to God
    By the power of the blood,
    Now the Lord is my abode,
    Hallelujah to the Lamb!
    ", "I've Believed the True Report. ","Charles Price Jones ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Nearer, my God, to thee,
    Nearer to thee!
    E’en though it be a cross
    That raiseth me.
    Still all my song shall be
    Nearer, my God, to thee,
    Nearer, my God, to thee,
    Nearer to thee!
    
            \n\n2. Though like the wanderer,
    The sun gone down,
    Darkness be over me,
    My rest a stone,
    Yet in my dreams I’d be
    Nearer, my God, to thee,
    Nearer, my God, to thee,
    Nearer to thee!
    
            \n\n3. There let the way appear,
    Steps unto heav’n;
    All that thou sendest me,
    In mercy giv’n;
    Angels to beckon me
    Nearer, my God, to thee,
    Nearer, my God, to thee,
    Nearer to thee!
    
            \n\n4. Then with my waking thoughts
    Bright with thy praise,
    Out of my stony griefs
    Bethel I’ll raise;
    So by my woes to be
    Nearer, my God, to thee,
    Nearer, my God, to thee,
    Nearer to thee!
    
            \n\n5. Or if, on joyful wing
    Cleaving the sky,
    Sun, moon, and stars forgot,
    Upward I fly,
    Still all my song shall be
    Nearer, my God, to thee,
    Nearer, my God, to thee,
    Nearer to thee!
    ", "Nearer, My God, to Thee. ","Sarah F. Adams, Lowell Mason ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Walking in sunlight all of my journey,
    Over the mountains, through the deep vale;
    Jesus has said, I’ll never forsake thee—
    Promise divine that never can fail.
    
            \n\nRefrain:\nHeavenly sunlight! Heavenly sunlight!
    Flooding my soul with glory divine;
    Hallelujah! I am rejoicing,
    Singing His praises, Jesus is mine!
    
            \n\n2. Shadows around me, shadows above me
    Never conceal my Savior and Guide;
    He is the light, in Him is no darkness,
    Ever I’m walking close to His side.
    
            \n\n3. In the bright sunlight, ever rejoicing,
    Pressing my way to mansions above;
    Singing His praises, gladly I’m walking,
    Walking in sunlight, sunlight of love.
    ", "Heavenly Sunlight. ","Henry J. Zelley ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. There’s a land beyond the river,
    That we call the sweet forever,
    And we only reach that shore by faith’s decree;
    One by one we’ll gain the portals,
    There to dwell with the immortals,
    When they ring the golden bells for you and me.
    
            \n\nRefrain:\nDon’t you hear the bells now ringing?
    Don’t you hear the angels singing?
    ’Tis the glory hallelujah Jubilee.
    In that far-off sweet forever,
    Just beyond the shining river,
    When they ring the golden bells for you and me.
    
            \n\n2. We shall know no sin or sorrow,
    In that haven of tomorrow,
    When our barque shall sail beyond the silver sea;
    We shall only know the blessing
    Of our Father’s sweet caressing,
    When they ring the golden bells for you and me.
    
            \n\n3. When our days shall know their number,
    And in death we sweetly slumber,
    When the King commands the spirit to be free;
    Nevermore with anguish laden,
    We shall reach that lovely Eden,
    When they ring the golden bells for you and me.
    ", "When They Ring the Golden Bells. ","Daniel de Marbelle ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. As you travel along on the Jericho road,
    Does the world seem all wrong and heavy your load?
    Just bring it to Christ, your sins all confess,
    On the Jericho road your heart He will bless.
    
            \n\nRefrain:\nOn the Jericho road there’s room for just two,
    No more and no less, just Jesus and you;
    Each burden He’ll bear, each sorrow He’ll share,
    There’s never a care for Jesus is there.
    
            \n\n2. On the Jericho road blind Bartimaeus sat,
    His life was a voice, so empty and flat;
    But Jesus appeared, one word bro’t him sight,
    On the Jericho road Christ banished his night. [Refrain]
    
            \n\n3. O brother to you this message I bring,
    Tho hope may be gone, He’ll cause you to sing;
    At Jesus’ command, sin’s shackles must fall,
    On the Jericho road will you answer His call? [Refrain]
    ", "On the Jericho Road. ","Donald S. McCrossan ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I am now on the altar, I am now on the altar,
            I am now on the altar Which was made for me.
    
        \n\nCho-:\nJesus breaks every fetter,
        Jesus breaks every fetter,
        Jesus breaks every fetter, For He sets me free!
    
        \n\n2. I will never doubt my Saviour, I will never doubt my Saviour, I will never
    doubt my Saviour, For He cleanses me.
    
        \n\n3. I will rest on His promise, I will rest on
    His promise, I will rest on His promise, Which is given to me.
    
        \n\n4. On the other side of Jordon, In the sweet fields of Eden
            Where the Tree of Life is blooming, There is rest for me.
    ", "Jesus Breaks Every Fetter. ","Unknown ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I’m in the way, the bright and shining way,
    I’m in the glory-land way;
    Telling the world that Jesus saves today,
    Yes, I’m in the glory-land way.
    
            \n\nRefrain:\nI’m in the glory-land way;
    I’m in the glory-land way;
    Heaven is nearer, and the way groweth clearer,
    For I’m in the glory-land way.
    
            \n\n2. List to the call, the gospel call today,
    Get in the glory-land way;
    Wand’rers, come home, oh, hasten to obey,
    And get in the glory-land way. [Refrain]
    
            \n\n3. Onward I go, rejoicing in His love
    I’m in the glory-land way;
    Soon I shall see Him in that home above,
    Oh I’m in the glory-land way. [Refrain]
    ", "The Glory-Land Way. ","J. S. Torbett ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. We\'ve a story to tell to the nations,
    that shall turn their hearts to the right,
    a story of truth and mercy,
    a story of peace and light,
    a story of peace and light.
    
            \n\nRefrain:\nFor the darkness shall turn to dawning,
    and the dawning to noonday bright,
    and Christ\'s great kingdom shall come on earth,
    the kingdom of love and light.
    
            \n\n2. We\'ve a song to be sung to the nations,
    that shall lift their hearts to the Lord,
    a song that shall conquer evil
    and shatter the spear and sword,
    and shatter the spear and sword. [Refrain]
    
            \n\n3. We\'ve a message to give to the nations,
    that the Lord who reigneth above
    has sent us His Son to save us,
    and show us that God is love,
    and show us that God is love. [Refrain]
    
            \n\n4. We\'ve a Savior to show to the nations,
    who the path of sorrow has trod,
    that all of the world\'s great peoples
    may come to the truth of God,
    may come to the truth of God. [Refrain]
    ", "We've a Story to Tell. ","H. Ernest Nichol ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Bright is the path that I tread today, Blue is the sky above, For I am walking the Heavenly way, Praising my Savior\’s love. Sweet is the song that my spirit sings, Leaving my drear-y past; “I am a child of the King of kings; Jesus is mine at last.”
    
    \n\nCho-.\nMine at last! Mine at last! Jesus has hidden my wayward past, Heard my plea, made me free!
     \n\n2. Sin and its pleasures their charm have lost, I have been cleaned within; Never again shall my soul be tossed out on the waves of sin. Jesus has answered my heart\’s deep plea, From me my burden cast; I am as glad as a soul can be- Jesus is mine at last.
    \n\n3. Nothing shall cause me to part from Him, Till I have ceased to ream whether through sunshine or shadows dim, Jesus will lead me home. Closely I\’ll cling to Him all the way, Fearing no stormy blast, Singing right up to the land of day, “Jesus is mine at last.”
    ", "Mine at Last. ","James Rowe ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Though we may not know the moment when our blessèd Lord shall come
    To receive us to mansions over there,
    Yet we know ’twill not be long before He takes His loved ones home,
    And we’re caught up to meet Him in the air.
            \n\nRefrain:\nWe’ll be caught up to meet Him in the air,
    We’ll be caught up His blessedness to share;
    Very soon He will come To take His people home Caught up to meet Him in the air.
            \n\n2. There are times when we are called to wade through waters deep and wide,
    And the sorrows of Christ Himself to share; We remember He has promised to be ever at our side,
    Till we’re caught up to meet Him in the air.
            \n\n3. Then eternal years in glory with our Savior we will spend;
    O we long for that morning bright and fair!
    When the days of our sojourning and our pilgrimage shall end,
    We’ll be caught up to meet Him in the air.
    ", "Caught Up. ","Thoro Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. The Lord told His disciples the promised He would send, enduring them with power from on high; So to the upper room they went, their hearts in one accord, and to the Lord in spirit they drew nigh.
    
    \n\nCho-.\nThen don\’t came the fire, down came the fire, Down came the fire from on high, Oh Hallelujah! For the Holy Ghost descended and filled the room within; Down came the fire, praise the Lord.
    Then down comes the fire, down comes the fire, Down comes the fire from on high, Oh Hallelujah! For the Holy Ghost will enter your life and take control; Down comes the fire, praise the Lord.
    \n\n2. When Peter preached the gospel, Cornelius heart was stirred; He listened to the message from the word; And then all His household whose hearts were open wide The Spirit came to magnify the Lord.
    \n\n3. Come now, my friend, and claim it, \’tis promised in His word, The comforter will come and fill your soul; Enduring you with power to serve your blessed Lord; Then in your heart will waves of glory roll.
    \n\n4. Today the Lord is giving the Comforter to men, Enduring them with power from on High; The fountain still is open, you\’ll get your portion yet; Just call on Him, for He\’ll not pass you by.
    ", "Down Came the Fire. ","O. M. Knutson ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Thou Christ of burning, cleansing flame,
    Send the fire, send the fire, send the fire!
    Thy blood-bought gift today we claim,
    Send the fire, send the fire, send the fire!
    Look down and see this waiting host,
    Give us the promised Holy Ghost;
    We want another Pentecost,
    Send the fire, send the fire, send the fire!
    
            \n\n2. God of Elijah, hear our cry:
    Send the fire, send the fire, send the fire!
    To make us fit to live or die,
    Send the fire, send the fire, send the fire!
    To burn up every trace of sin,
    To bring the light and glory in,
    The revolution now begin,
    Send the fire, send the fire, send the fire!
    
            \n\n3. ’Tis fire we want, for fire we plead,
    Send the fire, send the fire, send the fire!
    The fire will meet our every need,
    Send the fire, send the fire, send the fire!
    For strength to ever do the right,
    For grace to conquer in the fight,
    For pow’r to walk the world in white,
    Send the fire, send the fire, send the fire!
    
            \n\n4. To make our weak hearts strong and brave,
    Send the fire, send the fire, send the fire!
    To live a dying world to save,
    Send the fire, send the fire, send the fire!
    Oh, see us on Thy altar lay
    Our lives, our all, this very day;
    To crown the off’ring now we pray,
    Send the fire, send the fire, send the fire!
    ", "Send the Fire. ","William Booth ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Ready to suffer grief or pain,
       Ready to stand the test;
       Ready to stay at home and send
       Others if He sees best.
    
       \n\nCho:-\nReady to go, ready to stay
    Ready my place to fill;
    Ready for service lowly or great,
    Ready to do His will.
    
    \n\n2. Ready to go, ready to bear,
       Ready to watch and pray;
       Ready to stand aside and give
       Till He shall clear the way.
    
       \n\n3. Ready to speak, ready to think,
       Ready with heart and brain;
       Ready to stand where He sees fit,
       Ready to bear the strain.
    
       \n\n4. Ready to speak, ready to warn,
            Ready o’er souls to yearn;
       Ready in life or ready in death,
            Ready for His return.
    ", "Ready. ","A. C. Palmer ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Must Jesus bear the cross alone,
    And all the world go free?
    No, there’s a cross for everyone,
    And there’s a cross for me.
    
            \n\n2. The consecrated cross I’ll bear
    Till death shall set me free;
    And then go home my crown to wear,
    For there’s a crown for me.
    
            \n\n3. Upon the crystal pavement down
    At Jesus’ pierced feet,
    Joyful I’ll cast my golden crown
    And His dear Name repeat.
    
            \n\n4. O precious cross! O glorious crown!
    O resurrection day!
    When Christ the Lord from heav’n comes down
    And bears my soul away.
    
            \n\n5. How happy are the saints above,
    Who once went sorr\’wing here!
    But now they taste unmingled love,
    And joy without a tear.
    ", "Must Jesus Bear the Cross Alone? ","Thomas Shepherd ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Sowing in the morning, sowing seeds of kindness,
    Sowing in the noontide and the dewy eve;
    Waiting for the harvest, and the time of reaping,
    We shall come rejoicing, bringing in the sheaves.
    
            \n\nRefrain:\nBringing in the sheaves, bringing in the sheaves,
    We shall come rejoicing, bringing in the sheaves;
    Bringing in the sheaves, bringing in the sheaves,
    We shall come rejoicing, bringing in the sheaves.
    
            \n\n2. Sowing in the sunshine, sowing in the shadows,
    Fearing neither clouds nor winter\’s chilling breeze;
    By and by the harvest, and the labor ended,
    We shall come rejoicing, bringing in the sheaves.
    
            \n\n3. Going forth with weeping, sowing for the Master,
    Though the loss sustained our spirit often grieves;
    When our weeping\’s over, He will bid us welcome,
    We shall come rejoicing, bringing in the sheaves.
    ", "Bringing In the Sheaves. ","Knowles Shaw ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. We are never,
            never weary of the grand old song,
            glory to God, hallelujah
    We can sing it loud as ever
    With our faith more strong
    Glory to God, hallelujah.
    
    \n\nCho-:\nO, the children of the Lord
       Have a right to shout and sing
       For the way is growing bright
       And the souls are on the wing
       We are going by and by
       To the palace of the King
       Glory to God, hallelujah.
    
            \n\n2. We are lost amid the rapture of redeeming love,
    Glory to God, hallelujah
    We are rising on its pinions to the hills above
    Glory to God, hallelujah.
    
    \n\n3. We are going to a palace that is build of gold, Glory to God, hallelujah
    Where the King in all his splendor
    We shall soon behold
    Glory to God hallelujah.
    
    \n\n4. There we’ll should redeeming mercy in a glad, new song ,  Glory to God hallelujah
    There we’ll sing the praise of Jesus
    With the blood-washed throng
    Glory to God, hallelujah.
    ", "Glory to God, Hallelujah! ","Fanny J. Crosby ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. To the regions beyond I must go, I must go
    Where the story has never been told;
    To the millions that never have heard of His love,
    I must tell the sweet story of old.
    
            \n\nRefrain:\nTo the regions beyond I must go, I must go,
    Till the world, all the world,
    His salvation shall know.
    
            \n\n2. To the hardest of places He calls me to go,
    Never thinking of comfort or ease;
    The world may pronounce me a dreamer, a fool,
    Enough if the Master I please.
    
            \n\n3. Oh, you that are spending your leisure and powers
    In those pleasures so foolish and fond;
    Awake from your selfishness, folly and sin,
    And go to the regions beyond.
    
            \n\n4. There are other lost sheep that the Master must bring,
    And to them must the message be told;
    He sends me to gather them out of all lands,
    And welcome them back to His fold.
    
                    \n\n2. To the hardest of places He calls me to go,
    Never thinking of comfort or ease;
    The world may pronounce me a dreamer, a fool,
    Enough if the Master I please.
    ", "The Regions Beyond. ","Albert B. Simpson ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. In these, the closing days of time,
    What joy the glorious hope affords,
    That soon, oh, wondrous truth sublime!
    He shall reign, King of kings and Lord of lords.
    
    \n\nCho-:\nHe’s coming soon,
    He’s coming soon;
    With joy we welcome His returning;
    It may be morn, it may be night or noon,
    We know He’s coming soon.
    
    \n\n2. The signs around in earth and air,
    Or painted on the starlit sky,
    God’s faithful witnesses declare
    That the coming of the Savior
    draweth nigh.
    
    \n\n3. The dead in Christ who ‘neath us lie,
    In countless numbers all shall rise,
    When thro’ the portals of the sky
    He shall come to prepare our
    paradise.
    
    \n\n4. And we who, living, yet remain,
    Caught up, shall meet our faithful Lord,
    This hope we cherish not in vain,
    But we comfort one another by this word.
    ", "He's Coming Soon. ","Thoro Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Out of my bondage, sorrow and night,
    Jesus, I come; Jesus I come.
    Into Thy freedom, gladness and light,
    Jesus, I come to Thee.
    Out of my sickness into Thy health,
    Out of my wanting and into Thy wealth,
    Out of my sin and into Thyself,
    Jesus, I come to Thee.
    
            \n\n2. Out of my shameful failure and loss,
    Jesus, I come; Jesus, I come.
    Into the glorious gain of Thy cross,
    Jesus, I come to Thee.
    Out of earth\'s sorrows into Thy balm,
    Out of life\'s storms and into Thy calm,
    Out of distress into jubilant psalm,
    Jesus, I come to Thee.
    
            \n\n3. Out of unrest and arrogant pride,
    Jesus, I come; Jesus, I come.
    Into Thy blessed will to abide,
    Jesus, I come to Thee.
    Out of myself to dwell in Thy love,
    Out of despair into raptures above,
    Upward forever on wings like a dove,
    Jesus, I come to Thee.
    
            \n\n4. Out of the fear and dread of the tomb,
    Jesus, I come; Jesus, I come.
    Into the joy and light of Thy home,
    Jesus, I come to Thee.
    Out of the depths of ruin untold,
    Into the peace of Thy sheltering fold,
    Ever Thy glorious face to behold,
    Jesus, I come to Thee.
    ", "Jesus I Come. ","William True Sleeper ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. In sin I wandered sore and sad
    With bleeding heart and aching head,
    Till Jesus came and sweetly said,
    I’ll take thy sins away.
    
            \n\nRefrain:\nThank God for the blood!
    Thank God for the blood!
    Thank God for the blood
    That washes white as snow.
    
            \n\n2. I gave my heart, my life, my all
    To Him who drank the cup of gall
    To raise the guilty from the fall
    And take their sins away. [Refrain]
    
            \n\n3. The water, Spirit and the blood
    Agree, if we but understood,
    In making sinners pure and good,
    And take their sins away. [Refrain]
    
            \n\n4. How wonderful God’s mighty plan!
    How grace the awful guilt did span
    When He took on the form of man
    To take our sins away. [Refrain]
    
            \n\n5. We cannot know, we may not tell
    How we are saved from death and hell;
    Thru faith we know that all is well—
    He took our sins away. [Refrain]
    ", "Thank God for the Blood. ","G. T. Haywood ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("       1. Love divine, all loves excelling,
    Joy of Heav’n to earth come down;
    Fix in us thy humble dwelling;
    All thy faithful mercies crown!
    Jesus, Thou art all compassion,
    Pure unbounded love Thou art;
    Visit us with Thy salvation,
    Enter every trembling heart.
    
    \n\n2. Breathe, O breathe Thy loving Spirit
    Into every troubled breast!
    Let us all in Thee inherit;
    Let us find that second rest.
    Take away our bent to sinning;
    Alpha and Omega be;
    End of faith, as its beginning,
    Set our hearts at liberty.
    
    \n\n3. Come, Almighty to deliver,
    Let us all Thy life receive;
    Suddenly return, and never,
    Nevermore Thy temples leave.
    Thee we would be always blessing,
    Serve Thee as Thy hosts above,
    Pray and praise Thee without ceasing,
    Glory in Thy perfect love.
    
    \n\n4. Finish, then, Thy new creation;
    Pure and spotless let us be;
    Let us see Thy great salvation
    Perfectly restored in Thee;
    Changed from glory into glory,
    Till in Heav’n we take our place,
    Till we cast our crowns before Thee,
    Lost in wonder, love, and praise.
    ", "Love Divine. ","Charles Wesley ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. They come from the East and West,
    They come from the lands afar
    To feast with the King, to dine as His guest;
    How blessed these pilgrims are!
    Beholding His hallow’d face,
    Aglow with a light divine;
    Blest partakers of His grace,
    As gems in His crown to shine.
    
    \n\nRefrain:\nO, Jesus is coming soon,
    The judgment will then begin;
    O what if our Lord, this moment should come
    For those who are free from sin,
    O then would it bring you joy,
    Or sorrow and deep despair;
    When the Lord in glory comes,
    We’ll meet Him up in the air.
    
    \n\n2.   I look on the great white throne,
    Before it the ransom’d stand;
    No longer are tears, no sorrow is known
    Nor death in that goodly land.
    My Savior has gone before,
    Preparing the way for me;
    Soon we’ll meet to part no more
    Thru time or eternity.
    
    \n\n3. The gates of that holy place
    Stand open by night and day;
    O look to the Lord who “giveth more grace,”
    Whose lone has prepar’d the way.
    A home in those mansions fair,
    His hand hath reserv’d for all;
    For the wedding feast prepare,
    Obeying the gracious call.
    
    \n\n4. They come from the thorny way,
    From regions beyond the sea;
    They come from the vale, from mountain and dale,
    O, Shepherd of men, to Thee,
    They soar to the mount of God,
    Beholding the Bridegroom fair;
    He who trod our earthly sod
    Will welcome each pilgrim there.
    
            \n\n5. Since Jesus has set me free,
    I’m happy as I can be;
    No longer I bear the burden of care,
    His yoke is so sweet to me.
    My soul was as bleak as night,
    But darkness has taken flight;
    Now I have the victory,
    For Jesus has set me free.
    ", "They Come. ","Thoro Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Heaven is a holy place,
    Filled with glory and with grace
    Sin can never enter there;
    All within its gates are pure,
    From defilement kept secure
    Sin can never enter there.
    
    \n\nRefrain:\nSin can never enter there,
    Sin can never enter there;
    So if at the judgment bar
    Sinful spots your soul shall mar,
    You can never enter there.
    
    \n\n2.  If you hope to dwell at last,
    When your life on earth is past,
    In that home so bright and fair,
    You must here be cleansed from sin,
    Have the life of Christ within
    Sin can never enter there.
    
    \n\n3. You may live in sin below,
    Heaven’s grace refuse to know,
    But you cannot enter there;
    It will stop you at the door,
    Bar you out forevermore
    Sin can never enter there.
    
    \n\n4. If you cling to sin till death,
    When you draw your latest breath,
    You will sink in dark despair
    To the regions of the lost,
    Thus to prove at awful cost,
    Sin can never enter there.
    ", "Sin Can Never Enter There. ","Charles W. Naylor (1899) ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. There is soon to be a meeting with our Savior in the air, For he\’s gone to realms of glory, There our mansions to prepare; He is coming back to meet us, With a host of angels Fair, And there\’s sure to be some shouting when we all meet there.
    
    \n\nCho-.\nWhen we all meet there, at the meeting in the air, There is sure to be some shouting when we all meet there; when we all meet there.
    
    \n\n2. See the Patriarchs and Prophets coming forth from bursting tombs, They are coming to this meeting, And they\’re coming very soon; There comes Abraham and Isaac, With their kindred everywhere, There is sure to be some shouting when they all get there.
    
    \n\n3. There is Moses from Mount Nebo, there is Daniel from the cave, They are Marching in the forefront, While the hosts their banners wave; Oh what joy to see them coming to the meeting in the air! There is sure to be some shouting when they all get there.
    
     \n\n4. Still they gather, see them coming, From the mission fields afar, With the pilgrims from all nations, And the mar-tyrs in the war, See St. Paul, the great apostle, In his robes made white and fair, I am sure they\’ll be some shouting when they all get there.
    
     \n\n5. Are you ready for the meeting With our Saver in the air? If your heart is pure and holy, You will be no stranger there: He His coming soon, my brother, With a host of angels fair, And I\’m sure they\’ll be some shouting when we all meet there.
    ", "The Meeting in the Air. ","L. G. Martin ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I have a song I love to sing,
    Since I have been redeemed,
    Of my Redeemer, Savior king,
    Since I have been redeemed.
    
    \n\nRefrain:\nSince I have been redeemed,
    Since I have been redeemed,
    I will glory in His name;
    Since I have been redeemed,
    I will glory in the Savior’s name.
    
    \n\n2. I have a Christ who satisfies
    Since I have been redeemed;
    To do His will my highest prize,
    Since I have been redeemed.
    
    \n\n3. I have a witness bright and clear,
    Since I have been redeemed,
    Dispelling every doubt and fear,
    Since I have been redeemed.
    
    \n\n4. I have a joy I can’t express,
    Since I have been redeemed,
    All through His blood and righteousness,
    Since I have been redeemed.
    
            \n\n5. I have a home prepared for me,
    Since I have been redeemed,
    Where I shall dwell eternally,
    Since I have been redeemed.
    ", "Since I Have Been Redeemed. ","Edwin O. Excell ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. I’ve been on Mount Pisgah’s lofty height,
    And I’ve satisfied my longing heart’s desire;
    For I caught a glimpse of glory bright,
    And my soul is burning with the fire.
    
    \n\n2. I will walk with Jesus, bless His name,
    And to be like Him I every day aspire;
    For His love is like a heav’nly flame,
    And my soul is burning with the fire.
    
    \n\n3. I my all upon the altar lay,
    As I to my closet lovingly retire;
    And the flame consumes while there I pray,
    And my soul is burning with the fire.
    
    \n\n4. By faith’s eye I scan the ocean’s foam,
    And beyond I see the haven I desire;
    There I view the beacon lights of home,
    And my soul is burning with the fire.
    
            \n\n5. O the fire is burning, yes, ’tis brightly burning,
    O ’tis burning, burning in my soul;
    O the fire is burning, yes, ’tis brightly burning,
    O ’tis burning, burning in my soul.
    
    ", "The Fire is Burning. ","Johnson Oatman, Jr., 1898. ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I was sinking deep in sin, far from the peaceful shore,
    Very deeply stained within, sinking to rise no more,
    But the Master of the sea heard my despairing cry,
    From the waters lifted me, now safe am I.
    
            \n\nRefrain:\nLove lifted me!
    Love lifted me!
    When nothing else could help,
    Love lifted me!
    
            \n\n2. All my heart to Him I give, ever to Him I’ll cling,
    In His blessed presence live, ever His praises sing,
    Love so mighty and so true, merits my soul’s best songs,
    Faithful, loving service, too, to Him belongs.
    
            \n\n3. Souls in danger, look above, Jesus completely saves,
    He will lift you by His love, out of the angry waves;
    He’s the Master of the sea, billows His will obey,
    He your Savior wants to be, be saved today.
    ", "Love Lifted Me. ","James Rowe, 1912 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. That God should love a sinner such as I
    Should yearn to change my sorrow into bliss
    Nor rest till He had planned to bring me nigh
    How wonderful is love like this?
    
    \n\nCho:-\nSuch love, such wondrous love
    Such love, such wondrous love
    That God should love a sinner such as I
    How wonderful is love like this.
    
    \n\n2. That Christ should join so freely in the scheme
    Although it meant His death on Calvary
    Did ever human tongue find nobler theme
    Than love divine that ransomed me?
    
    \n\n3. That for a wilful outcast such as I
    The Father planned, the Saviour bled and died
    Redemption for a worthless slave to buy
    Who long had law and grace defied.
    
    \n\n4. And now He takes me to His heart a son
    He asks me not to fill a servant\'s place
    The far-off country wand\'rings all are done
    Wide open are His arms of grace
    ", "Such Love. ","C. Bishop, Robert Harkness ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Hope is singing softly, In her sweetest strain; Won\’t you hear the tidings, Of this glad refrain? He that overcometh, Through His grace Proclaim, We are more than conquerors, In His mighty name.
    
    \n\nCho-.\nIn His name! In His name! We Shall overcome in Jesus name; Bear the joyful tidings, everywhere proclaim, We are more than conquerors, In His mighty name.
    
     \n\n2. When our foes oppress us Hard on every hand; Then we prove Him faithful, by divine command; If two men or demons, Loudly we proclaim; We are more than conquerors, in His mighty name.
    
    \n\n3. Thus we fight our battles, Thus our victories win, Thus we go rejoicing, Overcoming sin; Thus we lift our voices, Joyfully proclaim, We are more than conquerors, in His mighty name.
    
     \n\n4. Then at last triumphant, with all battles won, we shall come rejoicing when He says, “well done;” As we see the saved ones, Loudly we\’ll acclaim, We are more than conquerors, in His mighty name.
    ", "In His Name. ","L. C. Hall ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. When sorrow and storms are besetting my track,
    And Satan is whisp’ring, “You’d better turn back,”
    How oft I have proved it, tho’ dark be the way,
    A little believing drives clouds all away.
    
    \n\nRefrain:\nLord, I believe, Lord, I believe!
    Savior, raise my faith in Thee,
    Till it can move a mountain;
    Lord, I believe, Lord, I believe!
    All my doubts are buried in the fountain.
    
    \n\n2.  How easy when sailing the sea in a calm,
    To trust in the strength of Jehovah’s great arm;
    But somehow I find when the waves swamp the boat,
    It takes some believing to keep things afloat.
    
    \n\n3. “I’ll stand to the end,” I have heard people say,
    “I’ll fight till I die, and will ne’er run away;”
    But when by temptation so fiercely assailed,
    They left off believing, and terribly failed.
    
    \n\n4. And others there are full of courage and zeal,
    Who go to the battle like warriors of steel;
    But right in the heat of the conflict with sin,
    Instead of believing they faint and give in.
    
    \n\n5. Then let us remember in running this race,
    That faith is not feeling, and trust is not trace;
    And when around us seems dark as the night,
    We’ll keep on believing, and win in the fight.
    ", "Lord, I Believe. ","Herbert H. Booth ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I love to sing and pray, rejoicing ev’ry day,
       I love the blessings when the showers on us fall,
       I love the fellowship within the narrow way,
       But I love Jesus best of all.
    
       \n\nCho-:\nFor He saves my soul from sin,
    Gives me peace and joy within;
    I’ve been buried in His name
    And of Him I’m not ashamed,
    For I love Jesus best of all.
    
    \n\n2. I love to testify of all He’s done for me,
       I love the see the lost uplifted from the fall,
       I love to see them turn from all iniquity,
       But I love Jesus best of all.
    
            \n\n3. I love to see the signs that follow saints of God,
       I love to hear the joy bells ringing in my soul,
       I love to know He leads me by
       His staff and rod,
       But I love Jesus best of all.
    ", "I Love Jesus Best of All. ","G T. Harewood ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Beautiful robes so white,
    Beautiful land of light,
    Beautiful home so bright,
    Where there shall come no night;
    Beautiful crown I’ll wear,
    Shining with stars o’er there,
    Yonder in mansions fair,
    Gather us there.
    
            \n\nRefrain:\nBeautiful robes,
    Beautiful land,
    Beautiful home,
    Beautiful band;
    Beautiful crown,
    Shining so fair;
            Beautiful mansion bright,
    Gather us there.
    
            \n\n2. Beautiful thought to me,
    We shall forever be
    Thine in eternity,
    When from this world we’re free;
    Free from its toil and care,
    Heavenly joys to share,
    Let me cross over there,
    This is my prayer.
    
            \n\n3. Beautiful things on high,
    Over in yonder sky,
    Thus I shall leave this shore,
    Counting my treasures o’er;
    Where we shall never die,
    Carry me, by and by,
    Never to sorrow more,
    Heavenly store.
    ", "Beautiful. "," Barney E. Warren (1897) ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. She only touched the hem of His garment
      As to His side she stole,
    Amid the crowd that gathered around Him;
      And straightway she was whole.
    
         \n\nCho-:\nOh, touch the hem of His garment,
      And thou, too, shalt be free!
    His healing pow’r this very hour
        Shall give new life to thee!
    
            \n\n2. She came in fear and trembling before Him,
      She knew her Lord had come,
    She felt that from Him virtue had healed her,
      The mighty deed was done.
    
            \n\n3. He turned with “Daughter, be of good comfort,
      Thy faith hath made thee whole“;
    And peace that passeth all understanding
      With gladness filled her soul.
    ", "The Hem of His Garment. ","George F. Root ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Jesus is coming! go, herald the tidings
    Far over the land and the sea;
    Jesus is coming to gather the ransomed,
    Redeemed by His death on the tree.
    Angels attending, the heavens descending—
    All language our joy will transcend,
    When we shall see Him, the king in His beauty,
    Our bridegroom, Redeemer, and friend.
    
    \n\nRefrain\nJesus is coming, our Savior and lover divine;
    Soon in His glory the ransomed of ages will shine;
    Ages on ages we’ll reign with our king on His throne;
    Wonderful story! We’ll share in His glory,
    Redeemed by His mercy divine.
    
            \n\n2.  Lift up your heads and rejoice, O ye righteous,
    Your perfect redemption is nigh;
    Swiftly the darkness of midnight approaches,
    He cometh shall sound from the sky.
    Long has the battle been waged against evil
    By suffering saints here below.
    Jesus is coming to banish our sorrow,
    And lead us where tears never flow.
    
            \n\n3. Come, dear Lord Jesus, Thy promise fulfilling;
    Come quickly! our souls cry to Thee:
    Long has the world been enthralled by the tempter,
    The curse is on land and on sea.
    Yet Thou shalt triumph, the nations subduing,
    The truth of the Word cannot fail;
    God’s wondrous glory, like ocean’s deep billows,
    To earth’s farthest bounds shall prevail.
    ", "He Cometh! ","William T. Pettengell ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Hear the blessed Savior calling the oppressed,
    “Oh, ye heavy-laden, come to Me and rest;
    Come, no longer tarry, I your load will bear,
    Bring Me every burden, bring Me every care.”
    
            \n\nRefrain:\nCome unto Me, I will give you rest;
    Take My yoke upon you, hear Me and be blest;
    I am meek and lowly, come and trust My might;
    Come, My yoke is easy, and My burden’s light.
    
           \n\n2. Are you disappointed, wand’ring here and there,
    Dragging chains of doubt and loaded down with care?
    Do unholy feelings struggle in your breast?
    Bring your case to Jesus—He will give you rest.
    
            \n\n3. Stumbling on the mountains dark with sin and shame,
    Stumbling toward the pit of hell’s consuming flame;
    By the pow’rs of sin deluded and oppressed,
    Hear the tender Shepherd, “Come to Me and rest.”
    
            \n\n4. Have you cares of business, cares of pressing dept?
            Cares of social life or cares of hopes unmet?
            Are you by remorse or sense of guilt depressed?
            Come right on to Jesus, He will give you rest.
    
            \n\n5. Have you by temptation often conquered been,
    Has a sense of weakness brought distress within?
    Christ will sanctify you, if you’ll claim His best;
    In the Holy Spirit, He will give you rest.
    ", "Come Unto Me. ","Charles P. Jones, 1908 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. How often Holy converse with Christ, my Lord alone, I seem to hear the millions That sing around His throne.
    
     \n\nCho-.\nHallelujah, Amen, Hallelujah, Amen. Hallelujah, Amen. Amen, Amen.
    
    \n\n2. They passed through toils and trials, And tho\’ the strife was long, They share the victor\’s conquest, And sing the victor\’s song.
    
    \n\n3. My soul takes up the chorus, And pressing on my way, Communing still with Jesus, I sing from day to day.
    
    \n\n4. Thro\’ Grace I soon shall conquer, And reach my home on high; And thro\’ eternal ages I\’ll shout beyond the sky.
    ", "Hallelujah! Amen. ","Franny Crosby, 1885 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I cannot tell thee whence it came,
    This peace within my breast;
    But this I know, there fills my soul
    A strange and tranquil rest.
    
    \n\nRefrain\nThere’s a deep, settled peace in my soul,
    There’s a deep, settled peace in my soul,
    Tho’ the billows of sin near me roll,
    He abides, Christ abides.
    
    \n\n2.  Beneath the toil and care of life,
    This hidden stream flows on;
    My weary soul no longer thirsts,
    Nor am I sad and lone.
    
    \n\n3. I cannot tell the half of love,
    Unfeigned, supreme, divine,
    That caused my darkest inmost self
    With beams of hope to shine.
    
    \n\n4. I cannot tell thee why He chose
    To suffer and to die,
    But if I suffer here with Him
    I’ll reign with Him on high.
    ", "Hidden Peace. ","John S. Brown ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Hark! The herald angels sing,
    “Glory to the newborn King;
    Peace on earth, and mercy mild,
    God and sinners reconciled!”
    Joyful, all ye nations rise,
    Join the triumph of the skies;
    With th’angelic host proclaim,
    “Christ is born in Bethlehem!”
    
    \n\nRefrain:\nHark! the herald angels sing,
    “Glory to the newborn King!”
    
            \n\n2. Christ, by highest Heav’n adored;
    Christ the everlasting Lord;
    Late in time, behold Him come,
    Offspring of a virgin’s womb.
    Veiled in flesh the Godhead see;
    Hail th’incarnate Deity,
    Pleased with us in flesh to dwell,
    Jesus our Emmanuel.
    
    \n\n3. Hail the heav’nly Prince of Peace!
    Hail the Sun of Righteousness!
    Light and life to all He brings,
    Ris’n with healing in His wings.
    Mild He lays His glory by,
    Born that man no more may die;
    Born to raise the sons of earth,
    Born to give them second birth.
    
    \n\n4. Come, Desire of nations, come,
    Fix in us Thy humble home;
    Rise, the woman’s conqu’ring Seed,
    Bruise in us the serpent’s head.
    Now display Thy saving pow’r,
    Ruined nature now restore;
    Now in mystic union join
    Thine to ours, and ours to Thine.
    
    \n\n5. Adam’s likeness, Lord, efface,
    Stamp Thine image in its place:
    Second Adam from above,
    Reinstate us in Thy love.
    Let us Thee, though lost, regain,
    Thee, the Life, the inner man:
    Oh, to all Thyself impart,
    Formed in each believing heart.
    ", "Hark! the Herald Angels Sing. ","Charles Wesley, 1739 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. O little town of Bethlehem,
    how still we see thee lie;
    above thy deep and dreamless sleep
    the silent stars go by.
    Yet in thy dark streets shineth
    the everlasting light;
    the hopes and fears of all the years
    are met in thee tonight.
    
    \n\n2.     For Christ is born of Mary,
    and gathered all above,
    while mortals sleep, the angels keep
    their watch of wondering love.
    O morning stars together,
    proclaim the holy birth,
    and praises sing to God the king,
    and peace to all on earth!
    
    \n\n3. How silently, how silently,
    the wondrous gift is given;
    so God imparts to human hearts
    the blessings of his heaven.
    No ear may hear his coming,
    but in this world of sin,
    where meek souls will receive him, still
    the dear Christ enters in.
    
    \n\n4. O holy Child of Bethlehem,
    descend to us, we pray;
    cast out our sin, and enter in,
    be born in us today.
    We hear the Christmas angels
    the great glad tidings tell;
    o come to us, abide with us,
    our Lord Emmanuel!
    ", "O Little Town of Bethlehem. ","Phillips Brooks (1868) ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Angels from the realms of glory,
    Wing your flight o’er all the earth;
    Ye who sang creation’s story
    Now proclaim Messiah’s birth.
    
    \n\nRefrain:\nCome and worship, come and worship,
    Worship Christ, the newborn King.
    
            \n\n2. Shepherds, in the field abiding,
    Watching o’er your flocks by night,
    God with us is now residing;
    Yonder shines the infant light:
    
    \n\n3. Sages, leave your contemplations,
    Brighter visions beam afar;
    Seek the great Desire of nations;
    Ye have seen His natal star.
    
    \n\n4. Saints, before the altar bending,
    Watching long in hope and fear;
    Suddenly the Lord, descending,
    In His temple shall appear.
    ", "Angels, From the Realms of Glory. ","James Montgomery, 1816 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Silent night! Holy night!
    All is calm, all is bright
    ’round yon virgin mother and child!
    Holy infant, so tender and mild,
    sleep in heavenly peace,
    sleep in heavenly peace.
    
    \n\n2. Silent night, holiest night!
    Darkness flies, all is light!
    Shepherds hear the angels sing;
    Hallelujah! hail the King!
    Christ, the Savior is born,
    Christ, the Savior is born.
    
            \n\n3. Silent night, holiest night!
            Guiding Star, lend thy light;
            See the eastern wise men bring
            Gifts and homage to our King!
            Christ, the Savior is born,
            Christ, the Savior is born.
    
    \n\n4. Silent night! Holy night!
    Wondrous star, lend thy light;
    with the angels let us sing
    'Alleluia' to our King:
    “Christ the Savior is born!
    Christ the Savior is born.”
    
            \n\n5.  Silent night! Holy night!
    Shepherds quake at the sight.
    Glories stream from heaven afar,
    heav\'nly hosts sing: “Alleluia!
    Christ the Savior is born!
    Christ the Savior is born!”
    
    \n\n6. Silent night! Holy night!
    Son of God, love’s pure light
    radiant beams from Thy holy face
    with the dawn of redeeming grace,
    Jesus, Lord, at Thy birth!
    Jesus, Lord, at Thy birth!
    ", "Silent Night, Holy Night. ","Joseph Mohr ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I have been alone with Jesus with my head upon His breast,
    For I was so very weary that I waited there to rest.
    I have been alone with Jesus and He bade me stay awhile,
    And I felt it very precious in the sunshine of His smile.
    
    \n\nRefrain:\nI’ve been alone with Jesus,
    My blessed, blessed Jesus,
    I’ve been alone with Jesus,
    In the sunshine of his smile.
    
    \n\n2.  With a trembling heart I told Him while with joy I lingered there,
    All the burden of my sorrow and my heavy weight of care;
    How the voice of Satan’s whisp’rings often called me into sin,
    And I asked him if I might not say forever there with Him.
    
    \n\n3. Shall I tell you what He told me while I still was waiting there,
    For it took away my troubles and it took away my care.
    Oh! He told me how He loved me tho’ a wayward, erring child,
    And I felt so very happy as He looked on me and smiled.
    
    \n\n4. Then He told me I was welcome evermore with him to stay,
    And He said that He would never cast His loving child away,
    Lo! He said, I am thy Saviour, as a rock I firmly stand
    Come and rest beneath my shadow in this weary, thirsty land.
    ", "I Have Been Alone With Jesus. ","Fanny Lonsdale ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Standing by a purpose true,
    Heeding God’s command,
    Honor them, the faithful few!
    All hail to Daniel’s band!
    
    \n\nRefrain:\nDare to be a Daniel,
    Dare to stand alone!
    Dare to have a purpose firm!
    Dare to make it known.
    
    \n\n2. Many mighty men are lost,
    Daring not to stand,
    Who for God had been a host
    By joining Daniel’s band.
    
    \n\n3. Many giants, great and tall,
    Stalking through the land,
    Headlong to the earth would fall,
    If met by Daniel’s band.
    
    \n\n4. Hold the Gospel banner high!
    On to vict’ry grand!
    Satan and his hosts defy,
    And shout for Daniel’s band.
    ", "Dare to Be a Daniel. ","Philip P. Bliss, 1873 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Those were darksome years filled with gloomy fears
    When my life was marred by sin
    But the sunbeams play Round my path today
    And the light of heav’n streams in.
    
    \n\nCho-:\nSince the fullness of the light shone in,
    Clearing all the mists of doubt and sin,
    I can always see the face of Jesus,
    Since the fullness of the light shone in.
    
    \n\n2. Now He saves each hour by His wondrous pow’r
    How can I his praise begin?
    There’s a peace divine in this heart of mine since the glory light shone in.
    
    \n\n3. There is rest complete at the scared feet of the ever blessed One,
    For the love of God Now is shed abroad and eternal life begun.
    
    \n\n4. For my Lord so true this I mean to do:
    To His fold some soul to win
    Telling all around what a joy I’ve found since the golden light shone in.
    
    \n\n5. I shall dwell sometime in the realm sublime of the city pearly white
    And forevermore worship and adore where the Lamb is all the light.
    ", "Since the Fullness of the Light Shone in. ","Thoro Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. There’s a country far beyond the starry sky,
    There’s a city where there never comes a night;
    If we’re faithful we shall go there by and by,
    ’Tis the city where the Lamb is the light.
    
    \n\nRefrain:\nIn that city where the Lamb is the light,
    The city where there cometh no night;
    I’ve a mansion over there,
    And when free from toil and care,
    I am going where the Lamb is the light.
    
    \n\n2.  Here we have our days of sunshine, but we know
    That the sun which shines upon us now so bright
    Will be changed to clouds and rain until we go
    To the city where the Lamb is the light.
    
    \n\n3. There the flowers bloom forever and the day
    Shall be one eternal day without a night;
    And our tears shall be forever wiped away,
    In that city where the Lamb is the light.
    
    \n\n4. Here we have our disappointments all the while,
    And our fondest hopes but meet with bitter blight;
    Tho’ by night we weep, the morning brings a smile,
    In that city where the Lamb is the light
    
    \n\n5. Then let sunlight fade, let twilight bring its gloom,
    Not a shadow can my blissful soul affright;
    For I know that up in Heaven there is room,
    In that city where the Lamb is the light.
    ", "In the City Where the Lamb is Light. ","Herbert Buffum ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Soon our Lord that “far country” will return to claim His own: See them rise, joyfully rise! We will all go forth to meet Him, for this truth we long have known.
    
    \n\nCho-.\nSee them rise, (Hallelujah!) joyfully rise! (Praise the Lord!) Then from ocean to ocean will the old tombs burst: See them rise! (Hallelujah!) Joyfully rise! (Praise the Lord! they who died in the gospel will arise the first) See them rise! (Hallelujah!) Joyfully rise!
    
    \n\n2. We will comfort one another by this word of Holy cheer: And we know the hour draws nearer when the Savior shall appear.
    
    \n\n3. O the hope of Jesus coming is an anchor sure and fast; In the image of His glory we will stand redeemed at last.
    
    \n\n4. Let us sing it, let us tell it to the world, where\’er we go; Hallelujah! Praise the Lord! That the message of the kingdom of ev\’ry longing heart may know.
    ", "See Them Rise. ","Thoro Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. I am glad I found the Savior, For He makes my heart rejoice; In His name I find Salvation Thru His redeeming blood.
    
    \n\nCho-.\nHe is Lord of lords and King of kings, The beginning and the end, The Father, Son and Holy Ghost, The dying sinner\’s Friend. If you will hear His voice, Be buried in His name, Then the Comforter will come to abide.
    
    \n\n2. He His fear than the fairest, He\’s the Life, the Truth, the Way, He\’s the Alpha and Omega; I love His name today!
    
     \n\n3. He was bruised for our transgression; Sinners pierced His sacred side; By His own was He forsaken, He suffered, bled and died.
    
    \n\n4. But the third day in the morning, just about the break of day, from the grave He rose triumphant And led to heaven the way.
    ", "The Lord of Lords. ","G. T. Harewood ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. I am thinking of the rapture in our blessed home on high.
    When the redeemed are gathering in;
    How we’ll raise the heav’nly anthem in that city in the sky,
    When the redeemed are gathering in.
    
    \n\nRefrain:\nWhen the redeemed are gathering in,
    Washed like snow, and free from all sin;
    How we will shout, and how we will sing,
    When the redeemed are gathering in.
    
    \n\n2.  There will be a great procession over on the streets of gold,
    When the redeemed are gathering in;
    O what music, O what singing, o’er the city will be rolled,
    When the redeemed are gathering in.
    
    \n\n3. Saints will sing redemption’s story with their voices clear and strong,
    When the redeemed are gathering in;
    Then the angels all will listen, for they cannot join that song,
    When the redeemed are gathering in.
    
    \n\n4. Then the Savior will give orders to prepare the banquet board,
    When the redeemed are gathering in;
    And we’ll hear His invitation, “Come, ye blessed of the Lord,”
    When the redeemed are gathering in.
    ", "When the Redeemed Are Gathering In. ","Johnson Oatman ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. \'Tis almost time for the Lord to come,
    I hear the people say,
    The stars of heaven are growing dim,
    It must be the breaking of the day.
    
    \n\nCho:-\nO it must be the breaking of the day,
    O it must be the breaking of the day.
    The night is almost gone,
    The day is coming on,
    O it must be the breaking of the day.
    
    \n\n2.  The signs foretold in the sun and moon,
    In earth, and sea, and sky;
    Aloud proclaim to the race of men,
    That the coming of the Master draweth nigh.
    
    \n\n3. It must be time for the waiting church
    To cast her pride away;
    With girded loins and burning lamps,
    To look for the breaking of the day.
    
    \n\n4. There must be those in the field of sin,
    Far from the fold astray.
    Who once were happy in Jesus\' love,
    And looking for the breaking of the day.
    
    \n\n5. Go quickly out in the streets and lanes,
    And in the broad highway,
    And call the maimed, the halt and blind,
    To be ready for the breaking of the day.
    ", "The Breaking of the Day. ","G. W. Sederquist ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. We have heard the glorious news of our returning Lord; Jesus is coming, Hallelujah! Now on all the earth abroad the latter rain is poured; Jesus is coming, Hallelujah!
    
    \n\nCho-.\nThe Savior is coming, is coming again, Coming in glory, coming to reign let the “little folk” rejoice and sing the glad refrain, Jesus is coming, Hallelujah!
    
    \n\n2. We have seen the signs fulfilling every passing year; Jesus is coming, Hallelujah! Tho\’ the hearts of sinful men are failing them for fear, Jesus is coming, Hallelujah!
    
    \n\n3. Let us bear the joyful news over oceans surging main, Jesus is coming, Hallelujah! That the mighty King of kings is coming back again, Jesus is coming, Hallelujah!
    
    \n\n4. Speed, Oh speed Thee on Thy way, Thou blessed Prince of Peace! Jesus is coming, Hallelujah! When the golden morn shall break that brings from death release, Jesus is coming, Hallelujah!
    ", "Jesus Is Coming, Hallelujah! ","Thoro Harris ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Redeemed, how I love to proclaim it!
    Redeemed by the blood of the Lamb;
    Redeemed through His infinite mercy,
    His child and forever I am.
    
            \n\nRefrain:\nRedeemed, redeemed,
    Redeemed by the blood of the Lamb;
    Redeemed, redeemed,
    His child and forever I am.
    
    \n\n2. Redeemed, and so happy in Jesus,
    No language my rapture can tell;
    I know that the light of His presence
    With me doth continually dwell.
    
    \n\n3. I think of my blessed Redeemer,
    I think of Him all the day long:
    I sing, for I cannot be silent;
    His love is the theme of my song.
    
    \n\n4. I know I shall see in His beauty
    The King in whose law I delight;
    Who lovingly guardeth my footsteps,
    And giveth me songs in the night.
    
    \n\n5. I know there’s a crown that is waiting
    In yonder bright mansion for me,
    And soon, with the spirits made perfect,
    At home with the Lord I shall be.
    ", "Redeemed. ","Frances J. Crosby, 1882 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. When the mists have rolled in splendor
    From the beauty of the hills,
    And the sunlight falls in gladness
    On the rivers and the rills,
    We recall our Father’s promise
    In the rainbow of the spray:
    We shall know each other better
    When the mists have rolled away.
    
            \n\nRefrain:\nWe shall know, as we are known,
    Nevermore to walk alone,
    In the dawning of the morning
    Of that bright and happy day,
    We shall know each other better,
    When the mists have rolled away.
    
    \n\n2. Oft we tread the path before us
    With a weary, burdened heart;
    Oft we toil amid the shadows,
    And our fields are far apart;
    But the Savior’s “Come, ye blessed”
    All our labor will repay,
    When we gather in the morning
    Where the mists have rolled away.
    
    \n\n3. We shall come with joy and gladness,
    We shall gather round the throne;
    Face to face with those that love us,
    We shall know as we are known;
    And the song of our redemption
    Shall resound through endless day,
    When the shadows have departed,
    And the mists have rolled away.
    ", "When the Mists Have Rolled Away. ","Annie H. Barker, 1883 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Brightly beams our Father’s mercy,
    From His lighthouse evermore,
    But to us He gives the keeping
    Of the lights along the shore.
    
            \n\nRefrain:\nLet the lower lights be burning!
    Send a gleam across the wave!
    Some poor *struggling, fainting seaman
    You may rescue, you may save.
    
    \n\n2. Dark the night of sin has settled,
    Loud the angry billows roar;
    Eager eyes are watching, longing,
    For the lights along the shore.
    
    \n\n3. Trim your feeble lamp, my brother;
    Some poor sailor, tempest-tossed,
    Trying now to make the harbor,
    In the darkness may be lost.
    ", "Let the Lower Lights Be Burning. ","Philip P. Bliss, 1871 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. My Father is rich in houses and lands,
    He holdeth the wealth of the world in His hands!
    Of rubies and diamonds, of silver and gold,
    His coffers are full, He has riches untold.
    
            \n\nRefrain:\nI’m a child of the King,
    A child of the King:
    With Jesus my Savior,
    I’m a child of the King.
    
    \n\n2. My Father’s own Son, the Savior of men,
    Once wandered on earth as the poorest of them;
    But now He is pleading our pardon on high,
    That we may be His, when He comes by and by.
    
    \n\n3. I once was an outcast stranger on earth,
    A sinner by choice, an alien by birth,
    But I’ve been adopted, my name’s written down,
    An heir to a mansion, a robe and a crown.
    
    \n\n4. A tent or a cottage, why should I care?
    They’re building a palace for me over there;
    Though exiled from home, yet still may I sing:
    All glory to God, I’m a child of the King.
    ", "A Child of the King. ","Harriet E. Buell, 1877 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. To God be the glory, great things He hath done,
    So loved He the world that He gave us His Son,
    Who yielded His life our redemption to win,
    And opened the life-gate that all may go in.
    
            \n\nCho-:\nPraise the Lord, praise the Lord,
      Let the earth hear His voice;
    Praise the Lord, praise the Lord,
      Let the people rejoice;
    Oh, come to the Father, through Jesus the Son,
      And give Him the glory; great things He hath done.
    
    \n\n2. Oh, perfect redemption, the purchase of blood,
    To every believer the promise of God;
    The vilest offender who truly believes,
    That moment from Jesus a pardon receives.
    
    \n\n3. Great things He hath taught us, great things He hath done,
    And great our rejoicing through Jesus the Son;
    But purer, and higher, and greater will be
    Our wonder, our transport when Jesus we see.
    ", "Praise the Lord. ","Franny J. Crosby, 1875 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Since I started out to find Thee,
    Since I to the cross did flee,
    Every bridge is burned behind me;
    I will never turn from Thee.
    
    \n\nRefrain:\nStrengthen all the ties that bind me
    Closer, closer, Lord to Thee,
    Every bridge is burned behind me;
    Thine I evermore will be.
    
    \n\n2.  Thou didst hear my plea so kindly;
    Thou didst grant me so much grace,
    Every bridge is burned behind me;
    I will ne’er my steps retrace.
    
    \n\n3. Cares of life perplex and grind me,
    Yet I keep the narrow way.
    Every bridge is burned behind me;
    I from Thee will never stray.
    
    \n\n4. All in All I ever find Thee,
    Savior, Lover, Brother, Friend,
    Every bridge is burned behind me;
    I will serve Thee to the end.
    ", "Every Bridge is Burned Behind Me. ","Johnson Oatman ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. We read of a place that’s called heaven,
    It’s made for the pure and the free;
    These truths in God’s Word He hath given,
    How beautiful heaven must be.
    
            \n\nRefrain:\nHow beautiful heaven must be,
    Sweet home of the happy and free;
    Fair haven of rest for the weary,
    How beautiful heaven must be.
    
    \n\n2. In heaven no drooping nor pining,
    No wishing for elsewhere to be;
    God’s light is forever there shining,
    How beautiful heaven must be.
    
    \n\n3. Pure waters of life there are flowing,
    And all who will drink may be free;
    Rare jewels of splendor are glowing,
    How beautiful heaven must be.
    
    \n\n4. The angels so sweetly are singing,
    Up there by the beautiful sea;
    Sweet chords from their gold harps are ringing,
    How beautiful heaven must be.
    ", "How Beautiful Heaven Must Be. ","A. S. Bridgewater, 1920 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. As I journey through the land, singing as I go,
    Pointing souls to Calvary—to the crimson flow,
    Many arrows pierce my soul from without, within;
    But my Lord leads me on, through Him I must win.
    
            \n\nRefrain:\nOh, I want to see Him, look upon His face,
    There to sing forever of His saving grace;
    On the streets of glory let me lift my voice,
    Cares all past, home at last, ever to rejoice.
    
    \n\n2. When in service for my Lord dark may be the night,
    But I’ll cling more close to Him, He will give me light;
    Satan’s snares may vex my soul, turn my thoughts aside;
    But my Lord goes ahead, leads whate’er betide.
    
    \n\n3. When in valleys low I look toward the mountain height,
    And behold my Savior there, leading in the fight,
    With a tender hand outstretched toward the valley low,
    Guiding me, I can see, as I onward go.
    
    \n\n4. When before me billows rise from the mighty deep,
    Then my Lord directs my bark; He doth safely keep,
    And He leads me gently on through this world below;
    He’s a real Friend to me, oh, I love Him so.
    ", "O I Want to See Him. ","Rufus H. Cornelius, 1916 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Rock of Ages, cleft for me,
    let me hide myself in thee;
    let the water and the blood,
    from thy wounded side which flowed,
    be of sin the double cure;
    save from wrath and make me pure.
    
            \n\n2. Could my tears forever flow,
            Could my zeal no languor know,
            These for sin could not atone;
            Thou must save, and Thou alone:
            In my hand no price I bring,
            Simply to thy cross I cling.
    
    \n\n3. While I draw this fleeting breath,
    when mine eyes shall close in death,
    when I soar to worlds unknown,
    see thee on thy judgment throne,
    Rock of Ages, cleft for me,
    let me hide myself in thee.
    
            \n\n4. Not the labors of my hands
    can fulfill thy law\'s demands;
    could my zeal no respite know,
    could my tears forever flow,
    all for sin could not atone;
    thou must save, and thou alone.
    
            \n\n5. Nothing in my hand I bring,
    simply to the cross I cling;
    naked, come to thee for dress;
    helpless, look to thee for grace;
    foul, I to the fountain fly;
    wash me, Savior, or I die.
    ", "Rock of Ages. ","Augustus M. Toplady, 1776 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Holy Ghost! with light divine,
    Shine upon this heart of mine;
    Chase the shades of night away,
    Turn the darkness into day.
    
    \n\n2. Holy Ghost!, with pow\'r divine,
    Cleanse this guilty heart of mine;
    Long hath sin without control,
            Held dominion o\'ver my soul.
    
    \n\n3. Holy Ghost!, with joy divine
    Cheer this saddened heart of mine;
    Bid my many woes depart,
            Heal my wounded, bleeding heart.
    
    \n\n4. Holy Spirit, all divine,
    Dwell within this heart of mine;
    Cast down ev\'ry idol throne,
    Reign supreme, and reign alone.
    
    \n\n5.  Let me see my Savior\'s face,
    Let me all His beauties trace;
    Show those glorious truths to me
    Which are only known to Thee.
    ", "Holy Ghost! with Light Divine. ","Andrew Reed (1817) ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("       1. Conquering now and still to conquer, rideth a King in His might;
    Leading the host of all the faithful into the midst of the fight;
    See them with courage advancing, clad in their brilliant array,
    Shouting the Name of their Leader, hear them exultingly say:
    
            \n\nRefrain:\nNot to the strong is the battle, not to the swift is the race,
    Yet to the true and the faithful vict’ry is promised through grace.
    
            \n\n2. Conquering now and still to conquer, who is this wonderful King?
    Whence are the armies which He leadeth, while of His glory they sing?
    He is our Lord and Redeemer, Savior and Monarch divine;
            They are the stars that forever bright in His kingdom shall shine.
    
            \n\n3. Conquering now and still to conquer, Jesus, Thou Ruler of all,
    Thrones and their scepters all shall perish, crowns and their splendor shall fall,
    Yet shall the armies Thou leadest, faithful and true to the last,
    Find in Thy mansions eternal rest, when their warfare is past.
    ", "Victory Through Grace. ","Frances J. Crosby, 1890 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Praise Him! Praise Him! Jesus, our blessed Redeemer!
    Sing, O Earth, His wonderful love proclaim!
    Hail Him! Hail Him! Highest archangels in glory;
    Strength and honor give to His holy Name!
    Like a shepherd, Jesus will guard His children,
    In His arms He carries them all day long.
    
            \n\nRefrain:\nPraise Him! Praise Him! Tell of His excellent greatness;
    Praise Him! Praise Him! Ever in joyful song!
    
            \n\n2. Praise Him! Praise Him! Jesus, our blessed Redeemer!
    For our sins He suffered, and bled, and died.
            He our Rock, our hope of eternal salvation,
    Hail Him! Hail Him! Jesus the Crucified.
    Sound His praises! Jesus who bore our sorrows,
    Love unbounded, wonderful, deep and strong.
    
    \n\n3. Praise Him! Praise Him! Jesus, our blessed Redeemer!
    Heav’nly portals loud with hosannas ring!
    Jesus, Savior, reigneth forever and ever;
    Crown Him! Crown Him! Prophet, and Priest, and King!
    Christ is coming! over the world victorious,
    Pow’r and glory unto the Lord belong.
    ", "Praise Him! Praise Him! ","Frances J. Crosby, 1869 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. What a friend we have in Jesus,
    All our sins and griefs to bear!
    What a privilege to carry
    Everything to God in prayer!
    Oh, what peace we often forfeit,
    Oh, what needless pain we bear,
    All because we do not carry
    Everything to God in prayer!
    
    \n\n2. Have we trials and temptations?
    Is there trouble anywhere?
    We should never be discouraged—
    Take it to the Lord in prayer.
    Can we find a friend so faithful,
    Who will all our sorrows share?
    Jesus knows our every weakness;
    Take it to the Lord in prayer.
    
    \n\n3. Are we weak and heavy-laden,
    Cumbered with a load of care?
    Precious Savior, still our refuge—
    Take it to the Lord in prayer.
    Do thy friends despise, forsake thee?
    Take it to the Lord in prayer!
    In His arms He’ll take and shield thee,
    Thou wilt find a solace there.
    
    \n\n4. Blessed Savior, Thou hast promised
    Thou wilt all our burdens bear;
    May we ever, Lord, be bringing
    All to Thee in earnest prayer.
    Soon in glory bright, unclouded,
    There will be no need for prayer—
    Rapture, praise, and endless worship
    Will be our sweet portion there.
    ", "What a Friend. ","Joseph M. Scriven, 1855 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I have heard my  Savior calling,
    I have heard my  Savior calling,
    I have heard the  Savior calling,
    “Take thy cross and follow, follow Me.”
    
    \n\nRefrain:\nWhere He leads me I will follow,
    Where He leads me I will follow,
    Where He leads me I will follow,
    I’ll go with Him, with Him all the way.
    
    
    \n\n2. Tho\' He lead me thro\' the garden,
    Tho\' He lead me thro\' the garden,
    Tho\' He lead me thro\' the garden,
    I’ll go with Him, with Him all the way.
    
    \n\n3. Tho\' He lead me thro\' the conflict,
    Tho\' He lead me thro\' the conflict,
    Tho\' He lead me thro\' the conflict,
    I’ll go with Him, with Him all the way.
    
    \n\n4. He will give me grace and glory,
    He will give me grace and glory,
    He will give me grace and glory,
    He will keep me, keep me all the way.
    ", "Where He Leads Me. ","Geo. W. Collins ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Jesus, keep me near the cross,
    There a precious fountain—
    Free to all, a healing stream—
    Flows from Calv’ry’s mountain.
    
            \n\nRefrain:\nIn the cross, in the cross,
    Be my glory ever;
    Till my raptured soul shall find
    Rest beyond the river.
    
    \n\n2. Near the cross, a trembling soul,
    Love and Mercy found me;
    There the bright and morning star
    Sheds its beams around me.
    
    \n\n3. Near the cross! O Lamb of God,
    Bring its scenes before me;
    Help me walk from day to day,
    With its shadows o’er me.
    
    \n\n4. Near the cross I’ll watch and wait
    Hoping, trusting ever,
    Till I reach the golden strand,
    Just beyond the river.
    ", "Near the Cross. ","Fanny J. Crosby ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I am Thine, O Lord, I have heard Thy voice,
    And it told Thy love to me;
    But I long to rise in the arms of faith
    And be closer drawn to Thee.
    
            \n\nRefrain:\nDraw me nearer, nearer blessed Lord,
    To the cross where Thou hast died;
    Draw me nearer, nearer, nearer blessed Lord,
    To Thy precious, bleeding side.
    
    \n\n2. Consecrate me now to Thy service, Lord,
    By the pow’r of grace divine;
    Let my soul look up with a steadfast hope,
    And my will be lost in Thine.
    
    \n\n3. Oh, the pure delight of a single hour
    That before Thy throne I spend,
    When I kneel in prayer, and with Thee, my God
    I commune as friend with friend!
    
    \n\n4. There are depths of love that I cannot know
    Till I cross the narrow sea;
    There are heights of joy that I may not reach
    Till I rest in peace with Thee.
    ", "Draw Me Nearer. ","Franny J. Crosby  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. On the happy, golden shore,
    Where the faithful part no more,
    When the storms of life are o’er,
    Meet me there;
    Where the night dissolves away
    Into pure and perfect day,
    I am going home to stay—
    Meet me there.
    
            \n\nRefrain:\nMeet me there, meet me there,
    Where the tree of life is blooming,
    Meet me there;
    When the storms of life are o’er,
    On the happy golden shore,
    Where the faithful part no more,
    Meet me there.
    
    \n\n2. Here our fondest hopes are vain,
    Dearest links are rent in twain,
    But in Heav’n no throb of pain—
    Meet me there;
    By the river sparkling bright
    In the city of delight,
    Where our faith is lost in sight,
    Meet me there.
    
    \n\n3. Where the harps of angels ring,
    And the blest forever sing,
    In the palace of the King,
    Meet me there;
    Where in sweet communion blend
    Heart with heart and friend with friend,
    In a world that ne’er shall end,
    Meet me there.
    ", "Meet Me There. ","H. E. Blair, Wm. J. Kirkpatrick  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. There is a Name I love to hear,
    I love to sing its worth;
    It sounds like music in my ear,
    The sweetest Name on earth.
    
            \n\nRefrain:\nOh, how I love Jesus,
    Oh, how I love Jesus,
    Oh, how I love Jesus,
    Because He first loved me!
    
            \n\n2. It tells me of a Savior’s love,
    Who died to set me free;
    It tells me of His precious blood,
    The sinner’s perfect plea.
    
    \n\n3. It tells me what my Father hath
    In store for every day,
    And though I tread a darksome path,
    Yields sunshine all the way.
    
    \n\n4. It tells of One whose loving heart
    Can feel my deepest woe;
    Who in each sorrow bears a part
    That none can bear below.
    
    \n\n5. It bids my trembling heart rejoice;
    It dries each rising tear;
    It tells me, in a “still small voice,”
    To trust and never fear.
    
    \n\n6. Jesus, the Name I love so well,
    The Name I love to hear:
    No saint on earth its worth can tell,
    No heart conceive how dear.
    
    \n\n7. This Name shall shed its fragrance still
    Along this thorny road,
    Shall sweetly smooth the rugged hill
    That leads me up to God.
    
    \n\n8. And there with all the blood-bought throng,
    From sin and sorrow free,
    I’ll sing the new eternal song
    Of Jesus’ love for me.
    ", "O How I Love Jesus. ","F. Whitfield  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. There is a fountain filled with blood,
    Drawn from Immanuel’s veins,
    And sinners plunged beneath that flood
    Lose all their guilty stains:
    Lose all their guilty stains,
    Lose all their guilty stains;
            And sinners plunged beneath that flood
    Lose all their guilty stains.
    
    \n\n2. The dying thief rejoiced to see
    That fountain in His day;
    And there have I, though vile as he,
    Washed all my sins away:
    Washed all my sins away,
    Washed all my sins away;
            And there have I, though vile as he,
    Washed all my sins away.
    
    \n\n3. Dear dying Lamb, Thy precious blood
    Shall never lose its pow’r,
    Till all the ransomed church of God
    Are safe, to sin no more:
    Are safe, to sin no more,
    Are safe, to sin no more;
            Till all the ransomed church of God
    Are safe, to sin no more.
    
    \n\n4. E’er since by faith I saw the stream
    Thy flowing wounds supply,
    Redeeming love has been my theme,
    And shall be till I die:
    And shall be till I die,
    And shall be till I die;
    Redeeming love has been my theme,
    And shall be till I die.
    
    \n\n5. Then in a nobler, sweeter song,
    I’ll sing Thy power to save,
    When this poor lisping, stammering tongue
    Lies silent in the grave.
    Lies silent in the grave,
    Lies silent in the grave;
    ", "There is a Fountain. ","William Cowper  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Amazing grace! How sweet the sound
    That saved a wretch like me!
    I once was lost, but now am found;
    Was blind, but now I see.
    
    \n\n2. ’Twas grace that taught my heart to fear,
    And grace my fears relieved;
    How precious did that grace appear
    The hour I first believed.
    
    \n\n3. Through many dangers, toils and snares,
    I have already come;
    ’Tis grace hath brought me safe thus far,
    And grace will lead me home.
    
    \n\n4. The Lord has promised good to me,
    His Word my hope secures;
    He will my Shield and Portion be,
    As long as life endures.
    
            \n\n5. When we’ve been there ten thousand years,
    Bright shining as the sun,
    We’ve no less days to sing God’s praise
    Than when we’d first begun.
    
    \n\n6. Yea, when this flesh and heart shall fail,
    And mortal life shall cease,
    I shall possess, within the veil,
    A life of joy and peace.
    
    \n\n7. The earth shall soon dissolve like snow,
    The sun forbear to shine;
    But God, who called me here below,
    Will be forever mine.
    ", "Amazing Grace. ","John Newton, Wm. Walker ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Dearest Savior, Be Thou near me When the billows rage and roar, When alone and most forsaken, Help me then to love Thee more.
    
    \n\nCho-.\nBlessed Jesus, I will love Thee, I will love Thee more and more, \’Till I reach the Holy city Over on fair Canaan\’s shore.
    
    \n\n2. Dearest Savior, I will love Thee, And I\’ll closer cling to Thee, For I have not a friend or brother, Who can be so near to me.
     \n\n3. I have all for Thee Forsaken, All the pleasures of this life, And I\’m in the field of battle, Helping in the fearful strife.
    \n\n4. Yes, dear Jesus, I will love Thee, While I work and toil with Thee. Knowing well if I am faithful, In God\’s land at last I\’ll be.
    ", "Be Thou Near Me. ","D. Paul Ziegler, Geo. P. Root  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Who took my burdens all away? Jesus, Jesus. Who turned my darkness into day? Jesus, Jesus.
    \n\nCho-.\nJesus! Jesus! On-ly Jesus! Jesus! Jesus! On-ly Jesus!
    
    \n\n2. Who bore my grief up under the tree? Jesus, Jesus. When Sorrows come, Who cares for me? Jesus, Jesus.
    
    \n\n3. Who bears my sickness and my sin? Jesus, Jesus. Who gives me peace and joy within? Jesus, Jesus.
    
    \n\n4. Who pleads for me with tender love love? Jesus, Jesus. Who\’ll take me to His home above? Jesus, Jesus.
     \n\n5. Who\’s coming back to welcome me? Jesus, Jesus. Then in whose likeness shall I be? Jesus, Jesus.
    ", "Jesus! ","L. C. Hall  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. In the cross of Christ I glory,
    Tow’ring o’er the wrecks of time;
    All the light of sacred story
    Gathers round its head sublime.
    
    \n\n2. When the woes of life o’ertake me,
    Hopes deceive, and fears annoy,
    Never shall the cross forsake me,
    Lo! it glows with peace and joy.
    
    \n\n3. When the sun of bliss is beaming
    Light and love upon my way,
    From the cross the radiance streaming
    Adds more luster to the day.
    
    \n\n4. Bane and blessing, pain and pleasure,
    By the cross are sanctified;
    Peace is there that knows no measure,
    Joys that through all time abide.
    ", "In the Cross of Christ I Glory. ","John Bowring ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Would you be redeemed from ev’ry in-born sin,
    Have the Holy Spirit constantly within?
    Make the consecration, trust in God, and then,
    Let the Holy Ghost come in.
    
    \n\nRefrain:\nLet the Holy Ghost come in,
    Let the Holy Ghost come in,
    Make the consecration, trust in God, and then,
    Let the Holy Ghost come in.
    
    \n\n2. Would you have the Spirit in your heart to cheer?
    Would you be relieved from ev’ry doubt and fear?
    Make the consecration, trust in God, and then,
    Let the Holy Ghost come in.
    
    \n\n3. Do you want the “fire of God” to fill your soul,
    Burn up all the dross, and sanctify the whole?
    Make the consecration, trust in God, and then,
    Let the Holy Ghost come in.
    
    \n\n4. Do you want the “pow’r” to make you true and brave,
    So that you can rescue those that Christ would save?
    Make the consecration, trust in God, and then,
    Let the Holy Ghost come in.
    ", "Let the Holy Ghost Come In. ","R. F. Reynolds  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Softly and tenderly Jesus is calling,
    Calling for you and for me;
    See, on the portals He’s waiting and watching,
    Watching for you and for me.
    
            \n\nRefrain:\nCome home, come home,
    You who are weary, come home;
    Earnestly, tenderly, Jesus is calling,
    Calling, O sinner, come home!
    
    \n\n2. Why should we tarry when Jesus is pleading,
    Pleading for you and for me?
    Why should we linger and heed not His mercies,
    Mercies for you and for me?
    
    \n\n3. Time is now fleeting, the moments are passing,
    Passing from you and from me;
    Shadows are gathering, deathbeds are coming,
    Coming for you and for me.
    
    \n\n4. Oh, for the wonderful love He has promised,
    Promised for you and for me!
    Though we have sinned, He has mercy and pardon,
    Pardon for you and for me.
    ", "Softly and Tenderly. ","W. T. Thompson  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. My Jesus, I love Thee, I know Thou art mine;
    For Thee all the follies of sin I resign;
    My gracious Redeemer, my Savior art Thou;
    If ever I loved Thee, my Jesus, ’tis now.
    
    \n\n2. I love Thee because Thou hast first loved me,
    And purchased my pardon on Calvary’s tree;
    I love Thee for wearing the thorns on Thy brow;
    If ever I loved Thee, my Jesus, ’tis now.
    
    \n\n3. I’ll love Thee in life, I will love Thee in death,
    And praise Thee as long as Thou lendest me breath;
    And say when the death dew lies cold on my brow,
    If ever I loved Thee, my Jesus, ’tis now.
    
    \n\n4. In mansions of glory and endless delight,
    I’ll ever adore Thee in heaven so bright;
    I’ll sing with the glittering crown on my brow,
    If ever I loved Thee, my Jesus, ’tis now.
    ", "My Jesus, I Love Thee. ","A. J. Gordon  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. The great Physician now is near,
    The sympathizing Jesus;
    He speaks the drooping heart to cheer,
    Oh, hear the voice of Jesus.
    
    \n\nRefrain:\nSweetest note in seraph song,
    Sweetest name on mortal tongue;
    Sweetest carol ever sung,
    Jesus, blessed Jesus.
    
    \n\n2. Your many sins are all forgiv’n,
    Oh, hear the voice of Jesus;
    Go on your way in peace to heav’n,
    And wear a crown with Jesus.
    
    \n\n3. His name dispels my guilt and fear,
    No other name but Jesus;
    Oh, how my soul delights to hear
    The charming name of Jesus.
    
            \n\n4. The children, too, both great and small,
            Who love the name of Jesus,
            Many now accept the gracious call
            To work and live for Jesus.
    ", "The Great Physician. ","William Hunter  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I hear Thy welcome voice
    That calls me, Lord, to Thee,
    For cleansing in Thy precious blood
    That flowed on Calvary.
    
       \n\nRefrain\nI am coming, Lord!
    Coming now to Thee!
    Wash me, cleanse me in the blood
    That flowed on Calvary!
    
    \n\n2.  Tho\' coming weak and vile,
    Thou dost my strength assure;
    Thou dost my vileness fully cleanse,
    Till spotless all, and pure.
    
    \n\n3. \'Tis Jesus calls me on
    To perfect faith and love,
    To perfect hope and peace and trust,
    For earth and heav\'n above.
    
            \n\n4. All hail! atoning blood!
    All hail! redeeming grace!
    All hail! the gift of Christ our Lord,
    Our strength and righteousness.
    ", "I Am Coming Lord. ","L. Hartsough ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. In the resurrection morning,
    When the trump of God shall sound,
    We shall rise, Hallelujah! we shall rise!
    Then the saints will come rejoicing
    And no tears will e’er be found,
    We shall rise, Hallelujah! we shall rise.
    
    \n\nRefrain\nWe shall rise, Hallelujah! we shall rise!
    Amen! We shall rise! Hallelujah!
    In the resurrection morning,
    When death’s prison bars are broken,
    We shall rise, Hallelujah! We shall rise.
    
    \n\n2.  In the resurrection morning,
    What a meeting it will be,
    We shall rise, Hallelujah! we shall rise!
    When our fathers and our mothers,
    And our loved ones we shall see,
    We shall rise, Hallelujah! we shall rise!
    
    \n\n3. In the resurrection morning,
    Blessèd thought it is to me,
    We shall rise, Hallelujah! we shall rise!
    I shall see my blessèd Savior,
    Who so freely died for me,
    We shall rise, Hallelujah! we shall rise!
    
    \n\n4. In the resurrection morning,
    We shall meet Him in the air,
    We shall rise, Hallelujah! we shall rise!
    And be carried up to glory,
    To our home so bright and fair,
    We shall rise, Hallelujah! we shall rise!
    ", "Hallelujah! We Shall Rise. ","J. E. Thomas  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I am coming to Jesus for rest,
    Rest such as the purified know;
    My soul is athirst to be blest,
    To be washed and made whiter than snow.
    
            \n\nRefrain:\nI believe Jesus saves,
    And His blood washes whiter than snow;
    I believe Jesus saves,
    And His blood washes whiter than snow.
    
    \n\n2. In coming, my sin I deplore,
    My weakness and poverty show;
    I long to be saved evermore,
    To be washed and made whiter than snow.
    
    \n\n3. To Jesus I give up my all,
    Every treasure and idol I know;
    For His fullness of blessing I call,
    Till His blood washes whiter than snow.
    
    \n\n4. I am trusting in Jesus alone,
    Trusting now His salvation to know;
    And His blood doth so fully atone,
    I am washed and made whiter than snow.
    
           \n\n5. My heart is in raptures of love,
            Love, such as the ransomed ones know;
            I am strengthened with might from above,
            I am washed and made whiter than snow
    ", "I Believe Jesus Saves. ","Wm. McDonald  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. There’s a land that is fairer than day,
    And by faith we can see it afar;
    For the Father waits over the way
    To prepare us a dwelling place there.
    
            \n\nRefrain:\nIn the sweet by and by,
    We shall meet on that beautiful shore;
    In the sweet by and by,
    We shall meet on that beautiful shore.
    
    \n\n2. We shall sing on that beautiful shore
    The melodious songs of the blessed;
    And our spirits shall sorrow no more,
    Not a sigh for the blessing of rest.
    
    \n\n3. To our bountiful Father above,
    We will offer our tribute of praise
    For the glorious gift of His love
    And the blessings that hallow our days.
    ", "Sweet By and By. ","Sanford F. Bennett, 1868  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Oh, now I see the cleansing wave!
    The fountain deep and wide;
    Jesus, my Lord, mighty to save,
    Points to His wounded side.
    
            \n\nRefrain:\nThe cleansing stream I see, I see!
    I plunge, and, oh, it cleanseth me!
    Oh, praise the Lord, it cleanseth me!
    It cleanseth me, yes, cleanseth me.
    
    
    \n\n2. I rise to walk in Heav’n’s own light,
    Above the world and sin,
    With heart made pure and garments white,
    And Christ enthroned within.
    
    \n\n3. Amazing grace! ’tis Heav’n below
    To feel the blood applied,
    And Jesus, only Jesus know,
    My Jesus crucified.
    
           \n\n4. I see the new creation rise,
    I hear the speaking blood;
    It speaks, polluted nature dies,
    Sinks ’neath the cleansing flood.
    ", "The Cleansing Wave. ","P. Palmer, J. F. Knapp  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Sweet hour of prayer! sweet hour of prayer!
    That calls me from a world of care,
    And bids me at my Father’s throne
    Make all my wants and wishes known.
    In seasons of distress and grief,
    My soul has often found relief,
    And oft escaped the tempter’s snare,
    By thy return, sweet hour of prayer!
    
    \n\n2. Sweet hour of prayer! sweet hour of prayer!
    The joys I feel, the bliss I share,
    Of those whose anxious spirits burn
    With strong desires for thy return!
    With such I hasten to the place
    Where God my Savior shows His face,
    And gladly take my station there,
    And wait for thee, sweet hour of prayer!
    
    \n\n3. Sweet hour of prayer! sweet hour of prayer!
    Thy wings shall my petition bear
    To Him whose truth and faithfulness
    Engage the waiting soul to bless.
    And since He bids me seek His face,
    Believe His Word and trust His grace,
    I’ll cast on Him my every care,
    And wait for thee, sweet hour of prayer!
    
    \n\n4. Sweet hour of prayer! sweet hour of prayer!
    May I thy consolation share,
    Till, from Mount Pisgah’s lofty height,
    I view my home and take my flight.
    This robe of flesh I’ll drop, and rise
    To seize the everlasting prize,
    And shout, while passing through the air,
    “Farewell, farewell, sweet hour of prayer!”
    ", "Sweet Hour of Prayer. ","W. W. Watford, Wm. Bradbury  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Thou my everlasting portion,
    More than friend or life to me,
    All along my pilgrim journey,
    Savior, let me walk with Thee.
    
            \n\nRefrain:1\nClose to Thee, close to Thee,
    Close to Thee, close to Thee;
    All along my pilgrim journey,
    Savior, let me walk with Thee.
    
            \n\n2. Not for ease or worldly pleasure,
    Nor for fame my prayer shall be;
    Gladly will I toil and suffer,
    Only let me walk with Thee.
    
            \n\nRefrain:2\nClose to Thee, close to Thee,
    Close to Thee, close to Thee,
    Gladly will I toil and suffer,
    Only let me walk with Thee.
    
            \n\n3. Lead me through the vale of shadows,
    Bear me o’er life’s fitful sea;
    Then the gate of life eternal
    May I enter, Lord, with Thee.
    
            \n\nRefrain3:\nClose to Thee, close to Thee,
    Close to Thee, close to Thee,
    Then the gate of life eternal
    May I enter, Lord, with Thee.
    ", "Close to Thee. ","Fanny J. Crosby  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. I’ve wandered far away from God,
    Now I’m coming home;
    The paths of sin too long I’ve trod,
    Lord, I’m coming home.
    
            \n\nRefrain:\nComing home, coming home,
    Nevermore to roam;
    Open wide Thine arms of love,
    Lord, I’m coming home.
    
    \n\n2. I’ve wasted many precious years,
    Now I’m coming home;
    I now repent with bitter tears,
    Lord, I’m coming home.
    
    \n\n3. I’m tired of sin and straying, Lord,
    Now I’m coming home;
    I’ll trust Thy love, believe Thy word,
    Lord, I’m coming home.
    
    \n\n4. My soul is sick, my heart is sore,
    Now I’m coming home;
    My strength renew, my hope restore,
    Lord, I’m coming home.
    
    \n\n5. My only hope, my only plea,
    Now I’m coming home;
    That Jesus died, and died for me,
    Lord, I’m coming home.
    
    \n\n6. I need His cleansing blood I know,
    Now I’m coming home;
    Oh, wash me whiter than the snow,
    Lord, I’m coming home.
    ", "Lord, I'm Coming Home. ","Wm. J. Kirkpatrick  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. When Jesus laid His crown aside,
    He came to save me;
    When on the cross He bled and died,
    He came to save me.
    
    \n\nRefrain\nI’m so glad, I’m so glad,
    I’m so glad that Jesus came, and grace is free,
    I’m so glad, I’m so glad,
    I’m so glad that Jesus came, He came to save me.
    
    \n\n2. In my poor heart He deigns to dwell,
    He came to save me;
    O praise His name, I know it well,
    He came to save me.
    
    \n\n3. With gentle hand He leads me still,
    He came to save me;
    And trusting Him I fear no ill,
    He came to save me.
    
    \n\n4. To Him my faith with rapture clings,
    He came to save me,
    To Him my heart looks up and sings,
    He came to save me.
    ", "He Came to Save Me. ","Wm. J. Kirkpatrick  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Jesus, my Lord, to Thee I cry;
    Unless Thou help me I must die:
    Oh, bring Thy free salvation nigh,
    And take me as I am.
    
    \n\nCho:-\nAnd take me as I am,
    And take me as I am;
    My only plea--Christ died for me!
    Oh, take me as I am.
    
    \n\n2.  Helpless I am, and full of guilt;
    But yet for me Thy blood was spilt,
    And Thou canst make me what Thou wilt,
    And take me as I am.
    
    \n\n3. No preparation can I make,
    My best resolves I only break,
    Yet save me for Thine own name\'s sake,
    And take me as I am.
    
            \n\n4. I thirst, I long to know Thy love,
    Thy full salvation I would prove;
    But since to Thee I cannot move,
    Oh, take me as I am.
    
    \n\n5. If Thou hast work for me to do,
    Inspire my will, my heart renew,
    And work both in and by me, too,
    Oh, take me as I am.
    
    \n\n6. And when at last the work is done,
    The battle fought, the vict’ry won,
    Still, still my cry shall be alone,
    Lord, take me as I am.
    ", "Take Me As I Am. ","J. H. Stockton  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("  1. Pass me not, O gentle Savior,
    Hear my humble cry;
    While on others Thou art calling,
    Do not pass me by.
    
            \n\nRefrain:\nSavior, Savior,
    Hear my humble cry,
    While on others Thou art calling,
    Do not pass me by.
    
    \n\n2. Let me at Thy throne of mercy
    Find a sweet relief;
    Kneeling there in deep contrition,
    Help my unbelief.
    
    \n\n3. Trusting only in Thy merit,
    Would I seek Thy face;
    Heal my wounded, broken spirit,
    Save me by Thy grace.
    
    \n\n4. Thou the spring of all my comfort,
    More than life to me,
    Whom have I on earth beside Thee,
    Whom in Heav’n but Thee.
    ", "Pass Me Not. ","Franny J. Crosby  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Hover o’er me, Holy Spirit,
    Bathe my trembling heart and brow;
    Fill me with Thy hallowed presence,
    Come, oh, come and fill me now.
    
            \n\nRefrain:\nFill me now, fill me now,
    Holy Spirit, fill me now;
    Fill me with Thy hallowed presence,
    Come, oh, come, and fill me now.
    
    \n\n2. Thou canst fill me, gracious Spirit,
    Though I cannot tell Thee how;
    But I need Thee, greatly need Thee,
    Come, oh, come and fill me now.
    
    \n\n3. I am weakness, full of weakness,
    At Thy sacred feet I bow;
    Blest, divine, eternal Spirit,
    Fill with pow’r, and fill me now.
    
    \n\n4. Cleanse and comfort, bless and save me,
    Bathe, oh, bathe my heart and brow;
    Thou art comforting and saving,
            Thou art sweetly filling now.
    ", "Fill Me Now. ","E. R. Strokes  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. What can wash away my sin?
    Nothing but the blood of Jesus;
    What can make me whole again?
    Nothing but the blood of Jesus.
    
            \n\nRefrain:\nOh! precious is the flow
    That makes me white as snow;
    No other fount I know,
    Nothing but the blood of Jesus.
    
    \n\n2. For my pardon, this I see,
    Nothing but the blood of Jesus;
    For my cleansing this my plea,
    Nothing but the blood of Jesus.
    
    \n\n3. Nothing can for sin atone,
    Nothing but the blood of Jesus;
    Naught of good that I have done,
    Nothing but the blood of Jesus.
    
    \n\n4. This is all my hope and peace,
    Nothing but the blood of Jesus;
    This is all my righteousness,
    Nothing but the blood of Jesus.
    
    \n\n5. Now by this I’ll overcome
    Nothing but the blood of Jesus;
    Now by this I’ll reach my home
    Nothing but the blood of Jesus.
    ", "Nothing But the Blood. ","Robert Lowry  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("      1. Savior, more than life to me,
     I am clinging, clinging, close to Thee;
    Let Thy precious blood applied,
    Keep me ever, ever near Thy side.
    
            \n\nRefrain:\nEvery day, every hour,
    Let me feel Thy cleansing pow’r;
    May Thy tender love to me
    Bind me closer, closer, Lord to Thee.
    
    \n\n2. Through this changing world below,
    Lead me gently, gently as I go;
    Trusting Thee, I cannot stray,
    I can never, never lose my way.
    
    \n\n3. Let me love Thee more and more,
    Till this fleeting, fleeting life is o’er;
    Till my soul is lost in love,
    In a brighter, brighter world above.
    ", "Savior, More Than Life to Me. ","Franny J. Crosby  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("       1. Work, for the night is coming,
    Work thro\' the morning hours;
    Work while the dew is sparkling,
    Work \'mid springing flow\'rs.
    Work when the day grows brighter,
    Under the glowing sun;
    Work, for the night is coming,
    When man\'s work is done.
    
    \n\n2.  Work for the night is coming,
    Work thro\' the sunny noon;
    Fill brightest hours with labor
    Rest comes sure and soon.
    Give every flying minute
    Something to keep in store;
    Work, for the night is coming,
    When man works no more.
    
    \n\n3. Work for the night is coming,
    Under the sunset skies:
    While their bright tints are glowing,
    Work, for daylight flies.
    Work till the last beam fadeth,
    Fadeth to shine no more;
    Work, while the night is dark\'ning,
    When man\'s work is o\'er.
    ", "Work for the Night is Coming. ","Lowell Mason  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Jesus, Savior, pilot me,
    Over life’s tempestuous sea;
    Unknown waves before me roll,
    Hiding rock and treach’rous shoal;
    Chart and compass came from Thee:
    Jesus, Savior, pilot me.
    
    \n\n2. As a mother stills her child,
    Thou canst hush the ocean wild;
    Boist’rous waves obey Thy will
    When Thou say’st to them, “Be still!”
    Wondrous Sov’reign of the sea,
    Jesus, Savior, pilot me.
    
    \n\n3. When at last I near the shore,
    And the fearful breakers roar
    ’Twixt me and the peaceful rest,
    Then, while leaning on Thy breast,
    May I hear Thee say to me,
    “Fear not, I will pilot thee.”
    ", "Jesus, Savior, Pilot Me. ","Edward Hopper  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Savior, lead me lest I stray,
    Gently lead me all the way;
    I am safe when by Thy side,
    I would in Thy love abide.
    
            \n\nRefrain:\nLead me, lead me,
    Savior, lead me lest I stray;
    Gently down the stream of time,
    Lead me, Savior, all the way.
    
            \n\n2. Thou the refuge of my soul
    When life’s stormy billows roll;
    I am safe when Thou art nigh,
    All my hopes on Thee rely.
    
            \n\n3. Savior, lead me then at last,
    When the storm of life is past,
    To the land of endless day,
    Where all tears are wiped away.
    ", "Lead Me Savior. ","Frank .M Davis  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("         1. Speak to my soul, dear Jesus,
    Speak now in tend’rest tone;
    Whisper in loving-kindness,
    “Thou art not left alone.”
    Open my heart to hear Thee,
    Quickly to hear Thy voice,
    Fill Thou my soul with praises,
    Let me in Thee rejoice.
    
            \n\nRefrain:\nSpeak Thou in softest whispers,
    Whispers of love to me,
    “Thou shalt be always conqu’ror,
    Thou shalt be always free.”
    Speak Thou to me each day, Lord,
    Always in tend’rest tone,
    Let me now hear Thy whisper,
    “Thou art not left alone.”
    
    \n\n2. Speak to Thy children ever,
    Lead in the holy way;
    Fill them with joy and gladness,
    Teach them to watch and pray.
    May they in consecration
    Yield their whole lives to Thee,
    Hasten Thy coming kingdom,
    Till our dear Lord we see.
    
    \n\n3. Speak now as in the old time
    Thou didst reveal Thy will;
    Let me know all my duty,
    Let me Thy law fulfill.
    Lead me to glorify Thee,
    Help me to show Thy praise,
    Gladly to do Thy bidding,
    Honor Thee all my days.
    ", "Speak to My Soul. ","L. L. Pickett  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. ’Tis so sweet to trust in Jesus,
    Just to take Him at His Word;
    Just to rest upon His promise,
    And to know, “Thus saith the Lord!”
    
            \n\nRefrain:\nJesus, Jesus, how I trust Him!
    How I’ve proved Him o’er and o’er;
    Jesus, Jesus, precious Jesus!
    Oh, for grace to trust Him more!
    
    \n\n2. Oh, how sweet to trust in Jesus,
    Just to trust His cleansing blood;
    And in simple faith to plunge me
    ’Neath the healing, cleansing flood!
    
    \n\n3. Yes, ’tis sweet to trust in Jesus,
    Just from sin and self to cease;
    Just from Jesus simply taking
    Life and rest, and joy and peace.
    
    \n\n4. I’m so glad I learned to trust Thee,
    Precious Jesus, Savior, Friend;
    And I know that Thou art with me,
    Wilt be with me to the end.
    ", "'Tis so Sweet to Trust In Jesus. ","Wm. J. Kirkpatrick  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Come, ye that love the Lord,
    and let your joys be known;
    join in a song with sweet accord,
    while ye surround the throne.
    
            \n\n2.  Let those refuse to sing
    who never knew our God;
    but children of the heav\'nly King
    may speak their joys abroad.
    
          \n\n3. There we shall see his face,
    And never, never sin;
    There, from the rivers of his grace,
    Drink endless pleasures in:
    
            \n\n4. Then let our songs abound,
    and ev\'ry tear be dry;
    we\'re marching through Emmanuel\'s ground
    to fairer worlds on high.
    ", "Come, Ye that Love the Lord. ","I. Watts  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. More about Jesus would I know,
    More of His grace to others show;
    More of His saving fullness see,
    More of His love who died for me.
    
            \n\nRefrain:\nMore, more about Jesus,
    More, more about Jesus;
    More of His saving fullness see,
    More of His love who died for me.
    
    \n\n2. More about Jesus let me learn,
    More of His holy will discern;
    Spirit of God, my teacher be,
    Showing the things of Christ to me.
    
    \n\n3. More about Jesus, in His Word,
    Holding communion with my Lord;
    Hearing His voice in every line,
    Making each faithful saying mine.
    
    \n\n4. More about Jesus on His throne,
    Riches in glory all His own;
    More of His kingdom’s sure increase;
    More of His coming, Prince of Peace.
    ", "More About Jesus. ","E. E. Hewitt  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. “Almost persuaded” now to believe;
    “Almost persuaded” Christ to receive;
    Seems now some soul to say,
    “Go, Spirit, go Thy way,
    Some more convenient day
    On Thee I’ll call.”
    
    \n\n2. “Almost persuaded,” come, come today;
    “Almost persuaded,” turn not away;
    Jesus invites you here,
    Angels are ling’ring near,
    Prayers rise from hearts so dear;
    O wand’rer, come!
    
    \n\n3. “Almost persuaded,” harvest is past!
    “Almost persuaded,” doom comes at last!
    “Almost” cannot avail;
    “Almost” is but to fail!
    Sad, sad, that bitter wail
    “Almost,” but lost!
    ", "Almost Persuaded. ","P. P. Bliss  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. When He cometh, when He cometh,
    To make up His jewels,
    All His jewels, precious jewels,
    His loved and His own.
    
            \n\nRefrain:\nLike the stars of the morning,
    His bright crown adorning,
    They shall shine in their beauty,
    Bright gems for His crown.
    
    \n\n2. He will gather, He will gather
    The gems for His kingdom,
    All the pure ones, all the bright ones,
    His loved and His own.
    
    \n\n3. Little children, little children,
    Who love their Redeemer,
    Are the jewels, precious jewels,
    His loved and His own.
    ", "Jewels. ","W. O. Cushing  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. While Jesus whispers to you,
    Come, sinner, come!
    While we are praying for you,
    Come, sinner come!
    Now is the time to own Him,
    Come, sinner, come!
    Now is the time to know Him,
    Come, sinner come!
    
    \n\n2.  Are you too, heavy laden?
    Come, sinner, come!
    Jesus will bear your burden,
    Come, sinner come!
    Jesus will not deceive you,
    Come, sinner, come!
    Jesus will now receive you,
    Come, sinner come!
    
    \n\n3. Oh, hear His tender pleading,
    Come, sinner, come!
    Come and receive the blessing,
    Come, sinner come!
    While Jesus whispers to you,
    Come, sinner, come!
    While we are praying for you,
    Come, sinner come!
    ", "Come, Sinner, Come. ","Will E. Witter  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. There is a land of pure delight,
    where saints immortal reign;
    infinite day excludes the night,
    and pleasures banish pain.
            \n\n2. There everlasting spring abides,
    and never-withering flowers;
    death, like a narrow sea, divides
    that heavenly land from ours.
            \n\n3. Sweet fields beyond the swelling flood
    stand dressed in living green;
    so to the Jews old Canaan stood,
    while Jordan rolled between.
    
            \n\n4. Could we but climb where Moses stood,
    and view the landscape o\'er,
    not Jordan\'s stream, nor death\'s cold flood,
    should fright us from the shore!
    ", "I'll Be There. ","Issac Watts, Wm J. Kirkpatrick  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Abide with me: fast falls the eventide;
    the darkness deepens; Lord, with me abide.
    When other helpers fail and comforts flee,
    Help of the helpless, O abide with me.
    
            \n\n2.  Swift to its close ebbs out life\'s little day;
    earth\'s joys grow dim, its glories pass away.
    Change and decay in all around I see.
    O Lord who changes not, abide with me.
    
            \n\n3. I need thy presence every passing hour.
    What but your grace can foil the tempter\'s power?
    Who like thyself my guide and strength can be?
    Through cloud and sunshine, O abide with me.
    
            \n\n4. Hold thou thy cross before my closing eyes.
    Shine through the gloom and point me to the skies.
    Heaven\'s morning breaks and earth\'s vain shadows flee;
    in life, in death, O Lord, abide with me.
    ", "Abide With Me. ","H. F. Lyte, W.H Monk  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. I am coming to the cross;
    I am poor and weak and blind;
    I am counting all but dross;
    I shall full salvation find.
    
            \n\nRefrain:\nI am trusting, Lord, in Thee,
    Blessed Lamb of Calvary;
    Humbly at Thy cross I bow,
    Save me, Jesus, save me now.
    
    \n\n2. Long my heart has sighed to be
    Wholly purified within;
    Jesus sweetly speaks to me:
    “I will cleanse you from all sin.”
    
    \n\n3. Here I give my all to Thee:
    Friends and time and earthly store;
    Soul and body Thine to be,
    Wholly Thine forevermore.
    
    \n\n4. In the promises I trust;
    Now I feel the blood applied;
    I am prostrate in the dust;
    I with Christ am crucified.
    
    \n\n5. Jesus comes! He fills my soul!
    Perfected in Him I am;
    I am every whit made whole:
    Glory, glory to the Lamb!
    ", "I am Trusting Lord, in Thee. ","William Mcdonald  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. How firm a foundation, ye saints of the Lord,
    is laid for your faith in His excellent Word!
    What more can He say than to you He hath said,
    who unto the Savior for refuge have fled?
    
            \n\n2.  Fear not, I am with thee; O be not dismayed,
    for I am thy God and will still give thee aid.
    I\'ll strengthen thee, help thee, and cause thee to stand,
    upheld by My righteous, omnipotent hand.
    
            \n\n3. When through the deep waters I call thee to go,
    the rivers of sorrow shall not overflow;
    for I will be with thee, thy troubles to bless,
    and sanctify to thee thy deepest distress.
    
            \n\n4. When through fiery trials thy pathway shall lie,
    My grace, all-sufficient, shall be thy supply.
    The flame shall not hurt thee; I only design
    thy dross to consume, and thy gold to refine.
    
            \n\n5. The soul that on Jesus hath leaned for repose
    I will not, I will not desert to his foes;
    that soul, though all hell should endeavor to shake,
    I\'ll never, no never, no never forsake!
    ", "How Firm a Foundation. ","George Keith  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Just as I am, without one plea,
    But that Thy blood was shed for me,
    And that Thou bid’st me come to Thee,
    O Lamb of God, I come! I come!
    
    \n\n2. Just as I am, and waiting not
    To rid my soul of one dark blot;
    To Thee whose blood can cleanse each spot,
    O Lamb of God, I come, I come!
    
    \n\n3. Just as I am, though tossed about
    With many a conflict, many a doubt;
    Fightings within, and fears without,
    O Lamb of God, I come, I come!
    
    \n\n4. Just as I am, poor, wretched, blind;
    Sight, riches, healing of the mind;
    Yes, all I need, in Thee to find,
    O Lamb of God, I come, I come!
    
    \n\n5. Just as I am, Thou wilt receive,
    Wilt welcome, pardon, cleanse, relieve;
    Because Thy promise I believe,
    O Lamb of God, I come, I come!
    
    \n\n6. Just as I am, Thy love unknown
    Has broken every barrier down;
    Now, to be Thine, yea, Thine alone,
    O Lamb of God, I come, I come!
    ", "Just As I Am. ","Charlotte Elliot, William B. Bradbury  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I hear the Savior say,
    Thy strength indeed is small,
    Child of weakness, watch and pray,
    Find in Me thine all in all.
    
            \n\nRefrain:\nJesus paid it all,
    All to Him I owe;
    Sin had left a crimson stain,
    He washed it white as snow.
    
            \n\n2.  Lord, now indeed I find
    Thy pow\'r and Thine alone,
    Can change the leper\'s spots
    And melt the heart of stone.
    
            \n\n3. For nothing good have I
    Where-by Thy grace to claim;
    I\'ll wash my garments white
    In the blood of Calv\'ry\'s Lamb.
    
            \n\n4. And when, before the throne,
    I stand in Him complete,
    Jesus died my soul to save,
    My lips shall still repeat.
    ", "Jesus Paid it All. ","H. M. Hall, John T. Grape  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Jesus is tenderly calling thee home,
    Calling today, calling today;
    Why from the sunshine of love wilt thou roam
    Farther and farther away?
    
    \n\nCho-:\nCalling today,
    Calling today;
    Jesus is calling,
    Is tenderly calling today.
    
    \n\n2.  Jesus is calling the weary to rest,
    Calling today, calling today;
    Bring Him thy burden and thou shalt be blest;
    He will not turn thee away.
    
    \n\n3. Jesus is waiting; O come to Him now,
    Waiting today, waiting today;
    Come with thy sins; at His feet lowly bow;
    Come and no longer delay.
    
    \n\n4. Jesus is pleading; O list to His voice,
    Hear Him today, hear Him today;
    They who believe on His name shall rejoice;
    Quickly arise and away.
    ", "Jesus is Calling. ","Fanny J. Crosby  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. We have heard the joyful sound:
      Jesus saves! Jesus saves!
    Spread the tidings all around:
      Jesus saves! Jesus saves!
    Bear the news to every land,
    Climb the steeps and cross the waves;
    Onward!—’tis our Lord’s command;
      Jesus saves! Jesus saves!
    
      \n\n2. Waft it on the rolling tide,
      Jesus saves, Jesus saves;
    Tell to sinners far and wide,
      Jesus saves, Jesus saves;
    Sing, ye islands of the sea,
    Echo back, ye ocean caves;
    Earth shall keep her jubilee,
      Jesus saves, Jesus saves.
    
      \n\n3. Sing above the battle’s strife,
      Jesus saves, Jesus saves;
    By His death and endless life,
      Jesus saves, Jesus saves;
    Sing it softly thru the gloom,
    When the heart for mercy craves,
    Sing in triumph o’er the tomb,
      Jesus saves, Jesus saves.
    
      \n\n4. Give the winds a mighty voice,
      Jesus saves, Jesus saves;
    Let the nations now rejoice.
      Jesus saves, Jesus saves;
    Shout salvation full and free,
    Highest hills and deepest caves,
    This our song of victory,
      Jesus saves, Jesus saves.
    ", "Jesus Saves. ","Priscilla J. Owens  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I need Thee every hour, most gracious Lord;
    No tender voice like Thine can peace afford.
    
    \n\nRefrain:\nI need Thee, oh, I need Thee;
    Every hour I need Thee;
    Oh, bless me now, my Savior,
    I come to Thee.
    
    \n\n2. I need Thee every hour, stay Thou nearby;
    Temptations lose their pow’r when Thou art nigh.
    
    \n\n3. I need Thee every hour, in joy or pain;
    Come quickly and abide, or life is vain.
    
    \n\n4. I need Thee every hour, most Holy One;
    Oh, make me Thine indeed, Thou blessed Son.
    
    \n\n5. I need Thee every hour; teach me Thy will;
    And Thy rich promises in me fulfill.
    ", "I Need Thee Every Hour. ","Mary S. Hawks  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. We praise thee, O God, for the Son of thy love,
    For Jesus who died and is now gone above.
    
            \n\nRefrain:\nHallelujah, thine the glory!
    Hallelujah, Amen!
    Hallelujah, thine the glory!
    Revive us again.
    
    \n\n2.  We praise thee, O God, for thy Spirit of light,
    Who has shown us our Savior and scattered our night.
    
    \n\n3. All glory and praise to the Lamb that was slain,
    Who has borne all our sins and has cleansed ev\'ry stain.
    
    \n\n4. All glory and praise
    To the God of all grace,Who hast brought us,
            and sought us, And guided our ways.
    
    \n\n5. Revive us again - fill each heart with thy love;
    May each soul be rekindled with fire from above.
    
    ", "Revive Us Again. ","W. P. Mackay  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Nearer, still nearer, close to Thy heart,
    Draw me, my Savior—so precious Thou art!
    Fold me, oh, fold me close to Thy breast;
    Shelter me safe in that “haven of rest”;
    Shelter me safe in that “haven of rest.”
    
    \n\n2. Nearer, still nearer, nothing I bring,
    Naught as an off’ring to Jesus, my King;
    Only my sinful, now contrite heart,
    Grant me the cleansing Thy blood doth impart;
    Grant me the cleansing Thy blood doth impart.
    
    \n\n3. Nearer, still nearer, Lord, to be Thine!
    Sin, with its follies, I gladly resign,
    All of its pleasures, pomp and its pride,
    Give me but Jesus, my Lord crucified;
    Give me but Jesus, my Lord crucified.
    
    \n\n4. Nearer, still nearer, while life shall last,
    Till safe in glory my anchor is cast;
    Through endless ages ever to be
    Nearer, my Savior, still nearer to Thee;
    Nearer, my Savior, still nearer to Thee!
    ", "Nearer, Still Nearer. ","C. H. Morris  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. I care not today what the morrow may bring,
    If shadow or sunshine or rain,
    The Lord I know ruleth o’er everything,
    And all of my worries are vain.
    
            \n\nRefrain:\nLiving by faith in Jesus above,
    Trusting, confiding in His great love;
    From all harm safe in His sheltering arm,
    I’m living by faith and feel no alarm.
    
            \n\n2.  Though tempests may blow and the storm clouds arise,
    Obscuring the brightness of life,
    I’m never alarmed at the overcast skies
    The Master looks on at the strife.
    
            \n\n3. I know that He safely will carry me through,
    No matter what evils betide;
    Why should I then care though the tempest may blow,
    If Jesus walks close to my side.
    
            \n\n4. Our Lord will return to this earth some sweet day,
    Our troubles will then all be o’er;
    The Master so gently will lead us away,
    Beyond that blest heavenly shore.
    ", "Living by Faith. ","James Wells  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Rescue the perishing, care for the dying,
    Snatch them in pity from sin and the grave;
    Weep o’er the erring one, lift up the fallen,
    Tell them of Jesus, the mighty to save.
    
            \n\nRefrain:\nRescue the perishing, care for the dying,
    Jesus is merciful, Jesus will save.
    
    \n\n2. Though they are slighting Him, still He is waiting,
    Waiting the penitent child to receive;
    Plead with them earnestly, plead with them gently;
    He will forgive if they only believe.
    
    \n\n3. Down in the human heart, crushed by the tempter,
    Feelings lie buried that grace can restore;
    Touched by a loving heart, wakened by kindness,
    Chords that were broken will vibrate once more.
    
    \n\n4. Rescue the perishing, duty demands it;
    Strength for thy labor the Lord will provide;
    Back to the narrow way patiently win them;
    Tell the poor wand’rer a Savior has died.
    ", "Rescue the Perishing. ","Franny J. Crosby  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. A wonderful Savior is Jesus my Lord,
    A wonderful Savior to me;
    He hideth my soul in the cleft of the rock,
    Where rivers of pleasure I see.
    
            \n\nRefrain:\nHe hideth my soul in the cleft of the rock,
    That shadows a dry, thirsty land;
    He hideth my life in the depths of His love,
    And covers me there with His hand,
    And covers me there with His hand.
    
    \n\n2. A wonderful Savior is Jesus my Lord,
    He taketh my burden away,
    He holdeth me up and I shall not be moved,
    He giveth me strength as my day.
    
    \n\n3. With numberless blessings each moment He crowns,
    And filled with His fullness divine,
    I sing in my rapture, oh, glory to God!
    For such a Redeemer as mine.
    
    \n\n4. When clothed with His brightness transported I rise
    To meet Him in clouds of the sky,
    His perfect salvation, His wonderful love,
    I’ll shout with the millions on high.
    ", "He Hideth My Soul. ","Franny J. Crosby  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. O safe to the Rock that is higher than I,
    My soul in its conflicts and sorrows would fly;
    Alone I would perish, undone would I be;
    Thou blest “Rock of Ages,” I’m hiding in Thee.
    
            \n\nRefrain:\nHiding in Thee, hiding in Thee,
    Thou blest “Rock of Ages,” I’m hiding in Thee.
    
    \n\n2. In the calm of the noontide, in sorrow’s lone hour,
    In times when temptation casts o’er me its pow’r;
    In the tempests of life, on its wide, heaving sea,
    Thou blest “Rock of Ages,” I’m hiding in Thee.
    
    \n\n3. How oft in the conflict, when pressed by the foe,
    I have fled to my Refuge and breathed out my woe;
    How often, when trials like sea billows roll,
    Have I hidden in Thee, O Thou Rock of my soul.
    ", "Hiding in Thee. ","William O. Cushing  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. All to Jesus I surrender,
    All to Him I freely give;
    I will ever love and trust Him,
    In His presence daily live.
    
            \n\nRefrain:\nI surrender all,
    I surrender all;
    All to Thee, my blessed Savior,
    I surrender all.
    
    \n\n2. All to Jesus I surrender,
    Humbly at His feet I bow;
    Worldly pleasures all forsaken,
    Take me, Jesus, take me now.
    
    \n\n3. All to Jesus I surrender,
    Make me, Savior, wholly Thine;
    Let me feel the Holy Spirit,
    Truly know that Thou art mine.
    
    \n\n4. All to Jesus I surrender,
    Lord, I give myself to Thee;
    Fill me with Thy love and power,
    Let Thy blessing fall on me.
    
    \n\n5. All to Jesus I surrender,
    Now I feel the sacred flame;
    Oh, the joy of full salvation!
    Glory, glory, to His Name!
    ", "I Surrender All. ","J.W Van De Venter  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Come to Jesus,
    Come to Jesus,
    Come to Jesus just now, just now.
    Come to Jesus,
    Come to Jesus just now!
    
            \n\n2.  He will save you,
            He will save you,
    He will save you just now, just now.
    He will save you,
    He will save you just now!
    
            \n\n3. He is able.
            \n\n4. He is willing.
            \n\n5. Call upon Him.
            \n\n6. He will hear you.
            \n\n7. He\'ll forgive you.
            \n\n8. He will cleanse you.
            \n\n9. Jesus love you.
            \n\n10. Only trust Him
    ", "Come to Jesus. ","Unknown, Arr. E. O. E  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Sing the wondrous love of Jesus,
    sing his mercy and his grace;
    in the mansions bright and blessed
    he\'ll prepare for us a place.
    
            \n\nRefrain:\nWhen we all get to heaven,
    what a day of rejoicing that will be!
    When we all see Jesus
    we\'ll sing and shout the victory.
    
            \n\n2.  While we walk the pilgrim pathway
    clouds will overspread the sky,
    but when traveling days are over,
    not a shadow, not a sigh.
    
            \n\n3. Let us then be true and faithful,
    trusting, serving every day;
    just one glimpse of Him in glory
    will the toils of life repay.
    
           \n\n4. Onward to the prize before us!
    Soon His beauty we’ll behold;
    Soon the pearly gates will open;
    We shall tread the streets of gold.
    ", "When We All Get to Heaven. ","E. E. Hewitt  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Sitting at the feet of Jesus,
    Oh, what words I hear Him say!
    Happy place! so near, so precious!
    May it find me there each day;
    Sitting at the feet of Jesus,
    I would look upon the past;
    For His love has been so gracious,
    It has won my heart at last.
    
            \n\n2.  Sitting at the feet of Jesus,
    Where can mortal be more blest?
    There I lay my sins and sorrows,
    And, when weary, find sweet rest;
    Sitting at the feet of Jesus,
    There I love to weep and pray;
    While I from His fullness gather
    Grace and comfort every day.
    
            \n\n3. Bless me, O my Savior, bless me,
    As I sit low at Thy feet;
    Oh, look down in love upon me,
    Let me see Thy face so sweet;
    Give me, Lord, the mind of Jesus,
    Keep me holy as He is;
    May I prove I’ve been with Jesus,
    Who is all my righteousness.
    ", "Sitting at the Feet of Jesus. ","J. Lincoln Hall  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. My life, my love, I give to Thee,
    Thou Lamb of God who died for me;
    Oh, may I ever faithful be,
    My Savior and my God!
    
    \n\nRefrain:\nI’ll live for Him who died for me,
    How happy then my life shall be!
    I’ll live for Him who died for me,
    My Savior and my God!
    \n\n2.  I now believe Thou dost receive,
    For Thou hast died that I might live;
    And now henceforth I’ll trust to Thee,
    My Savior and my God!
    
    \n\n3. Oh, Thou who died on Calvary,
    To save my soul and make me free;
    I’ll consecrate my life to Thee,
    My Savior and my God!
    ", "I'll Live For Him. ","R. E. Hudson, C. R. Dunbar  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Gone from my heart the world and all its charms;
    Now through the blood I’m saved from all alarms;
    Down at the cross my heart is bending low;
    The precious blood of Jesus cleanses white as snow.
    
            \n\nRefrain:\nI love Him, I love Him,
    Because He first loved me,
    And purchased my salvation on Calv’ry’s tree.
    
    \n\n2. Once I was lost, and ’way down deep in sin;
    Once was a slave to passions fierce within;
    Once was afraid to meet an angry God;
    But now I’m cleansed from ev’ry stain through Jesus’ blood.
    
    \n\n3. Once I was bound, but now I am set free;
    Once I was blind, but now the light I see;
    Once I was dead, but now in Christ I live,
    To tell the world around the peace that He doth give.
    ", "I Love Him. ","Stephan C. Foster  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. All hail the pow’r of Jesus’ Name!
    Let angels prostrate fall;
    Bring forth the royal diadem,
    And crown Him Lord of all!
    
    \n\n2. Ye chosen seed of Israel’s race,
    Ye ransomed from the fall,
    Hail Him Who saves you by His grace,
    And crown Him Lord of all!
    
    \n\n3. Let every kindred, every tribe,
    On this terrestrial ball,
    To Him all majesty ascribe,
    And crown Him Lord of all!
    
    \n\n4. Oh, that with yonder sacred throng
    We at His feet may fall!
    We’ll join the everlasting song,
    And crown Him Lord of all!
    ", "All Hail the Power of Jesus' Name! ","E. Parronet, Oliver Holden  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Time is filled with swift transition,
    Naught of earth unmoved can stand,
    Build your hopes on things eternal,
    Hold to God’s unchanging hand.
    
            \n\nRefrain:\nHold to God’s unchanging hand,
    Hold to God’s unchanging hand;
    Build your hopes on things eternal,
    Hold to God’s unchanging hand.
    
    \n\n2. Trust in Him who will not leave you,
    Whatsoever years may bring,
    If by earthly friends forsaken
    Still more closely to Him cling.
    
    \n\n3. Covet not this world’s vain riches
    That so rapidly decay,
    Seek to gain the heav’nly treasures,
    They will never pass away.
    
    \n\n4. When your journey is completed,
    If to God you have been true,
    Fair and bright the home in glory
    Your enraptured soul will view.
    ", "Hold to God's Unchanging Hand. ","Jennie Wilson  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. My happy soul rejoices,
    The sky is bright above;
    I’ll join the heav’nly voices,
    And sing redeeming love.
    
            \n\nRefrain:\nFor there’s pow’r in Jesus’s blood,
    Pow’r in Jesus’ blood;
    There’s pow’r in Jesus’ blood
    To wash me white as snow.
    
            \n\n2.  I heard the blessed story
    O Him who died to save;
    The love of Christ swept o’er me,
    My all to him I gave.
    
            \n\n3. His gracious words of pardon
    Were music to my heart;
    He took away my burden,
    And bade my fears depart.
    
            \n\n4. I plunge beneath this fountain,
    That cleanseth white as snow;
    It pours from Calv’ry’s mountain,
    With blessing in its flow.
    
            \n\n5. Oh, crown him King forever!
    My Saviour and my friend;
    By Zion’s crystal river
    His praise shall never end.
    ", "There's Power In Jesus' Blood. ","Hope Tryaway  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. If you from sin are longing to be free,
    Look to the Lamb of God;
    He, to redeem you, died on Calvary,
    Look to the Lamb of God.
    
            \n\nCho:-\nLook to the Lamb of God,
    Look to the Lamb of God,
    For He alone is able to save you--
    Look to the Lamb of God.
    
            \n\n2.  When Satan tempts and doubts and fears assail,
    Look to the Lamb of God;
    You in His strength shall over all prevail,
    Look to the Lamb of God.
    
            \n\n3. Are you a-weary, does the way seem long?
    Look to the Lamb of God;
    His love will cheer and fill your heart with song,
    Look to the Lamb of God.
    
            \n\n4. Fear not when shadows on your pathway fall,
    Look to the Lamb of God;
    In joy or sorrow Christ is all in all,
    Look to the Lamb of God.
    ", "Look to the Lamb of God. ","H. G. Jackson, James M. Black  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Stand up, stand up for Jesus! ye soldiers of the cross;
    Lift high His royal banner, it must not suffer loss:
    From vict’ry unto vict’ry, His army shall He lead,
    Till every foe is vanquished, and Christ is Lord indeed.
    
            \n\n2. Stand up, stand up for Jesus! The trumpet call obey:
    Forth to the mighty conflict, in this His glorious day;
    Ye that are men now serve Him against unnumbered foes;
    Let courage rise with danger, and strength to strength oppose.
    
            \n\n3. Stand up, stand up for Jesus! Stand in His strength alone,
    The arm of flesh will fail you, ye dare not trust your own;
    Put on the gospel armor, and watching unto prayer,
    Where calls the voice of duty, be never wanting there.
    
            \n\n4. Stand up, stand up for Jesus! the strife will not be long;
    This day the noise of battle, the next the victor’s song;
    To him that overcome-th a crown of life shall be;
    He with the King of glory shall reign eternally.
    ", "Stand Up, Stand Up for Jesus. ","Geo. Duffield, Geo. J. Webb  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Guide me, O Thou great Jehovah,
    Pilgrim through this barren land;
    I am weak, but Thou art mighty,
    Hold me with Thy pow’rful hand.
    Bread of heaven, Bread of heaven,
    Feed me till I want no more;
    Feed me till I want no more.
    
            \n\n2. Open now the crystal fountain,
    Whence the healing stream doth flow;
    Let the fire and cloudy pillar
    Lead me all my journey through.
            Strong Deliv’rer, strong Deliv’rer,
    Be Thou still my Strength and Shield;
    Be Thou still my Strength and Shield.
    
            \n\n3. When I tread the verge of Jordan,
    Bid my anxious fears subside;
    Death of death and hell’s Destruction,
    Land me safe on Canaan’s side.
    Songs of praises, songs of praises,
    I will ever give to Thee;
    I will ever give to Thee.
    
    \n\n4. Lord, I trust Thy mighty power,
    Wondrous are Thy works of old;
    Thou deliver’st Thine from thralldom,
    Who for naught themselves had sold:
    Thou didst conquer, Thou didst conquer
    Sin and Satan and the grave,
    Sin and Satan and the grave.
    ", "Guide Me, O Thou Great Jehovah. ","W. Williams, Wm L. Vine  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Safe in the arms of Jesus,
    Safe on His gentle breast;
    There by His love o’ershaded,
    Sweetly my soul shall rest.
    Hark! ’tis the voice of angels
    Borne in a song to me,
    Over the fields of glory,
    Over the jasper sea.
    
    \n\nRefrain:\nSafe in the arms of Jesus,
    Safe on His gentle breast;
    There by His love o’ershaded,
    Sweetly my soul shall rest.
    
    \n\n2. Safe in the arms of Jesus,
    Safe from corroding care,
    Safe from the world’s temptations;
    Sin cannot harm me there.
    Free from the blight of sorrow,
    Free from my doubts and fears;
    Only a few more trials,
    Only a few more tears!
    
    \n\n3. Jesus, my heart’s dear Refuge,
    Jesus has died for me;
    Firm on the Rock of Ages
    Ever my trust shall be.
    Here let me wait with patience,
    Wait till the night is o’er;
    Wait till I see the morning
    Break on the golden shore.
    ", "Safe in the Arms of Jesus. ","Fanny J. Crosby, W. H. Doane  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. God pities His children,
    He cares for His own
    With love true and tender
    No mortal hath known.
    Where’er you may wander,
    Whate’er you may do,
    O dear one, remember
    He careth for you!
    
            \n\nRefrain:\nHe careth for you,
    He careth for you;
    O dear one, remember
    He careth for you.
    
            \n\n2.  Do trials oppress you
    Too great to be borne?
    O think of the Suf’rer
    Encircled with thorn!
    Do loved ones you cherished,
    Lie hidden from view?
    One Friend yet remaineth
    Who careth for you!
    
            \n\n3. God sees all your sorrow,
    He hears when you pray;
    By tears are you blinded?
    He’ll point out the way.
    Perplexed, do you wonder
    What course to pursue?
    There’s One who can guide you—
    He careth for you!
    
            \n\n4. He’ll never forsake you;
    Tho’ far you may roam,
    His love-light is shining
    To welcome you home.
    To Him who redeemed you
    Be loyal and true;
    For He is your Savior
    And careth for you!
    ", "He Careth For You. ","Thoro Harris  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. There\’s a fountain flowing for the healing of the Soul, Praise the Lord! Praise the Lord! I have plunged beneath and now His blood has made me whole.
    
    \n\nCho-.\nPraise the Lord! Praise the Lord! Let the people praise the Lord, Hallelujah! Hallelujah! Sing He all with one accord, sing aloud and praise His name, Sing His glory, tell the story Of His wondrous love to men, Hallelujah, hallelujah, Let us spread abroad His fame.
    
    \n\n2. There\’s a table setting where each hungry soul may eat, And the Lord will fill Thee with the finest of the wheat.
    
    \n\n3. By His strong right hand Jehovah lifteth up the weak, And with full salvation He will beautify the meek.
    
    \n\n4. Let us serve the Lord with gladness, make a Joyful noise, Praise the Lord! Praise the Lord! Blessed is the people that know the heaven-ly Joys.
    
    ", "Praise the Lord. ","J. V. Ried ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Why do you wait, dear brother,
    Oh, why do you tarry so long?
    Your Savior is waiting to give you
    A place in His sanctified throng.
    
    \n\nCho-:\nWhy not? why not?
    Why not come to Him now?
    Why not? why not?
    Why not come to Him now?
    
    \n\n2.  What do you hope, dear brother,
    To gain by a further delay?
    There’s no one to save you but Jesus,
    There’s no other way but His way.
    
     \n\n3. do you know, dear brother?
            This life you should surly begin?
            O why not accept His salvation,
            And throw off thy burden of sin?
    
    \n\n4. Why do your wait, dear brother?
    The harvest is passing away;
    Your Savior is longing to bless you,
    There are danger and death in delay.
    ", "Why Do You Wait? ","Geo. F. Root  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. When the toils of life are over,
    And we lay our armor down,
    And we bid farewell to earth with all its cares,
    We shall meet and greet our loved ones,
    And our Christ we then shall crown,
    In the new Jerusalem.
    
            \n\nRefrain:\nThere’ll be singing, there’ll be shouting
    When the saints come marching home,
    In Jerusalem, in Jerusalem,
    Waving palms with loud hosannas
    As the King shall take His throne,
    In the new Jerusalem.
    
            \n\n2.  Tho’ the way is sometimes lonely,
    He will hold me with His hand,
    Thro’ the testings and the trials I must go.
    But I’ll trust and gladly follow,
    For sometime I’ll understand,
    In the new Jerusalem.
    
            \n\n3. When the last goodbye is spoken
    And the tear stains wiped away,
    And our eyes shall catch a glimpse of glory fair,
    Then with bounding hearts we’ll meet Him
    Who hath washed our sins away,
    In the new Jerusalem.
    
            \n\n4. When we join the ransomed army
    In the summer land above,
    And the face of our dear Saviour we behold,
    We will sing and shout forever,
    And we’ll grow in perfect love,
    In the new Jerusalem.
    ", "In the New Jerusalem. ","C. B. Widmeyer, 1939 ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. I\’ve left the way of death and sin, The road that many travel in; And if you ask the reason why, I seek a glorious home on high.
    
     \n\nCho-.\nThis world, this world is not my home, This world, this world is not my home; This world is not my resting place, This world, this world is not my home.
    
    \n\n2. Tho\’ many would my progress stay, And begged me not to watch and pray; I dare not listen to their cry; I seek a glorious home on high.
    
    \n\n3. O sinner,  come and go with me, And see this land of liberty;  O, do not stay, but tell me why You do not seek this home on High?
    ", "This World is Not My Home. ","John T. Benson  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. My hope is built on nothing less
    Than Jesus’ blood and righteousness;
    I dare not trust the sweetest frame,
    But wholly lean on Jesus’ name.
    
            \n\nRefrain:\nOn Christ, the solid Rock, I stand;
    All other ground is sinking sand,
    All other ground is sinking sand.
    
            \n\n2. When darkness veils His lovely face,
    I rest on His unchanging grace;
    In every high and stormy gale,
    My anchor holds within the veil.
    
            \n\n3. His oath, His covenant, His blood
    Support me in the whelming flood;
    When all around my soul gives way,
    He then is all my hope and stay.
    
            \n\n4. When He shall come with trumpet sound,
    Oh, may I then in Him be found;
    Dressed in His righteousness alone,
    Faultless to stand before the throne.
    ", "The Solid Rock. ","Edward Mote, Wm. B. Bradbury  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("                1. It was good for our mothers,
    It was good for our mothers,
    It was good for our mothers,
    It\'s good enough for me.
    
                    \n\nRefrain:\n\'Tis the old time religion,
    \'Tis the old time religion,
    \'Tis the old time religion,
    It\'s good enough for me.
    
            \n\n2. Makes me love ev\'ry body,
    Makes me love ev\'ry body,
    Makes me love ev\'ry body,
    It\'s good enough for me.
    
           \n\n3. It has saved our fathers.
            \n\n4. It was good for the Prophet Daniel.
    
    \n\n5.  It was good for the Hebrew children,
    It was good for the Hebrew children,
    It was good for the Hebrew children,
    It\'s good enough for me.
    
            \n\n6. It was tried in the fiery furnace.
    
    7. It was good for Paul and Silas,
    It was good for Paul and Silas,
    It was good for Paul and Silas,
    It\'s good enough for me.
    
            \n\n8. It will do when I am dying.
            \n\n9. It will take us all to heaven.
    ", "The Old Time Religion. ","Unknown  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. The disciples met together In the Masters blessed name, When the spirit of the Father Fell in cloven tongues of flame.
    
    \n\nCho-.\nOh Lord, send the power just now, Oh Lord, send the power just now, Oh Lord, send the power just now, And baptized every One.
    
    \n\n2. While we claim the promise given, Fill us now, Oh Lord, we pray; May the Holy Ghost from heaven Fall upon Thy saints today.
    
    \n\n3. Loose the floodgates of salvation, Let the tide overflow again; Come, and visit Thy plantation With the showers of latter rain.
    
    \n\n4. Healing all our souls diseases, Make us temples fit for God, That the mighty name of Jesus May be publish\’d far abroad. (Send the power just now).
    
    \n\n5. Then Thy people will adore Thee Who for them great things hath done, And the world will bow before Thee, Oh Thou great and glorious One!
    ", "Send the Power. ","Thoro Harris  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. There’s nothing so precious as Jesus to me;
    Let earth with its treasures be gone;
    I’m rich as can be when my Savior I see;
    I’m happy with Jesus alone.
    
            \n\nCho-:\nI’m happy with Jesus alone,
    I’m happy with Jesus alone.
    Tho’ poor and deserted, thank God I can say
    I’m happy with Jesus alone.
    
            \n\n2.  When sinful and doomed to a life of despair,
    No light on my pathway to shine,
    ’Twas Jesus who found me and made me an heir
    To mansions of glory divine.
    
            \n\n3. When nothing but death for my ransom could pay,
    And make me accepted with God,
    ’Twas Jesus who freely Himself made a prey
    And ransomed my soul with His blood.
    
            \n\n4. ’Twas Jesus who called me and showed me the way
    To peace upon earth and in heaven;
    ’Tis Jesus who teaches me daily to pray
    And walk in the light He has given.
    
            \n\n5. Should father and mother forsake me below,
    My bed upon earth be a stone,
    I\'ll cling to my Savior, He loves me I know,
    I\'m happy with Jesus alone.
    ", "I'm Happy With Jesus Alone. ","Chas. P. Jones  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("      1. In the blood from the cross
    I have been washed from sin;
    But to be free from dross,
    Still I would enter in.
    
            \n\nCho-:\nDeeper yet, deeper yet,
    Into the crimson flood;
    Deeper yet, deeper yet,
    Under the precious blood.
    
            \n\n2.  Day by day, hour by hour,
    Blessings are sent to me;
    But for more of His pow\'r
    Ever my prayer shall be.
    
            \n\n3. Near to Christ I would live,
    Following Him each day;
    What I ask He will give,
    So then with faith I pray.
    
            \n\n4. Now I have peace, sweet peace,
    While in this world of sin;
    But to pray I’ll not cease
    Till I am pure within.
    ", "Deeper Yet. ","Rev. J. Oatman Jr, Wm. J. Kirkpatrick ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Down at the cross where my Savior died,
    Down where for cleansing from sin I cried,
    There to my heart was the blood applied;
    Glory to His Name!
    
            \n\nRefrain:\nGlory to His Name,
    Glory to His Name:
    There to my heart was the blood applied;
    Glory to His Name!
    
    \n\n2. I am so wondrously saved from sin,
    Jesus so sweetly abides within,
    There at the cross where He took me in;
    Glory to His Name!
    
    \n\n3. Oh, precious fountain that saves from sin,
    I am so glad I have entered in;
    There Jesus saves me and keeps me clean;
    Glory to His Name!
    
    \n\n4. Come to this fountain so rich and sweet,
    Cast thy poor soul at the Savior’s feet;
    Plunge in today, and be made complete;
    Glory to His Name!
    ", "Glory to His Name. ","Elisha A. Hoffman, pub.1878  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Jesus, lover of my soul,
    let me to thy bosom fly,
    while the nearer waters roll,
    while the tempest still is high;
    hide me, O my Savior, hide,
    till the storm of life is past;
    safe into the haven guide,
    O receive my soul at last!
    
            \n\n2.  Other refuge have I none;
    hangs my helpless soul on thee;
    leave, ah! leave me not alone,
    still support and comfort me.
    All my trust on thee is stayed,
    all my help from thee I bring;
    cover my defenseless head
    with the shadow of thy wing.
    
            \n\n3. Thou, O Christ, art all I want,
    more than all in thee I find;
    raise the fallen, cheer the faint,
    heal the sick, and lead the blind.
    Just and holy is thy name,
    I am all unrighteousness;
    false and full of sin I am;
    thou art full of truth and grace.
    
            \n\n4. Plenteous grace with thee is found,
    grace to cover all my sin;
    let the healing streams abound;
    make and keep me pure within.
    Thou of life the fountain art;
    freely let me take of thee;
    spring thou up within my heart,
    rise to all eternity.
    ", "Refuge. ","Charles Wesley, J. P. Holbrook  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Jesus, lover of my soul,
    let me to thy bosom fly,
    while the nearer waters roll,
    while the tempest still is high;
    hide me, O my Savior, hide,
    till the storm of life is past;
    safe into the haven guide,
    O receive my soul at last!
    
            \n\n2.  Other refuge have I none;
    hangs my helpless soul on thee;
    leave, ah! leave me not alone,
    still support and comfort me.
    All my trust on thee is stayed,
    all my help from thee I bring;
    cover my defenseless head
    with the shadow of thy wing.
    
            \n\n3. Plenteous grace with thee is found,
    grace to cover all my sin;
    let the healing streams abound;
    make and keep me pure within.
    Thou of life the fountain art;
    freely let me take of thee;
    spring thou up within my heart,
    rise to all eternity.
    ", "Jesus, Lover of My Soul. ","S. B. Marsha  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I must tell Jesus all of my trials;
    I cannot bear these burdens alone;
    In my distress He kindly will help me;
    He ever loves and cares for His own.
    
         \n\nCho-:\nI must tell Jesus! I must tell Jesus!
    I cannot bear my burdens alone;
    I must tell Jesus! I must tell Jesus!
      Jesus can help me, Jesus alone.
    
            \n\n2.  I must tell Jesus all of my troubles;
    He is a kind, compassionate Friend;
    If I but ask Him, He will deliver,
    And in my griefs with me He will blend.
    
    \n\n3. Tempted and tried I need a great Savior,
    One who can help my burdens to bear;
    I must tell Jesus, I must tell Jesus;
    He all my cares and sorrows will share,
    
    \n\n4. O how the world to evil allures me!
    O how my heart is tempted to sin!
    I must tell Jesus; He will enable
    Over the world the vic’try to win.
    ", "I Must Tell Jesus. ","Elisha A. Hoffman  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. My faith looks up to Thee,
    Thou Lamb of Calvary,
    Savior divine!
    Now hear me while I pray,
    Take all my guilt away,
    Oh, let me from this day
    Be wholly Thine!
    
    \n\n2. May Thy rich grace impart
    Strength to my fainting heart,
    My zeal inspire!
    As Thou hast died for me,
    Oh, may my love to Thee
    Pure, warm, and changeless be,
    A living fire!
    
    \n\n3. While life’s dark maze I tread,
    And griefs around me spread,
    Be Thou my guide;
    Bid darkness turn to day,
    Wipe sorrow’s tears away,
    Nor let me ever stray
    From Thee aside.
    
    \n\n4. When ends life’s transient dream,
    When death’s cold, sullen stream
    Shall o’er me roll;
    Blest Savior, then in love,
    Fear and distrust remove;
    Oh, bear me safe above,
    A ransomed soul!
    ", "My Faith Looks Up to Thee. ","Ray Palmer, Lowell Mason  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. O for a thousand tongues to sing
    my great Redeemer\'s praise,
    the glories of my God and King,
    the triumphs of his grace!
    
    \n\n2. My gracious Master and my God,
    assist me to proclaim,
    to spread thro\' all the earth abroad
    the honors of your name.
    
    \n\n3. Jesus! the name that charms our fears,
    that bids our sorrows cease,
    \'tis music in the sinner\'s ears,
    \'tis life and health and peace.
    
    \n\n4. He breaks the power of cancelled sin,
    he sets the prisoner free;
    his blood can make the foulest clean;
    his blood availed for me.
    
    \n\n5. To God all glory, praise, and love
    be now and ever given
    by saints below and saints above,
    the Church in earth and heaven.
    ", "Oh, For A Thousand Tongues. ","C. Wesley, Lowell Mason  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. When pangs of death seized on my soul,
    Unto the Lord I cried;
    Till Jesus came and made me whole,
    I would not be denied.
    
    \n\nRefrain:\nI would not be denied,
    I would not be denied,
    Till Jesus came and made me whole,
    I would not be denied.
    
    \n\n2. As Jacob in the days of old,
    I wrestled with the Lord;
    And instant, with a courage bold,
    I stood upon His Word.
    
    \n\n3. Old Satan said my Lord was gone
    And would not hear my prayer;
    But praise the Lord, the work is done,
    And Christ the Lord is here.
    ", "I Would Not Be Denied. ","C. P. Jones  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. ’Tis the blessed hour of prayer, when our hearts lowly bend,
    And we gather to Jesus, our Savior and friend;
    If we come to Him in faith, His protection to share,
    What a balm for the weary, oh, how sweet to be there!
    
            \n\nRefrain:\nBlessed hour of prayer, blessed hour of prayer,
    What a balm for the weary, oh, how sweet to be there!
    
    \n\n2. ’Tis the blessed hour of prayer, when the Savior draws near,
    With a tender compassion His children to hear;
    When He tells us we may cast at His feet every care,
    What a balm for the weary, oh, how sweet to be there!
    
    \n\n3. ’Tis the blessed hour of prayer, when the tempted and tried
    To the Savior who loves them their sorrow confide;
    With a sympathizing heart He removes every care;
    What a balm for the weary, oh, how sweet to be there!
    
    \n\n4. At the blessed hour of prayer, trusting Him, we believe
    That the blessing we’re needing we’ll surely receive;
    In the fullness of the trust we shall lose every care;
    What a balm for the weary, oh, how sweet to be there!
    ", "'Tis the Blessed Hour of Prayer. ","Fanny J. Cosby  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Yield not to temptation, for yielding is sin;
    Each vict’ry will help you some other to win;
    Fight manfully onward, dark passions subdue;
    Look ever to Jesus, He’ll carry you through.
    
            \n\nRefrain:\nAsk the Savior to help you,
    Comfort, strengthen, and keep you;
    He is willing to aid you,
    He will carry you through.
    
    \n\n2. Shun evil companions, bad language disdain,
    God’s name hold in rev’rence, nor take it in vain;
    Be thoughtful and earnest, kindhearted and true;
    Look ever to Jesus, He’ll carry you through.
    
    \n\n3. To him that o’ercometh, God giveth a crown,
    Through faith we will conquer, though often cast down;
    He who is our Savior, our strength will renew;
    Look ever to Jesus, He’ll carry you through.
    ", "Yield Not to Temptation. ","H. R. Palmer  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. When the trump of the great archangel
    Its mighty tones shall sound,
    And, the end of the age proclaiming,
    Shall pierce the depths profound;
    When the Son of Man shall come in His glory
    To take the saints on high,
    What a shouting in the skies
    From the multitudes that rise,
    Changed in the twinkling of an eye.
    
     \n\nRefrain:\nChanged in the twinkling of an eye,
    Changed in the twinkling of an eye,
    The trumpet shall sound, the dead shall be raised,
    Changed in the twinkling of an eye.
    
            \n\n2. When He comes in the clouds descending,
    And they who loved Him here,
    From their graves shall awake and praise Him
    With joy and not with fear;
    When the body and the soul are united,
    And clothed no more to die,
    What a shouting there will be
    When each other’s face we see,
    Changed in the twinkling of an eye.
    
            \n\n3. O the seed that was sown in weakness
    Shall then be raised in pow’r
    And the songs of the blood bought millions
    Shall hail that blissful hour;
    When we gather safely home in the morning,
    And night’s dark shadows fly,
    What a shouting on the shore
    When we meet to part no more,
    Changed in the twinkling of an eye.
    ", "In the Twinkling of an Eye. ","Franny J. Crosby, Wm J. Kirkpatrick  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Precious mem\'ries, unseen angels,
    Sent from somewhere to my soul;
    How they linger, ever near me,
    And the sacred past unfold.
    
            \n\nRefrain:\nPrecious mem\'ries, how they linger,
    How they ever flood my soul;
    In the stillness of the midnight,
    Precious, sacred scenes unfold.
    
            \n\n2. Precious father, loving mother,
    Fly across the lonely years;
    And old home scenes of my childhood,
    In fond memory appear.
    
            \n\n3. In the stillness of the midnight,
    Echoes from the past I hear;
    Old time singing, gladness bringing,
    From that lovely land somewhere.
    
            \n\n4. As I travel on life\'s pathway,
    Know not what the years may hold;
    As I ponder, hope grows fonder,
    Precious mem\'ries flood my soul.
    ", "Precious Memories. ","J. B. F. Wright  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. In the lands beyond the sea
    Countless million children be
    Who have never heard the gospel story told;
    Little ones for whom the Christ
    Died and bought them with a price,
    Waiting to be gathered in the Savior\'s fold.
    
    
            \n\nCho-:\nJesus loves the little children,
    All the children of the world;
    Little ones are His delight,
    They are precious in His sight;
    Jesus loves the little
    Children of the world.
    
    \n\n2. Everywhere we hear their cry,
    Come and save us or we die:
    Who will haste to these blessed news to tell?
    They are precious to the King;
    Let us go these lambs to bring
    To the gracious arms of Him we love so well.
    
    \n\n3. From the east and from the west
    See them thronging to be blest,
    Precious jewels to adorn His diadem.
    If we thus obey the Lord,
    We shall reap a sure reward
    When He comes again to gather us, and them.
    ", "Jesus Loves the Children. ","Thoro Harris  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Come now, from your slumber awaking,
    And list to the voice of the Lord;
    Above you His sunlight is breaking,
    And o’er you His love is outpoured.
    
            \n\nRefrain:\nO come, come, come, come,
    Come while the Spirit is calling,
    O come to the Saviour today,
    This may be your last invitation;
    O come and no longer delay.
    
            \n\n2.  There\'s no one can save you but Jesus,
            There\'s no other name God has given;
            He only can heal your diseases,
            He only can guide you to heaven
    
    \n\n3. Find pardon and peace in believing,
    And rest only saved ones can know;
    The grace of assurance receiving,
    And joy only He can bestow.
    
    \n\n4. Then haste while the Saviour is waiting,
    The call of the Gospel obey;
    Give over your fruitless debating,
    And kneel at His pierced feet today.
    
    \n\n5. Today is the day of salvation,
    Now enter the wide open door;
    This may be your last invitation;
    God’s Spirit may call you no more.
    ", "Your Last Invitation. ","Thoro Harris  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Master, the tempest is raging!
    The billows are tossing high!
    The sky is o’ershadowed with blackness.
    No shelter or help is nigh.
    Carest thou not that we perish?
    How canst thou lie asleep
    When each moment so madly is threat’ning
    A grave in the angry deep?
    
          \n\nCho:-\nThe winds and the waves shall obey thy will:
    Peace, be still.
    Whether the wrath of the storm-tossed sea
    Or demons or men or whatever it be,
    No waters can swallow the ship where lies
    The Master of ocean and earth and skies.
    They all shall sweetly obey thy will:
    Peace, be still; peace, be still.
    They all shall sweetly obey thy will:
    Peace, peace, be still.
    
            \n\n2.  Master, with anguish of spirit
    I bow in my grief today.
    The depths of my sad heart are troubled.
    Oh, waken and save, I pray!
    Torrents of sin and of anguish
    Sweep o’er my sinking soul,
    And I perish! I perish! dear Master.
    Oh, hasten and take control!
    
    \n\n3. Master, the terror is over.
    The elements sweetly rest.
    Earth’s sun in the calm lake is mirrored,
    And heaven’s within my breast.
    Linger, O blessed Redeemer!
    Leave me alone no more,
    And with joy I shall make the blest harbor
    And rest on the blissful shore.
    ", "Master, the Tempest Is Raging. ","H. R. Palmer  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Lord, I have started to walk in the light
    That shines on my pathway so clearly, so bright;
    I’ve bade the world and its follies adieu,
    And now with my Savior I mean to go through.
    
    \n\nRefrain\nI’m going through, I’m going through,
    I’ll pay the price, whatever others do;
    I’ll take the way with the Lord’s despisèd few;
    I’m going through, Jesus, I’m going through.
    
    \n\n2. Many once started to run in this race,
    But with our Redeemer they could not keep pace;
    Others accepted because it was new,
    But not very many seem bound to go through.
    
    \n\n3. Let me but follow my Lord all alone,
    And have for my pillow, like Jacob, a stone,
    Rather than vain worldly pleasures pursue,
    Than turn from this pathway and fail to go through.
    
    \n\n4. Come then, my comrades, and walk in this way
    That leads to the kingdom of unending day;
    Turn from your idols and join with the few,
    Start in with your Savior, and keep going through.
    ", "I'm Going Through. ","Thoro Harris  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Jehovah’s arm is now revealed:
    How wonderful Jesus is!
    For Him hath God the Father sealed:
    How wonderful Jesus is!
    
    \n\nRefrain:\nHow wonderful Jesus is!
    All glory and praise be His!
    Heav’n’s fairest One,
    God’s matchless Son:
    How wonderful Jesus is!
    
    \n\n2.  Great sacrifice ordained by God,
    How wonderful Jesus is!
    Proclaim thru all the earth abroad
    How wonderful Jesus is!
    
    \n\n3. The Lion strong of Judah He,
    How wonderful Jesus is!
    The smitten Lamb of Calvary,
    How wonderful Jesus is!
    
    \n\n4. He pleads before the courts of Heav’n,
    How wonderful Jesus is!
    Thru His atonement peace is giv’n
    How wonderful Jesus is!
    
    \n\n5. He bears our sorrows far away,
    How wonderful Jesus is!
    In His prevailing name we pray:
    How wonderful Jesus is!
    
    \n\n6. His deathless love let saints declare,
    How wonderful Jesus is!
    And speak His glory everywhere:
    How wonderful Jesus is!
    
    \n\n7. His perfect praise let angels sing:
    How wonderful Jesus is!
    And make the bells of Heaven ring:
    How wonderful Jesus is!
    ", "How Wonderful Jesus Is! ","Thoro Harris  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. Jesus Christ is ris\'n today, Alleluia!
    our triumphant holy day, Alleluia!
    who did once upon the cross, Alleluia!
    suffer to redeem our loss. Alleluia!
    
            \n\n2. Hymns of praise then let us sing, Alleluia!
    unto Christ, our heav\'nly King, Alleluia!
    who endured the cross and grave, Alleluia!
    sinners to redeem and save. Alleluia!
    
            \n\n3. But the pains which He endured, Alleluia!
    our salvation have procured, Alleluia!
    now above the sky He\'s King, Alleluia!
    where the angels ever sing. Alleluia!
    
            \n\n4. Now be God the father praised, Alleluia!
            With the Son from death up-raised, Alleluia!
            And the spirit ever blest, Alleluia!
            One true God, by all confessed,Alleluia! A-men.
    ", "Jesus Christ Is Risen to-day. ","Lyra Davidica  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. The mighty God is Jesus, the Prince of Peace is He,
    The Everlasting Father, the King eternally,
    The wonderful in wisdom, by whom all things were made,
    The fullness of the Godhead in Jesus is displayed
    
    \n\nCho-:\nIt’s all in Him, it’s all in Him, the fullness of the Godhead is all in Him;
    It’s all in Him, it’s all in Him, the mighty God is Jesus, and it’s all in Him.
    
    \n\n2. Emmanuel God with us, Jehovah, Lord of hosts!
    The omnipresent Spirit, who fills the universe,
    The Advocate, the High Priest, the Lamb for sinners slain,
    The Author of redemption; O glory to His name!
    
    \n\n3. The Alpha and Omega, Beginning and the End,
    The Living Word incarnate, the helpless sinner’s Friend,
    Our wisdom and perfection, our righteousness and power,
    Yes, all we need in Jesus, we find this very hour.
    
    \n\n4. “Our God for whom we’ve waited” will be the glad refrain
    Of Israel recreated, when Jesus comes again;
    Lo! He will come and save us, our King and Priest to be,
    For in Him dwells all fullness, the Lord of all is He.
    ", "All in Him. ","Geo. R. Farrow  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Throw out the lifeline across the dark wave;
    There is a brother whom someone should save;
    Somebody’s brother! Oh, who then will dare
    To throw out the lifeline, his peril to share?
    
            \n\nRefrain:\nThrow out the lifeline! Throw out the lifeline!
    Someone is drifting away;
    Throw out the lifeline! Throw out the lifeline!
    Someone is sinking today.
    
    \n\n2. Throw out the lifeline with hand quick and strong:
    Why do you tarry, why linger so long?
    See! he is sinking; oh, hasten today
    And out with the life boat! Away, then, away!
    
    \n\n3. Throw out the lifeline to danger-fraught men,
    Sinking in anguish where you’ve never been;
    Winds of temptation and billows of woe
    Will soon hurl them out where the dark waters flow.
    
    \n\n4. Soon will the season of rescue be o’er,
    Soon will they drift to eternity’s shore;
    Haste, then, my brother, no time for delay,
    But throw out the lifeline and save them today.
    
    \n\n5. This is the lifeline, oh, tempest-tossed men,
    Baffled by waves of temptation and sin;
    Wild winds of passion, your strength cannot brave,
    But Jesus is mighty, and Jesus can save.
    
    \n\n6. Jesus is able! To you who are driv’n
    Farther and farther from God and from Heav’n,
    Helpless and hopeless, o’erwhelmed by the wave,
    We throw out the lifeline—’tis, “Jesus can save.”
    
    \n\n7. This is the lifeline, oh, grasp it today!
    See, you are recklessly drifting away;
    Voices in warning, shout over the wave,
    Oh, grasp the strong lifeline, for Jesus can save.
    ", "Throw Out the Life-Line. ","Rev. Edwin S. Ufford  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Great is the Lord, the Prince of life and glory,
            Great is the Lord, and wonderful his name;
            Shout, shout again the soul redeeming story,
            Mercy for all through him proclaim.
    
            \n\nCho-:\nGreat is the Lord, great is the Lord,
            Hail him, Hail him, sound his name afar;
            He is the light that shin-eth in the darkness,
            He is the Bright and Morning Star
    
            \n\n2. Great was the love that from his throne of splendor
            Brought him to earth for sinful man to die;
            Oh, for a gift amazing so tender,
            Glory to God, to God on high.
    
            \n\n3. Great is the Lord, the hope of our salvation,
            Strong is the tower where on the faithful stand;
            Oh, clap your hands with holy exultation,
            Come with a song at his command.
    
            \n\n4. Wake, every heart; let every voice adore him;
            Now let the world with hallelujahs ring;
            Sceptres and crowns in dust shall fall before him,
            Jesus alone shall reign our King.
    ", "Great is the Lord. ","Franny J. Cosby, Wm J. Kirkpatrick  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. When the sun refuse’s to shine,
    When the sun refuse’s to shine,
    Dear Lord I want to be in that number
    When the sun refuse’s to shine.
    
    \n\nRefrain:\nWhen the saints go marching in,
    When the saints go marching in;
    Dear Lord I want to be in that number
    When the saints go marching in.
    
    \n\n2.  When the moon turns into blood,
    When the moon turns into blood,
    Dear Lord I want to be in that number
    When the moon turns into blood.
    
    \n\n3. When we crown Him King of kings,
    When we crown Him King of kings,
    Dear Lord I want to be in that number
    When we crown Him King of kings.
    
    \n\n4. When they gather ‘round the throne,
    When they gather ‘round the throne,
    Dear Lord I want to be in that number
    When they gather ‘round the throne.
    
    \n\n5. While the happy ages roll,
    While the happy ages roll,
    Dear Lord I want to be in that number
    While the happy ages roll.
    ", "When the Saints Go Marching In. ","R. E. Winsett  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("         1. Jesus, my Saviour, to Bethlehem came,
    Born in a manger to sorrow and shame;
    Oh, it was wonderful — blest be His name!
    Seeking for me, for me!
    Seeking for me! For me!
    Seeking for me! For me!
    Oh, it was wonderful — blest be His name!
    Seeking for me, for me!
    
    \n\n2. Jesus, my Saviour, on Calvary’s tree,
    Paid the great debt and my soul He set free;
    Oh, it was wonderful — how could it be?
    Dying for me, for me!
    Dying for me! For me!
    Dying for me! For me!
    Oh, it was wonderful — how could it be?
    Dying for me, for me!
    
    \n\n3. Jesus, my Saviour, the same as of old,
    While I was wand’ring afar from the fold,
    Gently and long did He plead with my soul,
    Calling for me, for me!
    Calling for me! For me!
    Calling for me! For me!
    Gently and long did He plead with my soul,
    Calling for me, for me!
    
    \n\n4. Jesus, my Saviour, shall come from on high,—
    Sweet is the promise as weary years fly;
    Oh, I shall see Him descending the sky,
    Coming for me, for me!
    Coming for me! For me!
    Coming for me! For me!
    Oh, I shall see Him descending the sky,
    Coming for me, for me!
    ", "Seeking For Me. ","E. E. Hasty  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Are you weary, are you heavyhearted?
    Tell it to Jesus,
    Tell it to Jesus;
    Are you grieving over joys departed?
    Tell it to Jesus alone.
    
    \n\nCho-:\nTell it to Jesus, tell it to Jesus,
    He is a friend that\'s well-known;
    You\'ve no other such a friend or brother,
    Tell it to Jesus alone.
    
    \n\n2.  Do the tears flow down your cheeks unbidden?
    Tell it to Jesus,
    Tell it to Jesus;
    Have you sins that to men\'s eyes are hidden?
    Tell it to Jesus alone.
    
    \n\n3. Do you fear the gath\'ring clouds of sorrow?
    Tell it to Jesus,
    Tell it to Jesus;
    Are you anxious what shall be tomorrow?
    Tell it to Jesus alone.
    
    \n\n4. Are you troubled at the thought of dying?
    Tell it to Jesus,
    Tell it to Jesus;
    For Christ\'s coming kingdom are you sighing?
    Tell it to Jesus alone.
    ", "Tell it to Jesus. ","J. E. Rankin, D. D.  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. O wondrous day that\'s coming soon,
            When He is crowned the King of kings,
            In brightness far above the noon,
            When He is crowned the King of kings.
    
            \n\nCho-:\nWhen He is crowned,when He is crowned,
            The King of kings, the Lord of lords.
            When He is crowned, when He is crowned,
            The King of kings, the Lord of lords.
    
            \n\n2. O bliss-ful day, we wait thy light,
            When He is crowned the King of kings,
            With all the saints in spot-less white,
            When He is crowned the King of kings.
    
            \n\n3. O end-less day that soon will come,
            When He is crowned the King of kings,
            Then all the saints shall gather home,
            When He is crowned the King of kings.
    
            \n\n4. O day of the days the best,
            When He is crowned the King of kings,
            \'Twill be God\'s sweet Sab-bat-ic rest,
            When He is crowned the King of kings.
    ", "When He is Crowned. ","L. C. Hall  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("1. I will sing you a song of that beautiful land,
    The far away home of the soul,
    Where no storms ever beat on the glittering strand,
    While the years of eternity roll.
    
    \n\n2. O that home of the soul! In my visions and dreams,
    Its bright jasper walls I can see;
    Till I fancy but thinly the vail intervenes
    Between the fair city and me.
    
    \n\n3. That unchangeable home is for you and for me,
    Where Jesus of Nazareth stands;
    The King of all kingdoms for ever is he,
    And he holdeth our crowns in his hands.
    
    \n\n4. O how sweet it will be in that beautiful land,
    So free from all sorrow and pain,
    With songs on our lips and with harps in our hands,
    To meet one another again.
    ", "Home of the Soul. ","Ellen H. Gates  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. Jesus Christ my Savior, He has set me free; Now from sin and Satan I have liberty, All because He loved me, Died upon the tree; Died for my salvation on Mount Calvary.
    
     \n\nCho-.\nWorthy is the Lamb slain from the foundation of the world; Worthy is the is the lamb slain from the foundation of the world; Worthy is the Lamb slain from the foundation of the world, Christ the King of Kings let His banner be unfurled.
    
     \n\n2. He was wounded for my transgressions and my inequities; By the stripes on His body I am healed of all disease. Oh, this great salvation makes me sing with glee
    In my heart I feel this is the year of Jubilee.
    
    \n\n3. John saw Him in Heaven, King of Kings was He The four beasts and elders upon the glass-y sea,
    Threw their crowns before Him and fell down at His feet; Crying, Worthy is the Lamb slain, to all eternity.
    ", "Worthy is the Lamb. ","R. C. Lawson  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. God be with you till we meet again,
    By His counsels guide, uphold you,
    With His sheep securely fold you,
    God be with you till we meet again.
    
            \n\nCho:-\nTill we meet, till we meet,
    Till we meet at Jesus’ feet;
    Till we meet, till we meet,
    God be with you till we meet again.
    
    \n\n2. God be with you till we meet again,
    ’Neath His wings protecting hide you,
    Daily manna still divide you,
    God be with you till we meet again.
    
    \n\n3. God be with you till we meet again,
    Keep love’s banner floating o’er you,
    Smite death’s threatening wave before you,
    God be with you till we meet again.
    
            \n\n4. God be with you till we meet again,
    When life’s perils thick confound you,
    Put His arms unfailing round you,
    God be with you till we meet again.
    ", "God Be With You. ","J. E. Rankin  ");  this.custom_hymn.push(cusHymn);  
    
    
            let cusHymn=new CustomHymnProvider ("        1. We shall March forward and as conquerors, We dedicate our Lives to Him,
            And by his spirit that dwells within us Over every foe we\'ll win,
            We shall match forward as conquerors our banner we shall keep unfurled,
            And by His power, His Holy Spirit We shall evangelize the world.
    
     \n\nCho:-\nWe are the Pentecostal Conquerors, The world shall hear our conquering tread,
            Each day we\'re gaining, new heights attaining There\'s victory ahead
            We are the Pentecostal Conquerors we triumph o\'er every foe It\'s not by might nor yet by
            power, But by His spirit as we go.
    
     \n\n2. This gospel message we shall humbly tell And day by day we\'ll live it too Lord
            Keep us ever so meek and humble That thy will we\'ll gladly do This gospel message we shall ever tell
            Because it won\'t be very long Until His coming in clouds of glory And we shall sing the conquerors song.
    
    \n\n3. Though battle fierce, we\'ll match triumphantly His loyal soldiers brave and true His hand is guiding, in him abiding
            We shall press the battle through Like Paul of old we then can humbly say
            When life on earth comes to an end I\'ve fought a good fight, I\'ve kept the faith A crown of righteousness to win.
    ", "Pentecostal Conquerors Theme Song "," ");  this.custom_hymn.push(cusHymn);  

    return this.custom_hymn;
  }

}
