import { Injectable } from '@angular/core';

import { Platform } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/Rx';
import { Storage } from '@ionic/storage';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Injectable()
export class DatabaseProvider {
  database: SQLiteObject;
  private databaseReady: BehaviorSubject<boolean>;
  hymns:any;

  constructor(public sqlitePorter: SQLitePorter, private storage: Storage, private sqlite: SQLite, private platform: Platform, private http: Http) {
     this.databaseReady = new BehaviorSubject(false);
    this.platform.ready().then(() => {
       this.sqlite.create({
         name: 'hymns.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
           this.database = db;
          db.executeSql("drop table hymns");
          this.storage.set('database_filled', false);
           this.storage.get('database_filled').then(val => {
            if (val) {
              this.databaseReady.next(true);
            } else {
              this.fillDatabase();
            }
          }).catch(e => console.error(e));
        }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }
 
  fillDatabase() {
    this.http.get('assets/hymnsDump.sql')
      .map(res => res.text())
      .subscribe(sql => {
        this.sqlitePorter.importSqlToDb(this.database, sql)
          .then(data => {
            this.databaseReady.next(true);
            this.storage.set('database_filled', true);
          })
          .catch(e => console.error(e));
      });
  }
getHymn(id: number){
  return this.database.executeSql('SELECT * FROM hymns WHERE id = ?',[id])
    .then((data) => {
      let hymnsRes=[];
      console.log('RESULT: '+ JSON.stringify(data));

      if(data.rows.length >0) {
        hymnsRes.push({ title: data.rows.item(0).title, author: data.rows.item(0).author, hymn: data.rows.item(0).hymn});
      }
      return hymnsRes;
  },  err => {
    console.log('Error: '+ err);
    return [];
  });
}
 
  getAllHymns() {
    return this.database.executeSql("SELECT * FROM hymns", []).then((data) => {
      this.hymns = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          this.hymns.push({ title: data.rows.item(i).title, author: data.rows.item(i).author, hymn: data.rows.item(i).hymn});
        }
      }
      return this.hymns;
    }, err => {
      console.log('Error: ', err);
      return [];
    });
  }

//   filterItems(searchTerm){

//     return this.hymns.title.filter((item) => {
//         return item.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
//     });     

// }
 
  getDatabaseState() {
    return this.databaseReady.asObservable();
  }
}

